#!/usr/bin/env zsh

# Create analysis
./bin/ma5 -E -R
mht
cms_sus_13_012

# Get necessary files and compile:
cd mht/Build/SampleAnalyzer/User/Analyzer
rm cms_sus_13_012.*
wget 'http://inspirehep.net/record/1305458/files/cms_sus_13_012.cpp'
wget 'http://inspirehep.net/record/1305458/files/cms_sus_13_012.h'
wget 'http://inspirehep.net/record/1305458/files/cms_sus_13_012.info'
cd ../../..
source setup.sh
make

# if needed adjust to newer version of MadAnalysis:
vim SampleAnalyzer/User/Analyzer/cms_sus_13_012.cpp
:%s/TLorentzVector/MALorentzVector/g
:wq
make

# Get a root file and do an analysis on it:
wget 'http://phys.onmybike.nl/ma5/testfile.root'
echo $(realpath testfile.root ) > ../Input/testmht
./MadAnalysis5job ../Input/testmht

# Copy the recasting program and compute CLs and upper limits:
cd ..
cp ../tools/RecastingTools/exclusion_CLs.py .
./exclusion_CLs.py cms_sus_13_012 testmht 0 0.2
./exclusion_CLs.py cms_sus_13_012 testmht 0 2.0
# Give negative cross section for computing an upper limit:
./exclusion_CLs.py cms_sus_13_012 testmht 0 -1
