#!/usr/bin/env zsh


#wget http://phys.onmybike.nl/ma5/pythia_events.hep
recastcard='/data/bin/madanalysis5/recasting_card.dat'
hepfile='/data/federico/bak/SMS-GlGl_onejet_T1bbbb_1500_100.hepmc'
xsec=10

### echo instructions to ma5
echo '
set main.recast = on
set main.recast.store_root = True
import ' $hepfile ' as test
set test.xsection = ' $xsec '
set main.recast.card_path = ' $recastcard '
submit
N
' | ./bin/ma5 -R


# /home/jory/bin/korea/madanalysis5/madanalysis/configuration/recast_configuration.py
