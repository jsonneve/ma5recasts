#include "SampleAnalyzer/User/Analyzer/cms_sus_16_014.h"
using namespace MA5;
using namespace std;

// Define =====================================================
// =====some utilities to be used in the analysis ======
// ========================================================

double calcSumPt(const RecLeptonFormat* mylepton, double coneSize)
{
  double sumPt_ = 0;
  for(unsigned int c=0; c<mylepton->isolCones().size(); c++)
    {
      if(!(fabs(mylepton->isolCones()[c].deltaR() - coneSize)<0.0001)) continue;
      sumPt_ = mylepton->isolCones()[c].sumPT();
    }
  return sumPt_;
}


// -----------------------------------------------------------------------------
// Initialize
// function called one time at the beginning of the analysis
// -----------------------------------------------------------------------------
bool cms_sus_16_014::Initialize(const MA5::Configuration& cfg, const std::map<std::string,std::string>& parameters)
{
  cout << "BEGIN Initialization" << endl;
  // initialize variables, histos
  cout << "END   Initialization" << endl;
  // Information on the analysis, authors, ...
  // VERY IMPORTANT FOR DOCUMENTATION, TRACEABILITY, BUG REPORTS
  INFO << "        <><><><><><><><><><><><><><><><><><><><><><><><>" << endmsg;
  INFO << "        <>    Analysis: CMS-PAS-SUS-16-014                <>" << endmsg;
  INFO << "        <>    Multijet+MHT @sqrt(s) = 13 TeV, 13 fb^-1 luminosity  <>" << endmsg;
  INFO << "        <>    Recasted by: J. Sonneveld <>" << endmsg;
  INFO << "        <>    Contact: jory.sonneveld@desy.de           <>" << endmsg;
  INFO << "        <>    Based on MadAnalysis 5 v1.4 and above        <>" << endmsg;
  INFO << "        <>    For more information, see               <>" << endmsg;
  INFO << "        <>    http://madanalysis.irmp.ucl.ac.be/wiki/PublicAnalysisDatabase <>" << endmsg;
  INFO << "        <>    Validated on cutflow given at " << endmsg;
  INFO << "        <>    Notes: " << endmsg;
  INFO << "        <><><><><><><><><><><><><><><><><><><><><><><><>" << endmsg;

// =====Declare the 12 signal regions in this analysis=====
  Manager()->AddRegionSelection(   "Nbjets0, Njets>=3, HT>500, MHT>500");
  Manager()->AddRegionSelection(   "Nbjets0, Njets>=3, HT>1500, MHT>750");
  Manager()->AddRegionSelection(   "Nbjets0, Njets>=5, HT>500, MHT>500");
  Manager()->AddRegionSelection(   "Nbjets0, Njets>=5, HT>1500, MHT>750");
  Manager()->AddRegionSelection(   "Nbjets0, Njets>=9, HT>1500, MHT>750");

  Manager()->AddRegionSelection(   "Nbjets1, Njets>=3, HT>750, MHT>750");
  Manager()->AddRegionSelection(   "Nbjets1, Njets>=5, HT>750, MHT>750");
  Manager()->AddRegionSelection(   "Nbjets1, Njets>=7, HT>300, MHT>300");


  Manager()->AddRegionSelection(   "Nbjets2, Njets>=3, HT>500, MHT>500");
  Manager()->AddRegionSelection(   "Nbjets2, Njets>=5, HT>1500, MHT>750");

  Manager()->AddRegionSelection(  "Nbjets>=3, Njets>=5, HT>500, MHT>500");
  Manager()->AddRegionSelection(  "Nbjets>=3, Njets>=9, HT>750, MHT>750");
//=============Baseline Cuts=================//
 Manager()->AddCut("Njets>=3");
 Manager()->AddCut("MHT>300 GeV");
 Manager()->AddCut("HT>300 GeV");
 Manager()->AddCut("No Lepton");
 //Manager()->AddCut("ElectronVeto");
 Manager()->AddCut("ElectronIsoTrackVeto");
 //Manager()->AddCut("MuonVeto");
 Manager()->AddCut("MuonIsoTrackVeto");
 Manager()->AddCut("HadronIsoTrackVeto");
 Manager()->AddCut("MinDeltaPhi");
 //Manager()->AddCut("dphij1mht > 0.5");
 //Manager()->AddCut("dphij2mht > 0.5");
 //Manager()->AddCut("dphij3mht > 0.3");
 //Manager()->AddCut("dphij4mht > 0.3");

//======Signal Region Cuts====AggregatedRegions==========


 //Nbjet regions
  string SR_NbJet0[] = 
    {
      "Nbjets0, Njets>=3, HT>500, MHT>500",
      "Nbjets0, Njets>=3, HT>1500, MHT>750",
      "Nbjets0, Njets>=5, HT>500, MHT>500",
      "Nbjets0, Njets>=5, HT>1500, MHT>750",
      "Nbjets0, Njets>=9, HT>1500, MHT>750",

    };
  Manager()->AddCut("NbJets0",SR_NbJet0);

  string SR_NbJet1[] = 
    {
      "Nbjets1, Njets>=3, HT>750, MHT>750",
      "Nbjets1, Njets>=5, HT>750, MHT>750",
      "Nbjets1, Njets>=7, HT>300, MHT>300",
    };
  Manager()->AddCut("NbJets1",SR_NbJet1);

  string SR_NbJet2[] = 
    {
      "Nbjets2, Njets>=3, HT>500, MHT>500",
      "Nbjets2, Njets>=5, HT>1500, MHT>750",
    };
  Manager()->AddCut("NbJets2",SR_NbJet2);

  string SR_NbJet3[] = 
    {
      "Nbjets>=3, Njets>=5, HT>500, MHT>500",
      "Nbjets>=3, Njets>=9, HT>750, MHT>750",
    };
  Manager()->AddCut("NbJets3",SR_NbJet3);

 //Njet regions
  string SR_NJet3[] = 
    {
      "Nbjets0, Njets>=3, HT>500, MHT>500",
      "Nbjets0, Njets>=3, HT>1500, MHT>750",
      "Nbjets1, Njets>=3, HT>750, MHT>750",
      "Nbjets2, Njets>=3, HT>500, MHT>500",

    };
  Manager()->AddCut("NJets>=3",SR_NJet3);

  string SR_NJet5[] = 
    {
      "Nbjets0, Njets>=5, HT>500, MHT>500",
      "Nbjets0, Njets>=5, HT>1500, MHT>750",
      "Nbjets1, Njets>=5, HT>750, MHT>750",
      "Nbjets2, Njets>=5, HT>1500, MHT>750",
      "Nbjets>=3, Njets>=5, HT>500, MHT>500",
    };
  Manager()->AddCut("NJets>=5",SR_NJet5);

  string SR_NJet7[] = 
    {
      "Nbjets1, Njets>=7, HT>300, MHT>300",
    };
  Manager()->AddCut("NJets>=7",SR_NJet7);

  string SR_NJet9[] = 
    {
      "Nbjets0, Njets>=9, HT>1500, MHT>750",
      "Nbjets>=3, Njets>=9, HT>750, MHT>750",
    };
  Manager()->AddCut("NJets>=9",SR_NJet9);

  //HT regions
  string SR_HTgt300[] = 
    { 
      "Nbjets1, Njets>=7, HT>300, MHT>300",
    };
  Manager()->AddCut("HT>300",SR_HTgt300);
  string SR_HTgt500[] = 
    { 
      "Nbjets0, Njets>=3, HT>500, MHT>500",
      "Nbjets0, Njets>=5, HT>500, MHT>500",
      "Nbjets2, Njets>=3, HT>500, MHT>500",
      "Nbjets>=3, Njets>=5, HT>500, MHT>500",
    };
  Manager()->AddCut("HT>500",SR_HTgt500);

  string SR_HTgt750[] = 
    { 
      "Nbjets1, Njets>=3, HT>750, MHT>750",
      "Nbjets1, Njets>=5, HT>750, MHT>750",
      "Nbjets>=3, Njets>=9, HT>750, MHT>750",
    };
  Manager()->AddCut("HT>750",SR_HTgt750);
  string SR_HTgt1500[] = 
    { 
      "Nbjets0, Njets>=3, HT>1500, MHT>750",
      "Nbjets0, Njets>=5, HT>1500, MHT>750",
      "Nbjets0, Njets>=9, HT>1500, MHT>750",
      "Nbjets2, Njets>=5, HT>1500, MHT>750",
    };
  Manager()->AddCut("HT>1500",SR_HTgt1500);

  //HT regions
  string SR_MHTgt300[] = 
    { 
      "Nbjets1, Njets>=7, HT>300, MHT>300",
    };
  Manager()->AddCut("MHT>300",SR_MHTgt300);
  string SR_MHTgt500[] = 
    { 
      "Nbjets0, Njets>=3, HT>500, MHT>500",
      "Nbjets0, Njets>=5, HT>500, MHT>500",
      "Nbjets2, Njets>=3, HT>500, MHT>500",
      "Nbjets>=3, Njets>=5, HT>500, MHT>500",
    };
  Manager()->AddCut("MHT>500",SR_MHTgt500);

  string SR_MHTgt750[] = 
    { 
      "Nbjets0, Njets>=3, HT>1500, MHT>750",
      "Nbjets0, Njets>=5, HT>1500, MHT>750",
      "Nbjets0, Njets>=9, HT>1500, MHT>750",
      "Nbjets1, Njets>=3, HT>750, MHT>750",
      "Nbjets1, Njets>=5, HT>750, MHT>750",
      "Nbjets2, Njets>=5, HT>1500, MHT>750",
      "Nbjets>=3, Njets>=9, HT>750, MHT>750",
    };
  Manager()->AddCut("MHT>750",SR_MHTgt750);


  //histograms
  Manager()->AddHisto("NJets_METCleaning",16,-0.5,15.5);
  Manager()->AddHisto("NJets_NoLepton",16,-0.5,15.5);
  Manager()->AddHisto("NJets_NJets>3",16,-0.5,15.5);
  Manager()->AddHisto("NJets_HT>300",16,-0.5,15.5);
  Manager()->AddHisto("NJets_MHT>300",16,-0.5,15.5);
  Manager()->AddHisto("NJets_MinDeltaPhi",16,-0.5,15.5);

  Manager()->AddHisto("HT_METCleaning",40,0,4000);
  Manager()->AddHisto("HT_NoLepton",40,0,4000);
  Manager()->AddHisto("HT_NJets>3",40,0,4000);
  Manager()->AddHisto("HT_HT>300",40,0,4000);
  Manager()->AddHisto("HT_MHT>300",40,0,4000);
  Manager()->AddHisto("HT_MinDeltaPhi",40,0,4000);

  Manager()->AddHisto("MHT_METCleaning",30,0,1500);
  Manager()->AddHisto("MHT_NoLepton",30,0,1500);
  Manager()->AddHisto("MHT_NJets>3",30,0,1500);
  Manager()->AddHisto("MHT_HT>300",30,0,1500);
  Manager()->AddHisto("MHT_MHT>300",30,0,1500);
  Manager()->AddHisto("MHT_MinDeltaPhi",30,0,1500);





//================================================
  return true;
}

// -----------------------------------------------------------------------------
// Finalize
// function called one time at the end of the analysis
// -----------------------------------------------------------------------------
void cms_sus_16_014::Finalize(const SampleFormat& summary, const std::vector<SampleFormat>& files)
{
  cout << "BEGIN Finalization" << endl;
  // saving histos
  cout << "END   Finalization" << endl;
}

// -----------------------------------------------------------------------------
// Execute
// function called each time one event is read
// -----------------------------------------------------------------------------
bool cms_sus_16_014::Execute(SampleFormat& sample, const EventFormat& event)
{





  double JEscale = 1;
  double myEventWeight;
  if(Configuration().IsNoEventWeight()) myEventWeight=1.;
  else if(event.mc()->weight()!=0.) myEventWeight=event.mc()->weight();
  else
    {
      INFO << "Found one event with a zero weight. Skipping...\n";
      return false;
    }
  Manager()->InitializeForNewEvent(myEventWeight);

  //the loop start
  if (event.rec()==0) {return true;}
       EventFormat myEvent;
       myEvent = event;
  // ==========================================
  // Define collections of objects =============
  // ==========================================

    // =====first declare the empty containers:=====
    vector<const RecLeptonFormat*> isoElectron, isoMuon;
    vector<const RecLeptonFormat*> isoElectronTrack, isoMuonTrack, isoHadronTrack;
    vector<const RecJetFormat*> looseJet, tightJet, bJet;


     // =====fill the electrons container:=====
    for(unsigned int e=0; e<event.rec()->electrons().size(); e++)
    {
      const RecLeptonFormat *CurrentElectron = &(event.rec()->electrons()[e]);
      double pt = CurrentElectron->momentum().Pt();
      if(!(pt>10)) continue;
      double eta = CurrentElectron->momentum().Eta();
      if(!(fabs(eta)<2.5)) continue;
      double radius = 0.2;
      if (pt > 200.) {
          radius = .5;
      } else if(pt > 50) {
          radius = 10./pt;
      }
      double sumpt = calcSumPt(CurrentElectron, radius);
      if(!(sumpt/pt < 0.1)) continue;
      isoElectron.push_back(CurrentElectron);
    }

    // =====fill the muons container:=====
    for(unsigned int m=0; m<event.rec()->muons().size(); m++)
    {
      const RecLeptonFormat *CurrentMuon = &(event.rec()->muons()[m]);
      double pt = CurrentMuon->momentum().Pt();
      if(!(pt>10)) continue;
      double eta = CurrentMuon->momentum().Eta();
      if(!(fabs(eta)<2.4)) continue;
      double radius = 0.2;
      if (pt > 200.) {
          radius = .5;
      } else if(pt > 50) {
          radius = 10./pt;
      }
      double sumpt = calcSumPt(CurrentMuon, radius);
      if(!(sumpt/pt < 0.2)) continue;
      isoMuon.push_back(CurrentMuon);
    }

    // =====fill the jet containers and order=====
    for(unsigned int j=0; j<event.rec()->jets().size(); j++)
    {
      const RecJetFormat *CurrentJet = &(event.rec()->jets()[j]);
      double pt = JEscale*CurrentJet->momentum().Pt();
      double eta = CurrentJet->momentum().Eta();
      if(!(pt > 30 && fabs(eta) < 5))continue;
      looseJet.push_back(CurrentJet);
      if(CurrentJet->btag()) bJet.push_back(CurrentJet);
      if(!(pt>30. && fabs(eta) < 2.4))continue;
      tightJet.push_back(CurrentJet);
    }
    SORTER->sort(looseJet);
    SORTER->sort(tightJet);

//======Overlap removal of jets===================//
//     SignalJets = PHYSICS->Isol->JetCleaning(SignalJets, SignalElectrons, 0.4);
//     SignalJets = PHYSICS->Isol->JetCleaning(SignalJets, SignalMuons,     0.4);
    // =====Get the missing ET=====
    //MALorentzVector pTmiss = event.rec()->MET().momentum();
    //double MET = pTmiss.Pt();
    MALorentzVector pTmiss = event.rec()->MET().momentum();
    //double MET = pTmiss.Pt();

    // =====Calculate the HT=====
    double HT = 0;
    for(unsigned int j = 0; j < tightJet.size(); j++)
      {
	HT += JEscale*tightJet[j]->momentum().Pt();
      }

    // =====Calculate the missing HT=====
    double MHT = 0;
    MALorentzVector MHT_vec = MALorentzVector();
    MALorentzVector CurrentJet;
    for(unsigned int j = 0; j < looseJet.size(); j++)
      {
	CurrentJet.SetPtEtaPhiE
	  (looseJet[j]->momentum().Pt()*JEscale, looseJet[j]->momentum().Eta(), 
	   looseJet[j]->momentum().Phi(), looseJet[j]->momentum().E()*JEscale);
	MHT_vec -= CurrentJet;
      }
    MHT = MHT_vec.Pt();
    int NJets = tightJet.size();
    int NbJets = tightJet.size();


    // ==========================================
    // Apply the "baseline selection" cuts =======
    // ==========================================

  
    // =====Before Any Cuts=====
    Manager()->FillHisto("NJets_METCleaning",      NJets);
    Manager()->FillHisto("HT_METCleaning",            HT);
    Manager()->FillHisto("MHT_METCleaning",          MHT);

    // =====Apply no-lepton cut========
    if(!Manager()->ApplyCut((isoElectron.size()+isoMuon.size() == 0),"No Lepton")) return true;
    Manager()->FillHisto("NJets_NoLepton",         NJets);
    Manager()->FillHisto("HT_NoLepton",               HT);
    Manager()->FillHisto("MHT_NoLepton",             MHT);

    // =====Apply NJets jut============
    if(!Manager()->ApplyCut((tightJet.size() > 3),"Njets>=3")) return true; 
    Manager()->FillHisto("NJets_NJets>3",          NJets);
    Manager()->FillHisto("HT_NJets>3",                HT);
    Manager()->FillHisto("MHT_NJets>3",              MHT);

    // =====Apply HT300 cut============
    if(!Manager()->ApplyCut((HT > 300),"HT>300 GeV")) return true;
    Manager()->FillHisto("NJets_HT>300",           NJets);
    Manager()->FillHisto("HT_HT>300",                 HT);
    Manager()->FillHisto("MHT_HT>300",               MHT);

    // =====Apply MHT300 cut===========
    if(!Manager()->ApplyCut((MHT > 300),"MHT>300 GeV")) return true;
    Manager()->FillHisto("NJets_MHT>300",          NJets);
    Manager()->FillHisto("HT_MHT>300",                HT);
    Manager()->FillHisto("MHT_MHT>300",              MHT);

    // =====Calculate the deltaPhi(jet(1,2,3), MHT)=====
    MALorentzVector jet1 = MALorentzVector();
    jet1.SetPtEtaPhiE
      (tightJet[0]->momentum().Pt(), tightJet[0]->momentum().Eta(), 
       tightJet[0]->momentum().Phi(), tightJet[0]->momentum().E());
    MALorentzVector jet2 = MALorentzVector();
    jet2.SetPtEtaPhiE
      (tightJet[1]->momentum().Pt(), tightJet[1]->momentum().Eta(), 
       tightJet[1]->momentum().Phi(), tightJet[1]->momentum().E());
    MALorentzVector jet3 = MALorentzVector();
    jet3.SetPtEtaPhiE
      (tightJet[2]->momentum().Pt(), tightJet[2]->momentum().Eta(), 
       tightJet[2]->momentum().Phi(), tightJet[2]->momentum().E());
    MALorentzVector jet4 = MALorentzVector();
    jet4.SetPtEtaPhiE
      (tightJet[3]->momentum().Pt(), tightJet[3]->momentum().Eta(), 
       tightJet[3]->momentum().Phi(), tightJet[3]->momentum().E());
    // or: {DeltaPhiJet1= SignalJets[0]->dphi_0_pi(pTmiss);
    double dPhi1 = fabs(MHT_vec.DeltaPhi(jet1));
    double dPhi2 = fabs(MHT_vec.DeltaPhi(jet2));
    double dPhi3 = fabs(MHT_vec.DeltaPhi(jet3));
    double dPhi4 = fabs(MHT_vec.DeltaPhi(jet4));

    // =====Apply min dPhi cut======
    if(!Manager()->ApplyCut( dPhi1>0.5 && dPhi2>0.5 && dPhi3>0.3 && dPhi4>0.3,"MinDeltaPhi")) return true;
    Manager()->FillHisto("NJets_MinDeltaPhi",          NJets);
    Manager()->FillHisto("HT_MinDeltaPhi",                HT);
    Manager()->FillHisto("MHT_MinDeltaPhi",              MHT);



    // ==========================================
    // Fill Histograms for extra checking =======
    // ==========================================


    // ==========================================
    // Apply the Signal Region - dependent cuts =======
    // ==========================================
    Manager()->ApplyCut(0 == NbJets, "NbJets0");
    Manager()->ApplyCut(1 == NbJets, "NbJets1");
    Manager()->ApplyCut(2 == NbJets, "NbJets2");
    Manager()->ApplyCut(3 <= NbJets, "NbJets>=3");
    Manager()->ApplyCut(5 <= NJets, "NJets>=5");
    Manager()->ApplyCut(7 <= NJets, "NJets>=7");
    Manager()->ApplyCut(9 <= NJets, "NJets>=9");
    Manager()->ApplyCut(300 <= HT, "HT>300");
    Manager()->ApplyCut(500 <= HT, "HT>500");
    Manager()->ApplyCut(750 <= HT, "HT>750");
    Manager()->ApplyCut(1500 <= HT, "HT>1500");
    Manager()->ApplyCut(300 <= MHT, "MHT>300");
    Manager()->ApplyCut(500 <= MHT, "MHT>500");
    Manager()->ApplyCut(750 <= MHT, "MHT>750");

//==========End===================//

    return true;




}

