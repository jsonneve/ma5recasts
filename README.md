
## Recasted analyses in MadAnalysis5



#### To get a ROOTfile:
cd cms_sus_16_033/code/
#### Set the correct directories in the file:
from_cards_to_rootfile.sh
#### Then execute:
./from_cards_to_rootfile.sh SMS SMS-GlGl_onejet T1bbbb_1500_100
#### This uses an slha card like
cms_sus_16_033/cards/mgpyth8/T1bbbb_1500_100.dat
#### and a MadGraph proc card like
cms_sus_16_033/cards/mgpyth8/SMS-GlGl_onejet.dat
#### and a pythia8 card, e.g.
cms_sus_16_033/cards/mgpyth8/SMS-GlGl_onejet_T1bbbb_1500_100_pythia8_card.dat
#### and a delphes card like e.g.
cms_sus_16_033/cards/delphes_card_CMS.tcl

### More general way to get a ROOTfile with inputcards:
cd automation_code

##### set your madgraph, delphes, and delphesdirectories:
mg5dir=/data/bin/korea/MG5_aMC_v2_6_0

delphesdir=/data/bin/delphes

outputfiledir=/data/bin/korea/outputfiles

##### then run:
./from_cards_to_rootfile.sh proc_card_mg5_test.dat run_card_test.dat
##### or
./from_cards_to_rootfile proccard runcard pythiacard slha delphescard
##### where you specify your card

#### How to set up the programs one can find in here:
cms_sus_16_033/code/produce.sh
#### To submit jobs one can do on e.g. cms connect:
cms_sus_16_033/code/cms_connect_condor.submit

#### To analyze a ROOTfile:
#### Set up MadAnalysis:
#### http://madanalysis.irmp.ucl.ac.be/wiki/Ma5PadStepByStep
cd cms_sus_16_033/code/
./make_ma5.sh
./makeroot_txtfiles.sh
./run_ma5.sh

#### See also http://madanalysis.irmp.ucl.ac.be/wiki/PublicAnalysisDatabase

