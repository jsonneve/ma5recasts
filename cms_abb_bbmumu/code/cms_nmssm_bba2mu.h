#ifndef analysis_cms_nmssm_bba2mu_h
#define analysis_cms_nmssm_bba2mu_h

#include "SampleAnalyzer/Process/Analyzer/AnalyzerBase.h"

namespace MA5
{
class cms_nmssm_bba2mu : public AnalyzerBase
{
  INIT_ANALYSIS(cms_nmssm_bba2mu,"cms_nmssm_bba2mu")

 public:
  virtual bool Initialize(const MA5::Configuration& cfg, const std::map<std::string,std::string>& parameters);
  virtual void Finalize(const SampleFormat& summary, const std::vector<SampleFormat>& files);
  virtual bool Execute(SampleFormat& sample, const EventFormat& event);

 private:
};
}

#endif
