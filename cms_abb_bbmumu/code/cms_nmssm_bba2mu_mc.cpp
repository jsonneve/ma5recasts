#include "SampleAnalyzer/User/Analyzer/cms_nmssm_bba2mu_mc.h"
using namespace MA5;
using namespace std;

// Define =====================================================
// =====some utilities to be used in the analysis ======
// ========================================================

double calcSumPt(const RecLeptonFormat* mylepton, double coneSize)
{
  double sumPt_ = 0;
  for(unsigned int c=0; c<mylepton->isolCones().size(); c++)
    {
      if(!(fabs(mylepton->isolCones()[c].deltaR() - coneSize)<0.0001)) continue;
      sumPt_ = mylepton->isolCones()[c].sumPT();
    }
  return sumPt_;
}


// ===============================================================
// Initialize =====================================================
// function called one time at the beginning of the analysis ======
// ===============================================================
bool cms_nmssm_bba2mu_mc::Initialize(const MA5::Configuration& cfg, const std::map<std::string,std::string>& parameters)
{
  cout << "========================================\n";
  cout << "Analysis: CMS-NMSSM-BBA2MU\n";
  cout << "Written with MA5 version 1.1.11 patch1b\n";
  cout << "Authors: J Sonneveld\n";
  cout << "Contact e-mail: sonneveld@physik.rwth-aachen.de\n";
  cout << "========================================\n";

  cout << "BEGIN Initialization" << endl;
  // =====Declare the signal regions in this analysis=====
  // There is only one in this case
  Manager()->AddHisto("NbJets_Parton",16,-0.5,15.5);

  Manager()->AddHisto("pTb_Parton",40,0,200);

  cout << "END   Initialization" << endl;

  return true;
}

// ==========================================================
// Finalize ==================================================
// function called one time at the end of the analysis =======
// ==========================================================
void cms_nmssm_bba2mu_mc::Finalize(const SampleFormat& summary, const std::vector<SampleFormat>& files)
{
  cout << "BEGIN Finalization" << endl;
  cout << "not really doing anything"<<endl;//could save histos here, where's this done?
  cout << "END   Finalization" << endl;
  return;
}


// ====================================================
// Execute =============================================
// function called each time one event is read =========
// ====================================================
bool cms_nmssm_bba2mu_mc::Execute(SampleFormat& sample, const EventFormat& event)
{

  // Give the event a weight so that it is counted:
  double myEventWeight;
  if(Configuration().IsNoEventWeight()) myEventWeight=1.;
  else if(event.mc()->weight()!=0.) myEventWeight=event.mc()->weight();
  else
    {
      INFO << "Found one event with a zero weight. Skipping...\n";
      return false;
    }
  Manager()->InitializeForNewEvent(myEventWeight);


  if (event.mc()!=0)
    {
    // ==========================================
    // Define collections of objects =============
    // ==========================================

    // =====first declare the empty containers:=====
    vector<const MCParticleFormat*> bparton;
    vector<const MCParticleFormat*> allparts;
    vector<const MCParticleFormat*> finalparts;

    // =====Go through all particles:=====
    for(unsigned int p=0; p<event.mc()->particles().size(); p++)
    {
        const MCParticleFormat *CurrentP = &(event.mc()->particles()[p]);
        allparts.push_back(CurrentP);

        // =====Find all final-state particles======
        // See http://arxiv.org/abs/hep-ph/0109068 p 6
        // –1 Incoming particle
        // +1 Outgoing final state particle
        // –2 Intermediate space-like propagator defining an x and Q 2 which should be preserved
        // +2 Intermediate resonance, Mass should be preserved
        // +3 Intermediate resonance, for documentation only 4
        // –9 Incoming beam particles at time t = −∞

        // final state particles have status code 1:
        if(!(CurrentP->statuscode() == 1)) continue;
        finalparts.push_back(CurrentP);

        // =====Find all final-state b partons======
        if(!(CurrentP->pdgid() == 5)) continue;
        bparton.push_back(CurrentP);
        double ptBparton = CurrentP->momentum().Pt();
        Manager()->FillHisto("pTb_Parton",     ptBparton);
    }


    int NbParton = bparton.size();

    // =====Get the missing ET=====
    TLorentzVector pTmiss = event.mc()->MET().momentum();
    double MET = pTmiss.Pt();


    // Fill histo with number of b jets:
    Manager()->FillHisto("NbJets_Parton",               NbParton);
    // =====finished=====
    return true;
  }
}

