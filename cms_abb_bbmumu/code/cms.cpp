#include "SampleAnalyzer/User/Analyzer/cms_nmssm_bba2mu.h"
using namespace MA5;
using namespace std;

// Define =====================================================
// =====some utilities to be used in the analysis ======
// ========================================================

double calcSumPt(const RecLeptonFormat* mylepton, double coneSize)
// See ihttp://arxiv.org/abs/1405.1617 p123 and
// http://arxiv.org/abs/1405.3982 p7,8
{
  double sumPt_ = 0;
  for(unsigned int c=0; c<mylepton->isolCones().size(); c++)
  {
      if(!(fabs(mylepton->isolCones()[c].deltaR() - coneSize)<0.0001)) continue;
      sumPt_ = mylepton->isolCones()[c].sumPT();
  }
  return sumPt_;
}


// ===============================================================
// Initialize =====================================================
// function called one time at the beginning of the analysis ======
// ===============================================================
bool cms_nmssm_bba2mu::Initialize(const MA5::Configuration& cfg, const std::map<std::string,std::string>& parameters)
{
  cout << "========================================\n";
  cout << "Analysis: CMS-NMSSM-BBA2MU\n";
  cout << "Written with MA5 version 1.1.11 patch1b\n";
  cout << "Authors: J Sonneveld\n";
  cout << "Contact e-mail: sonneveld@physik.rwth-aachen.de\n";
  cout << "========================================\n";

  cout << "BEGIN Initialization" << endl;
  // =====Declare the signal regions in this analysis=====
  // There is only one in this case
  Manager()->AddRegionSelection("Baseline");

  // =====Declare the baseline selection cuts=====
  Manager()->AddCut("first_muon");
  Manager()->AddCut("second_muon");
  Manager()->AddCut("N_b_jets>0");
  Manager()->AddCut("MET<40");



  // =====Declare the SR-defining cuts=====
  string SR_base[] =
    {
      "Baseline"
    };
  Manager()->AddCut("BaselineMET<40",SR_base);

  // =====Define histograms=====

  // ===============================
  // =======    JETS      ==========
  // ===============================
  // =====Number of jets=====
  Manager()->AddHisto("N_jets_-_no_cuts",16,-0.5,15.5);
  Manager()->AddHisto("N_jets_-_first_muon_cut",16,-0.5,15.5);
  Manager()->AddHisto("N_jets_-_second_muon_cut",16,-0.5,15.5);
  Manager()->AddHisto("N_jets_-_N_b_jets>0",16,-0.5,15.5);
  Manager()->AddHisto("N_jets_-_MET<40",16,-0.5,15.5);

  // ===============================
  // ====== b QUARKS/JETS ==========
  // ===============================
  // =====Number of b quarks at parton level=====
  Manager()->AddHisto("N_bs_MC_-_no_cuts",16,-0.5,15.5);

  // =====Number of b jets=====
  Manager()->AddHisto("N_b_jets_-_no_cuts",16,-0.5,15.5);
  Manager()->AddHisto("N_b_jets_-_first_muon_cut",16,-0.5,15.5);
  Manager()->AddHisto("N_b_jets_-_second_muon_cut",16,-0.5,15.5);
  Manager()->AddHisto("N_b_jets_-_N_b_jets>0",16,-0.5,15.5);
  Manager()->AddHisto("N_b_jets_-_MET<40",16,-0.5,15.5);

  // =====pT of b quarks at parton level=====
  Manager()->AddHisto("pT_b_MC_-_no_cuts",40,0,200);

  // =====pT of b jets=====
  Manager()->AddHisto("pT_b_jets_-_no_cuts",40,0,200);
  Manager()->AddHisto("pT_hardest_b_-_no_cuts",40,0,200);
  Manager()->AddHisto("pT_hardest_b_-_first_muon_cut",40,0,200);
  Manager()->AddHisto("pT_hardest_b_-_second_muon_cut",40,0,200);
  Manager()->AddHisto("pT_hardest_b_-_N_b_jets>0",40,0,200);
  Manager()->AddHisto("pT_hardest_b_-_MET<40",40,0,200);

  // =====eta of b jets=====
  Manager()->AddHisto("eta_b_-_no_cuts",20,-5,5);

  // ===============================
  // =======    MUONS     ==========
  // ===============================
  // =====pT of muons=====
  Manager()->AddHisto("pT_muon_no_iso",40,0,200);
  Manager()->AddHisto("pT_muon_-_no_cuts",40,0,200);
  //Manager()->AddHisto("pT_muon_-_first_muon_cut",40,0,200);
  //Manager()->AddHisto("pT_muon_-_second_muon_cut",40,0,200);
  //Manager()->AddHisto("pT_muon_-_N_b_jets>0",40,0,200);
  //Manager()->AddHisto("pT_muon_-_MET<40",40,0,200);

  // =====eta of muons=====
  Manager()->AddHisto("eta_muon_no_iso",20,-5,5);
  Manager()->AddHisto("eta_muon_-_no_cuts",20,-5,5);
  //Manager()->AddHisto("eta_muon_-_first_muon_cut",20,-5,5);
  //Manager()->AddHisto("eta_muon_-_second_muon_cut",20,-5,5);
  //Manager()->AddHisto("eta_muon_-_N_b_jets>0",20,-5,5);
  //Manager()->AddHisto("eta_muon_-_MET<40",20,-5,5);

  // =====Number of muons=====
  Manager()->AddHisto("N_muons_no_iso",16,-0.5,15.5);
  Manager()->AddHisto("N_muons_-_no_cuts",16,-0.5,15.5);
  Manager()->AddHisto("N_muons_-_first_muon_cut",16,-0.5,15.5);
  Manager()->AddHisto("N_muons_-_second_muon_cut",16,-0.5,15.5);
  Manager()->AddHisto("N_muons_-_N_b_jets>0",16,-0.5,15.5);
  Manager()->AddHisto("N_muons_-_MET<40",16,-0.5,15.5);

  // ===============================
  // =======      MET     ==========
  // ===============================
  // =====MET=====
  Manager()->AddHisto("MET_-_no_cuts",40,0,200);
  Manager()->AddHisto("MET_-_first_muon_cut",40,0,200);
  Manager()->AddHisto("MET_-_second_muon_cut",40,0,200);
  Manager()->AddHisto("MET_-_N_b_jets>0",40,0,200);
  Manager()->AddHisto("MET_-_MET<40",40,0,200);



  cout << "END   Initialization" << endl;

  return true;
}

// ==========================================================
// Finalize ==================================================
// function called one time at the end of the analysis =======
// ==========================================================
void cms_nmssm_bba2mu::Finalize(const SampleFormat& summary, const std::vector<SampleFormat>& files)
{
  cout << "BEGIN Finalization" << endl;
  cout << "not really doing anything"<<endl;//could save histos here, where's this done?
  cout << "END   Finalization" << endl;
  return;
}


// ====================================================
// Execute =============================================
// function called each time one event is read =========
// ====================================================
bool cms_nmssm_bba2mu::Execute(SampleFormat& sample, const EventFormat& event)
{
  // ==========================================
  // Give event a weight so it is counted  ====
  // ==========================================

  // Give the event a weight so that it is counted:
  double myEventWeight;
  if(Configuration().IsNoEventWeight()) myEventWeight=1.;
  else if(event.mc()->weight()!=0.) myEventWeight=event.mc()->weight();
  else
    {
      INFO << "Found one event with a zero weight. Skipping...\n";
      return false;
    }
  Manager()->InitializeForNewEvent(myEventWeight);


  // ==========================================
  // Collect parton level info    =============
  // ==========================================

  EventFormat myEvent;
  myEvent = event;
  unsigned int n = event.mc()->particles().size();
  int NbParton = 0;

  for(unsigned int i=0; i<n; i++)
  {
          MCParticleFormat* part = &myEvent.mc()->particles()[i];
          if(part->pdgid() == 5)
          { // bquark
            double pTbparton = part->momentum().Pt();
            //cout << "PT b parton:" << pTbparton << endl;
            Manager()->FillHisto("pT_b_MC_-_no_cuts",     pTbparton);
            NbParton ++;
          }
  }
  //cout << "Number of bs:" << NbParton << endl;
  Manager()->FillHisto("N_bs_MC_-_no_cuts",               NbParton);

  // Example of a cut at MC level (HEPinfo contained in ROOT file):
  //if(!Manager()->ApplyCut((sum_MET.Pt() > 80.),"MET Filter > 80"))
  //return true;


  // ==========================================
  // Start analyzing reco level event =========
  // ==========================================


  if (event.rec()!=0)
    {
    // ==========================================
    // Define collections of objects =============
    // ==========================================

    // =====first declare the empty containers:=====
    vector<const RecLeptonFormat*> isoElectron, isoMuonTight, isoMuonLoose, nonisoMuons;
    vector<const RecJetFormat*> bJet, allbs;

    // ======Collect parton level bquarks for histograms=====
    //  // whereas for e.g. event.rec()->jets() a vector of
    //  // RecJetFormat objects is returned, for
    //  // event.rec()->MCBquarks() a vector of pointers to
    //  // MCParticleFormat is returned.
    //  was not working, see above

    // =====fill the muon containers:=====
    int Nmu_all = event.rec()->muons().size();
    int Nmu = 0;

    for(unsigned int m=0; m<event.rec()->muons().size(); m++)
    {
      const RecLeptonFormat *CurrentMuon = &(event.rec()->muons()[m]);

      // Muon pt and eta
      double pt = CurrentMuon->momentum().Pt();
      double eta = CurrentMuon->momentum().Eta();

      // Fill Histos
      Manager()->FillHisto("pT_muon_no_iso",       pt);
      Manager()->FillHisto("eta_muon_no_iso",     eta);

      // Muon isolation criterium:
      // If sum pT inside cone of size 0.3 is less than
      // 0.1 times the muon pT
      //double sumpt = calcSumPt(CurrentMuon, 0.3);
      //if(!(sumpt/pt < 0.1)) continue; // Not a muon

      //==============================================
      //============== Suggestion muon iso from 
      //  -http://arxiv.org/abs/1405.3982 ========
      //==============================================

      for(unsigned int j=0; j<CurrentMuon->isolCones().size(); j++)
      {
        const IsolationConeType *cone =
                &CurrentMuon->isolCones()[j];
        // Make sure cone size (deltaR) is 0.3:
        if(std::fabs(cone->deltaR()-0.3)<1e-3)
        {
          // If sum of pT inside this cone is > 0.1*pT(muon)
          // count this muon as not isolated:
          if(cone->sumPT()/CurrentMuon->momentum().Pt()>.10)
              nonisoMuons.push_back(CurrentMuon);
        }
      }
      // If the muon was not isolated, skip this muon:
      if(!(nonisoMuons.size() == 0)) continue;
      //==============================================

      // Continue with isolated muons:
      Nmu ++;

      // Fill Histos
      Manager()->FillHisto("pT_muon_-_no_cuts",       pt);
      Manager()->FillHisto("eta_muon_-_no_cuts",     eta);

      // Find muons that satisfy loose criteria
      if(!(pt > 5.0)) continue;
      if(!(fabs(eta) < 2.4)) continue;

      // Muon got through all loose criteria
      isoMuonLoose.push_back(CurrentMuon);

      // Find muons that satisfy tight criteria as well
      if(!(pt > 25.0)) continue;
      if(!(fabs(eta) < 2.1)) continue;
      isoMuonTight.push_back(CurrentMuon);
    }

    // Define variables for histograms:
    int NJets = 0;

    // =====fill the jet containers=====
    for(unsigned int j=0; j<event.rec()->jets().size(); j++)
    {
      const RecJetFormat *CurrentJet = &(event.rec()->jets()[j]);
      double pt = CurrentJet->momentum().Pt();
      double eta = CurrentJet->momentum().Eta();

      //To fill histograms with number of jets and pt:
      NJets++;

      // To fill histogram with pT of bs:

      if(!(CurrentJet->btag())) continue;
      allbs.push_back(CurrentJet);
      Manager()->FillHisto("pT_b_jets_-_no_cuts",     CurrentJet->momentum().Pt());
      Manager()->FillHisto("eta_b_-_no_cuts",     eta);

      // Criterium for at least one bjet:
      if(!(pt > 30.0)) continue;
      if(!(fabs(eta) < 2.4)) continue;
      bJet.push_back(CurrentJet);
    }


    // Collect number of bs for histograms:
    int NbJets = allbs.size();

    // Collect pt of two hardest b jets for histograms:
    SORTER->sort(allbs);
    double pTb1 = -1;
    double pTb2 = -1;
    if(NbJets > 0) pTb1 = allbs[0]->momentum().Pt();
    if(NbJets > 1) pTb2 = allbs[1]->momentum().Pt();

    // =====Get the missing ET=====
    TLorentzVector pTmiss = event.rec()->MET().momentum();
    double MET = pTmiss.Pt();

    // ==========================================
    // Apply the "baseline selection" cuts =======
    // ==========================================

    // =====Before Any Cuts=====
    Manager()->FillHisto("N_muons_no_iso",                  Nmu_all);
    Manager()->FillHisto("N_muons_-_no_cuts",                   Nmu);
    Manager()->FillHisto("N_jets_-_no_cuts",                  NJets);
    Manager()->FillHisto("N_b_jets_-_no_cuts",               NbJets);
    Manager()->FillHisto("N_muons_-_no_cuts",                   Nmu);
    Manager()->FillHisto("MET_-_no_cuts",                       MET);
    if(pTb1>-1) Manager()->FillHisto("pT_hardest_b_-_no_cuts",         pTb1);

    // =====Apply first muon cut========
    if(!Manager()->ApplyCut((isoMuonTight.size()>0),"first_muon")) return true;
    Manager()->FillHisto("N_jets_-_first_muon_cut",               NJets);
    Manager()->FillHisto("N_b_jets_-_first_muon_cut",             NbJets);
    Manager()->FillHisto("N_muons_-_first_muon_cut",                Nmu);
    Manager()->FillHisto("MET_-_first_muon_cut",                   MET);
    if(pTb1>-1) Manager()->FillHisto("pT_hardest_b_-_first_muon_cut",      pTb1);

    // =====Apply second muon cut============
    if(!Manager()->ApplyCut((isoMuonLoose.size()>1),"second_muon")) return true;
    Manager()->FillHisto("N_jets_-_second_muon_cut",              NJets);
    Manager()->FillHisto("N_b_jets_-_second_muon_cut",            NbJets);
    Manager()->FillHisto("N_muons_-_second_muon_cut",               Nmu);
    Manager()->FillHisto("MET_-_second_muon_cut",                  MET);
    if(pTb1>-1) Manager()->FillHisto("pT_hardest_b_-_second_muon_cut",     pTb1);

    // =====Apply b-jet cut============
    if(!Manager()->ApplyCut((bJet.size()>0),"N_b_jets>0")) return true;
    Manager()->FillHisto("N_jets_-_N_b_jets>0",                    NJets);
    Manager()->FillHisto("N_b_jets_-_N_b_jets>0",                  NbJets);
    Manager()->FillHisto("N_muons_-_N_b_jets>0",                     Nmu);
    Manager()->FillHisto("MET_-_N_b_jets>0",                        MET);
    if(pTb1>-1) Manager()->FillHisto("pT_hardest_b_-_N_b_jets>0",           pTb1);

    // =====Apply MET40 cut===========
    if(!Manager()->ApplyCut((MET < 40),"MET<40")) return true;
    Manager()->FillHisto("N_jets_-_MET<40",                  NJets);
    Manager()->FillHisto("N_b_jets_-_MET<40",                NbJets);
    Manager()->FillHisto("N_muons_-_MET<40",                   Nmu);
    Manager()->FillHisto("MET_-_MET<40",                      MET);
    if(pTb1>-1) Manager()->FillHisto("pT_hardest_b_-_MET<40",         pTb1);

    // =====Select signal region Baseline:=======
    if(!Manager()->ApplyCut((MET < 40),"BaselineMET<40")) return true;

    // =====finished=====
    return true;
  }
}

