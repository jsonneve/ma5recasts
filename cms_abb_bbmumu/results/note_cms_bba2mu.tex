\documentclass[12pt,A4paper]{article}
%\pdfoutput=1

\usepackage{cite}
\usepackage{hyperref}
\usepackage{slashed}
\usepackage{graphicx}
    \usepackage{amsmath}
    \usepackage{caption}
    \usepackage{subcaption}
    \usepackage{cancel}
    \usepackage{etoolbox} % provides \patchcmd macro
    \makeatletter % modify the "headings" page style
    \patchcmd{\ps@headings}{{\slshape
ightmark}\hfil	hepage}{	hepage\hfil}{}{}
    \makeatother
    \pagestyle{headings}
\usepackage{listings}
\usepackage{color}
\usepackage{appendix}

\textwidth 170mm
\textheight 220mm
\oddsidemargin -5mm
\evensidemargin 5mm
\topmargin -16pt

\newcommand{\mulobs}{$\sigma^{\mathrm{UL}}_{\mathrm{obs}}$}
\newcommand{\ulobs}{\sigma^{\mathrm{UL}}_{\mathrm{obs}}}
\newcommand{\mulexp}{$\sigma^{\mathrm{UL}}_{\mathrm{exp}}$}
\newcommand{\ulexp}{\sigma^{\mathrm{UL}}_{\mathrm{exp}}}
\newcommand{\mtheo}{$\sigma^{\mathrm{pred}}_{\mathrm{theo}}$}
\newcommand{\theo}{\sigma_{\mathrm{pred}}^{\mathrm{theo}}}

\newcommand{\lumi}{\mathcal{L}}
\newcommand{\twomm}{\hspace{2mm}}
\newcommand{\neu}{{\widetilde{\chi}_1^0}}

\newcommand{\go}{{\widetilde{g}}}
\newcommand{\sq}{{\widetilde{q}}}
\newcommand{\mmsq}{$m_{\widetilde{q}}$}
\newcommand{\msq}{m_{\widetilde{q}}}
\newcommand{\mlsp}{m_{\widetilde{\chi}}}
\newcommand{\mmlsp}{$m_{\widetilde{\chi}}$}
\newcommand{\mgo}{m_{\widetilde{g}}}
\newcommand{\mmgo}{$m_{\widetilde{g}}$}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}
\lstset{ %
  language=C++,                % the language of the code
  basicstyle=\footnotesize,           % the size of the fonts that are used for the code
  %  numbers=left,                   % where to put the line-numbers
  %  numberstyle=\tiny\color{gray},  % the style that is used for the line-numbers
  %  stepnumber=2,                   % the step between two line-numbers. If it's 1, each line
  % will be numbered
  %  numbersep=5pt,                  % how far the line-numbers are from the code
  backgroundcolor=\color{white},      % choose the background color. You must add \usepackage{color}
  showspaces=false,               % show spaces adding particular underscores
  showstringspaces=false,         % underline spaces within strings
  showtabs=false,                 % show tabs within strings adding particular underscores
  frame=single,                   % adds a frame around the code
  rulecolor=\color{black},        % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. commens (green here))
  tabsize=2,                      % sets default tabsize to 2 spaces
  captionpos=b,                   % sets the caption-position to bottom
  breaklines=true,                % sets automatic line breaking
  breakatwhitespace=false,        % sets if automatic breaks should only happen at whitespace
  title=\lstname,                   % show the filename of files included with \lstinputlisting;
  % also try caption instead of title
  keywordstyle=\color{blue},          % keyword style
  commentstyle=\color{dkgreen},       % comment style
  stringstyle=\color{mauve},         % string literal style
  escapeinside={\%*}{*)},            % if you want to add a comment within your code
  morekeywords={*,\dots}               % if you want to add more keywords to the set
}


\title{Validation of the \texttt{MadAnalysis 5} implementation of CMS-XXX-XX-XXX}
\author{Jory Sonneveld (RWTH Aachen)\\
\normalsize {\it email: sonneveld@physik.rwth-aachen.de}
\date{\today}
}

\begin{document}
        \maketitle

%\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%    Setup   %%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Setup}
In this document, the \texttt{MadAnalysis 5} implementation of the search for a a light NMSSM pseudoscalar Higgs boson at the
LHC at $\sqrt{s}=8$ TeV
%\href{https://twiki.cern.ch/twiki/bin/view/CMSPublic/PhysicsResultsSUS12028}{CMS-SUS-12-028} (see also \href{http://arxiv.org/abs/1303.2985}{arXiv:1303.2985})
is validated%\footnote{
%A similar search was carried out at $\sqrt{s}=7$TeV, see \href{http://arxiv.org/abs/1210.7619}{arXiv:1210.7619}.
%The signature is discussed in the context of the NMSSM in \href{http://arxiv.org/abs/1011.6547}{arXiv:1011.6547} and \href{http://arxiv.org/abs/1101.1137}{arXiv:1101.1137}, for example.
%}.
.

NMSSM $\go \go \to b\bar{b}A$ production with $A\to\mu\mu$ is assumed.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
For the production process $\go \go \to b\bar{b}A$, the contributing diagrams are shown in \autoref{fig:gg_bba}.
\begin{figure}[]
    \centering
    \includegraphics[width=0.7\textwidth]{img/matrix11.jpg}
    \caption{The Feynman diagrams contributing to  $\go \go \to b\bar{b}A$.}
    \label{fig:gg_bba}
\end{figure}
The decay $A\to\mu\mu$ is assumed with a 100\% branching ratio; all other decays are disabled.

For validation of the search implemented in \texttt{MadAnalysis 5}, \texttt{Pythia 6.4} (the version in \texttt{MadGraph 1.5.12}) was used for simulation of $\go\go\to b\bar{b}A$, $A\to\mu\mu$ with input parameters given by CMS, whereafter detector simulated with the \texttt{MadAnalysis 5 v1.1.11 patch 1b} version of \texttt{Delphes} (\texttt{delphesMA5tune}).

The benchmark point was for a mass $m_A=30$ GeV.


The \texttt{Pythia 6.4} settings were read from an external card and are given in \autoref{appendix:pythia}.


In \texttt{MadAnalysis 5}, to run \texttt{DelphesMA5tune} the CMS settings 
with an altered version of this \href{http://madanalysis.irmp.ucl.ac.be/attachment/wiki/PhysicsAnalysisDatabase/delphesMA5tune_card_CMS_SUSY.tcl}{\texttt{DelphesMA5tune} card}, which can be found on the \texttt{MadAnalysis} \href{http://madanalysis.irmp.ucl.ac.be/attachment/wiki/PhysicsAnalysisDatabase/}{Physics Analysis Database} page, were used.
The parameter changed in the \texttt{DelphesMA5tune} card was the b-tag parameter to obtain a b-tag efficiency of 70\%, as shown in \autoref{appendix:delphesma5}.
Cut flows and histograms of several observables are compared with those of CMS in the next sections.
%In the last section, exclusions for the $A$ mass are compared.


\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%   Cut flow  %%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Cut flow}
The cuts of the analysis were a requirement of two muons, one $b$ jet, and small missing energy; they were as follows:
\begin{enumerate}
    \item First muon:
        \begin{itemize}
            \item $p_{\mathrm{T}}(\mu_1) > 25$ GeV;
            \item $|\eta(\mu_1)| < 2.1$;
        \end{itemize}
    \item Second muon:
        \begin{itemize}
            \item $p_{\mathrm{T}}(\mu_2) > 5$ GeV;
            \item $|\eta(\mu_2)| < 2.4$;
        \end{itemize}
    \item At least one $b$ jet:
        \begin{itemize}
            \item $p_{\mathrm{T}}(b) > 30$ GeV;
            \item $|\eta(b)| < 2.4$;
        \end{itemize}
    \item Missing energy $\slashed{E}_{\mathrm{T}} < 40$ GeV.
\end{enumerate}

The cut flow for the working point is given in \autoref{table:nmssm_bba2mu}
    \begin{table}[h!]
    \begin{centering}
    \begin{tabular}{ | l | l | r | }
%\hline
%    \texttt{T1bbbb} ($\mgo=900$, $\mlsp=500$; 0.060276 pb) & &\\
\hline
\input{nmssm_bba2mu_cutflow.tex}
        \hline
    \end{tabular}
    \caption{The cut flow
        for the baseline selection in CMS XXX-XX-XXX for
    the NMSSM working point $m_{A}=30$~GeV.
%    The second column is the official account as
%    reported by
%    https://twiki.cern.ch/twiki/pub/CMSPublic/PhysicsResultsSUS12028/T1qqqq.pdf,
 %   and our own results are given in column 3. The official counts are
    The counts are
    normalized to the luminosity ${\cal L}=20.2$/fb times cross section $\sigma= 1.276\times 10^{-3}$~pb
    corresponding to 25.77 events which are taken to be the count
    before any cuts.}
    \label{table:nmssm_bba2mu}
    \end{centering}
    \end{table}

\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%   Distributions of Observables  %%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Distributions of observables}
In \autoref{fig:nbmc} the number of $b$ quarks at parton level per Monte Carlo event is shown; this same number is shown for reconstructed level eventsin \autoref{fig:nbrec}.

\begin{figure}[]
    \centering
    \includegraphics[width=0.5\textwidth]{img/N_bs_MC_-_no_cuts.pdf}
    \caption{Number of $b$ quarks found in each event the Monte Carlo level event file (\texttt{Pythia 6.4} \texttt{STDHEP} file) for
    the NMSSM working point $m_{A}=30$~GeV.
}
    \label{fig:nbmc}
\end{figure}

\begin{figure}[]
    \centering
    \includegraphics[width=0.5\textwidth]{img/N_b_jets_-_no_cuts.pdf}
    \caption{Number of $b$ jets found in each event in the reconstructed level event file (\texttt{DelphesMA5tune} \texttt{ROOT} file) for
    the NMSSM working point $m_{A}=30$~GeV.
}
    \label{fig:nbrec}
\end{figure}

The transverse momenta of the $b$ jets is also shown for both Monte Carlo level and reconstructed level event files in \autoref{fig:ptbmc} and \autoref{fig:ptbrec}, respectively.
The distribution of transverse momenta of the hardest $b$ jets found for each event is shown in \autoref{fig:pthardestbrec}.
The distribution of missing transverse energy for each event at reconstructed level is shown in \autoref{fig:metrec}.

\begin{figure}[]
    \centering
    \includegraphics[width=0.5\textwidth]{img/pT_b_MC_-_no_cuts.pdf}
    \caption{Distribution of $p_{\mathrm{T}}$ of all $b$ quarks found in each event in the Monte Carlo level event file (\texttt{Pythia 6.4} \texttt{STDHEP} file) for
    the NMSSM working point $m_{A}=30$~GeV.
}
    \label{fig:ptbmc}
\end{figure}

\begin{figure}[]
    \centering
    \includegraphics[width=0.5\textwidth]{img/pT_b_jets_-_no_cuts.pdf}
    \caption{Distribution of $p_{\mathrm{T}}$ of $b$ jets in in the reconstructed level event file (\texttt{DelphesMA5tune} \texttt{ROOT} file) for
    the NMSSM working point $m_{A}=30$~GeV.
    \texttt{DelphesMA5tune} has cut on jets below 20 GeV.
}
    \label{fig:ptbrec}
\end{figure}

\begin{figure}[]
    \centering
    \includegraphics[width=0.5\textwidth]{img/pT_hardest_b_-_no_cuts.pdf}
    \caption{Distribution of $p_{\mathrm{T}}$ of the hardest $b$ jet in each event in the reconstructed level event file (\texttt{DelphesMA5tune} \texttt{ROOT} file) for
    the NMSSM working point $m_{A}=30$~GeV.
    \texttt{DelphesMA5tune} has cut on jets below 20 GeV.
}
    \label{fig:pthardestbrec}
\end{figure}

\begin{figure}[]
    \centering
    \includegraphics[width=0.5\textwidth]{img/MET_-_no_cuts.pdf}
    \caption{Distribution of $\slashed{E}_{\mathrm{T}}$ in each event in the reconstructed level event file (\texttt{DelphesMA5tune} \texttt{ROOT} file) for
    the NMSSM working point $m_{A}=30$~GeV.
}
    \label{fig:metrec}
\end{figure}

\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%% Appendices %%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\appendix
\appendixpage
\addappheadtotoc

%\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%% Pythia 6.4 %%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{\texttt{Pythia 6.4} settings}
\label{appendix:pythia}
The following settings in \texttt{Pythia 6.4} as used in \texttt{MadGraph 1.5.12} were read from an external card:
\lstset{language=fortran}
\begin{lstlisting}
!... pythia_card.dat
!...Parton showering on or off
      MSTP(61)=1
      MSTP(71)=1

!...Fragmentation/hadronization on or off
      MSTJ(1)=1

!...Multiple interactions on or off
      MSTP(81)=1

!...Don't stop execution after 10 errors
      MSTU(21)=1

!...Set pdf to cteq6l1:
      MSTP(51)=10042 ! structure function chosen (external PDF CTEQ6L1)
      MSTP(52)=2 ! work with LHAPDF
      LHAPATH=/path/to/lhapdf/PDFSets

!...More settings:
      MSTJ(22)=2 ! Decay those unstable particles
      PARJ(71)=10 . ! for which ctau 10 mm
      MSTP(33)=0 ! no K factors in hard cross sections
      MSTP(2)=1 ! which order running alphaS
      PARP(82)=1.921 ! pt cutoff for multiparton interactions
      PARP(89)=1800. ! sqrts for which PARP82 is set
      PARP(90)=0.227 ! Multiple interactions: rescaling power
      MSTP(96)=6 ! CR (color reconnection parameters)
      PARP(77)=1.016 ! CR
      PARP(78)=0.538 ! CR
      PARP(80)=0.1 ! Prob. colored parton from BBR
      PARP(83)=0.356 ! Multiple interactions: matter distribution parameter
      PARP(84)=0.651 ! Multiple interactions: matter distribution parameter
      PARP(62)=1.025 ! ISR cutoff
      MSTP(91)=1 ! Gaussian primordial kT
      PARP(93)=10.0 ! primordial kT-max
      MSTP(81)=21 ! multiple parton interactions 1 is Pythia default
      MSTP(82)=4 ! Defines the multi-parton model
\end{lstlisting}
In the pythia program \texttt{pythia.f}, the following settings were used:
\begin{lstlisting}
!... pythia.f
C... Model, process
        CALL PYGIVE('MSEL=0')
        CALL PYGIVE('MSUB(186)=1') ! gg->QQbarA (MSSM)
        CALL PYGIVE('KFPR(186,2)= 5') ! Q = b

C... Model params
            IMSS(4)= 2.     ! masses fixed by user',
            RMSS(5)= 30.   ! tan beta',
            RMSS(19)= 30. ! m_A',
            RMSS(1)= 100.  ! M1',
            RMSS(2)= 200.  ! M2',
            RMSS(3)= 800.  ! Mg',
            RMSS(4)= 200.  ! mu',
            RMSS(6)= 1000.  ! MS',
            RMSS(7)= 1000.  ! MS',
            RMSS(8)= 1000.  ! MS',
            RMSS(9)= 1000.  ! MS',
            RMSS(10)= 1000.  ! MS',
            RMSS(11)= 1000.  ! MS',
            RMSS(12)= 1000.  ! MS',
            RMSS(13)= 1000.  ! MS',
            RMSS(14)= 1000.  ! MS',
            RMSS(15)= 2000.  ! Ab',
            RMSS(16)= 2000.  ! At',
            RMSS(17)= 2000.  ! Atau',
            CALL PYGIVE('PMAS(36,1)=30.')  ! mA
C... Switch on decay of A -> mu+mu-:
            CALL PYGIVE('MDME(429,1)=1')  ! decay to mu+mu-',
C... Switch off all other decays:
            CALL PYGIVE('MDME(420,1)=0')  ! Higgs(H) decay into ddbar',
            CALL PYGIVE('MDME(421,1)=0')
            CALL PYGIVE('MDME(422,1)=0')
            CALL PYGIVE('MDME(423,1)=0')
            CALL PYGIVE('MDME(424,1)=0')
            CALL PYGIVE('MDME(425,1)=0')
            CALL PYGIVE('MDME(426,1)=0')
            CALL PYGIVE('MDME(427,1)=0')
            CALL PYGIVE('MDME(428,1)=0')
C... and these as well:
            CALL PYGIVE('MDME(430,1)=0')
            CALL PYGIVE('MDME(431,1)=0')
            ...
            CALL PYGIVE('MDME(502,1)=0')
\end{lstlisting}
The dots indicate the missing incrementing numbers from 432 up to and including 501.

\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%  DelphesMA5tune  %%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{\texttt{delphesMA5tune} settings}
\label{appendix:delphesma5}
In \texttt{MadAnalysis 5}, to run \texttt{DelphesMA5tune} the following settings were used:
\lstset{language=sh}
\begin{lstlisting}
ma5>set main.fastsim.package = delphesMA5tune
\end{lstlisting}
and
\begin{lstlisting}
ma5>set main.fastsim.detector = cms
\end{lstlisting}
The \href{http://madanalysis.irmp.ucl.ac.be/attachment/wiki/PhysicsAnalysisDatabase/delphesMA5tune_card_CMS_SUSY.tcl}{\texttt{DelphesMA5tune} card} used for this analysis can be found on the \texttt{MadAnalysis} \href{http://madanalysis.irmp.ucl.ac.be/attachment/wiki/PhysicsAnalysisDatabase/}{Physics Analysis Database} page.
It was adjusted to have an appropriate $b$-tag efficiency:
\lstset{language=sh}
\begin{lstlisting}
###########
# b-tagging
###########

module BTagging BTagging {
  set PartonInputArray Delphes/partons
  set JetInputArray JetEnergyScale/jets

  set BitNumber 0

  set DeltaR 0.5

  set PartonPTMin 1.0

  set PartonEtaMax 2.5

  # default efficiency formula (misidentification rate)
  add EfficiencyFormula {0} {0.01}

  # efficiency formula for c-jets (misidentification rate)
  add EfficiencyFormula {4} {0.20}

  # working point 60%
  # obtained from CMS-SUS-13-013, Section 9
  # efficiency formula for b-jets
  add EfficiencyFormula {5} {
                             (pt < 120.) * ((1.55e-6)*pt^3 + (-4.26e-4)*pt^2 + (0.0391)*pt + (-0.496)) + \
                             (pt >= 120.) * ((-3.26e-4)*pt + 0.7681)
                            }

}
\end{lstlisting}
%  # efficiency formula for b-jets 70%
%  add EfficiencyFormula {5} { 0.70 }



\end{document}
