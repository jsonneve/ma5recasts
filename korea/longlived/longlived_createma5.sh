#!/usr/bin/env zsh

name=JC_ll
analysis=cms_sus_16_022
ma5dir=/data/bin/korea/longlived/madanalysis5/
curdir=$PWD
cd ${ma5dir}Build
echo $name > tmpfile
echo $analysis >> tmpfile
cat tmpfile | ./bin/ma5 -E
ls ${name}/Build/SampleAnalyzer/User/Analyzer/
cd $curdir
