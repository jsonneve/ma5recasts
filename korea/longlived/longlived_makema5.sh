#!/usr/bin/env zsh

name=JC_ll
analysis=cms_sus_16_022
ma5dir=/data/bin/korea/longlived/madanalysis5/${analysis}/
codedir=/home/jory/korearecasts/cms_exo_16_022/

source /data/bin/korea/buildroot/bin/thisroot.sh
curdir=$PWD
cd ${ma5dir}Build
source setup.sh
cd ${ma5dir}Build/SampleAnalyzer/User/Analyzer
cp ${codedir}${name}.* .
cd ${ma5dir}Build
make
cd $curdir
