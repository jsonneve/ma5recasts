#!/usr/bin/env zsh

analysis=cms_sus_16_022
rootfiledir=/data/bin/korea/outputfiles/cms_exo_16_022/
ma5dir=/data/bin/korea/madanalysis5/${analysis}

for filename in $(ls ${rootfiledir}*root)
    do
        filename_noextension=$filename[0,-6]
        filename_short=$(basename ${filename_noextension})
        echo $(realpath $filename) '>'  ${ma5dir}Input/$filename_short
        echo $(realpath $filename) > ${ma5dir}Input/$filename_short
    done