#!/usr/bin/env zsh


source /data/bin/korea/buildroot/bin/thisroot.sh
pythiadir=/data/bin/korea/pythia8226/
carddir=/home/jory/korearecasts/cms_exo_16_022/cards/
pythiacmd=${carddir}main42_ll.cmnd
model=${carddir}sps1a_ll.spc
delphescard=${carddir}ma5_cms.tcl
outputfiledir=/data/bin/korea/outputfiles/cms_exo_16_022/

# copy pythia cmd and model to pythia examples:
cp ${pythiacmd} ${pythiadir}examples/main42.cmnd
cp ${model} ${pythiadir}examples

# check for existing files
hepfile=${outputfiledir}ll.hepmc
rootfile=${outputfiledir}ll.root
if [[ -a $hepfile ]]
then
    echo ''
    echo '******************************************************************'
    echo ''
    echo $hepfile exists! Moving it to ${hepfile}.bak
    echo ''
    echo '******************************************************************'
    echo ''
    mv $hepfile ${hepfile}.bak
fi
if [[ -a $rootfile ]]
then
    echo ''
    echo '******************************************************************'
    echo ''
    echo $rootfile exists! Moving it to ${rootfile}.bak
    echo ''
    echo '******************************************************************'
    echo ''
    mv $rootfile ${rootfile}.bak
fi

# Go to pythia dir to create hepmc
curdir=$PWD
cd ${pythiadir}examples
make main42
./main42 sps1a_ll.spc ll.hepmc
mv ll.hepmc ${hepfile}

# Run Delphes
./DelphesHepMC $delphescard $rootfile $hepfile

echo ''
echo '******************************************************************'
echo ''
echo Done running delphes -- you can find the root file in $rootfile
echo ''
echo '******************************************************************'
echo ''

cd $curdir







