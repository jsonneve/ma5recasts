#!/usr/bin/env zsh

name=JC_ll
analysis=cms_sus_16_022
ma5dir=/data/bin/korea/madanalysis5/${analysis}
codedir=/home/jory/korearecasts/cms_exo_16_022/
inputdir=${ma5dir}Input

curdir=$PWD
cd ${ma5dir}Build
source setup.sh
for modelpoint in $(ls $inputdir/*)
    do
        ./MadAnalysis5job $modelpoint
    done
cd $curdir


