db = 'madaneff.db'
tb = 'binnos_madan'
fi = '../alphat/binnames_alphat'
f = open(fi, 'r')
line = f.readline()
print line
# OUT: Njets2_3__Nb0__HT275_325
import sqlite3
conn = sqlite3.connect(db)
cur = conn.cursor()
binno = 0
binspec = line.strip()
binspec
# OUT: 'Njets2_3__Nb0__HT275_325'
an = 'CMS-SUS-12-028'
vals = (an, binno, binspec)
vals
# OUT: ('CMS-SUS-12-028', 0, 'Njets2_3__Nb0__HT275_325')
query = 'insert into ' + tb + ' values(?, ?, ?)'
query
# OUT: 'insert into binnos_madan values(?, ?, ?)'
cur.execute(query, vals)
# OUT: <sqlite3.Cursor object at 0x7f879b2de110>
conn.commit()
while line != '':
    binno += 1
    line = f.readline()
    vals = (an, binno, line.strip())
    cur.execute(query, vals)
    conn.commit()
