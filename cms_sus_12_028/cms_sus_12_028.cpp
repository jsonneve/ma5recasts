#include "SampleAnalyzer/User/Analyzer/cms_sus_12_028.h"
using namespace MA5;
using namespace std;

// Define =====================================================
// =====some utilities to be used in the analysis ======
// ========================================================

double calcSumPt(const RecLeptonFormat* mylepton, double coneSize)
{
  double sumPt_ = 0;
  for(unsigned int c=0; c<mylepton->isolCones().size(); c++)
    {
      if(!(fabs(mylepton->isolCones()[c].deltaR() - coneSize)<0.0001)) continue;
      sumPt_ = mylepton->isolCones()[c].sumPT();
    }
  return sumPt_;
}


double calcSumPtPhoton(const RecPhotonFormat* myphoton, double coneSize)
{
  double sumPt_ = 0;
  for(unsigned int c=0; c<myphoton->isolCones().size(); c++)
    {
      if(!(fabs(myphoton->isolCones()[c].deltaR() - coneSize)<0.0001)) continue;
      sumPt_ = myphoton->isolCones()[c].sumPT();
    }
  return sumPt_;
}

/*
double alphaTdijet(const RecJetFormat* myJets)
{
      double sum_et = 0;
      double sum_pt_x = 0;
      double sum_pt_y = 0;
      sum_et = myJets[0]->et() + myJets[1]->et();
      sum_pt_x = myJets[0]->pt()*math.cos(myJets[0]->phi());
      sum_pt_x += myJets[1]->pt()*math.cos(myJets[1]->phi());
      sum_pt_y = myJets[0]->pt()*math.sin(myJets[0]->phi());
      sum_pt_y += myJets[1]->pt()*math.sin(myJets[1]->phi());
      double m_transverse = math.sqrt(sum_et**2 - sum_pt_x**2 - sum_pt_y**2);
      return myJets[1]->et()/m_transverse;
}

double alphaTpseudojet(const RecJetFormat* myjets, double MHT, double HT)
{
    return 0.5 * (1.0 - myJets->deltaHT() / HT )/math.sqrt(1 - MHT**2/HT**2);
}

double deltaHT(const RecJetFormat* myjets)
{
    def combinations(iterable, r):
    # combinations('ABCD', 2) --> AB AC AD BC BD CD
    # combinations(range(4), 3) --> 012 013 023 123
    pool = tuple(iterable)
    n = len(pool)
    if r > n:
        return
    indices = range(r)
    yield tuple(pool[i] for i in indices)
    while True:
        for i in reversed(range(r)):
            if indices[i] != i + n - r:
                break
        else:
            return
        indices[i] += 1
        for j in range(i+1, r):
            indices[j] = indices[j-1] + 1
        yield tuple(pool[i] for i in indices)
}
*/


// ===============================================================
// Initialize =====================================================
// function called one time at the beginning of the analysis ======
// ===============================================================
bool cms_sus_12_028::Initialize(const MA5::Configuration& cfg, const std::map<std::string,std::string>& parameters)
{
  cout << "========================================\n";
  cout << "Analysis: CMS-SUS-12-028, arXiv:1303.2985 "
       << "(alphaT)\n";
  cout << "Written with MA5 version 1.1.11\n";
  cout << "Authors: J. Sonneveld \n";
  cout << "Contact e-mails: sonneveld@physik.rwth-aachen.de\n";
  cout << "DOI: xx.yyyy/zzz\n";
  cout << "Please cite arXiv.YYMM.NNNN\n"; //TODO: add arXiv ref
  cout << "========================================\n";

  cout << "BEGIN Initialization" << endl; 
  // =====Declare the 36 signal regions in this analysis=====
  Manager()->AddRegionSelection("Njets2-3, Nb0, HT275-325");
  Manager()->AddRegionSelection("Njets2-3, Nb0, HT325-375");
  Manager()->AddRegionSelection("Njets2-3, Nb0, HT375-475");
  Manager()->AddRegionSelection("Njets2-3, Nb0, HT475-575");
  Manager()->AddRegionSelection("Njets2-3, Nb0, HT575-675");
  Manager()->AddRegionSelection("Njets2-3, Nb0, HT675-775");
  Manager()->AddRegionSelection("Njets2-3, Nb0, HT775-875");
  Manager()->AddRegionSelection("Njets2-3, Nb0, HT>875");
  Manager()->AddRegionSelection("Njets2-3, Nb1, HT275-325");
  Manager()->AddRegionSelection("Njets2-3, Nb1, HT325-375");
  Manager()->AddRegionSelection("Njets2-3, Nb1, HT375-475");
  Manager()->AddRegionSelection("Njets2-3, Nb1, HT475-575");
  Manager()->AddRegionSelection("Njets2-3, Nb1, HT575-675");
  Manager()->AddRegionSelection("Njets2-3, Nb1, HT675-775");
  Manager()->AddRegionSelection("Njets2-3, Nb1, HT775-875");
  Manager()->AddRegionSelection("Njets2-3, Nb1, HT>875");
  Manager()->AddRegionSelection("Njets2-3, Nb2, HT275-325");
  Manager()->AddRegionSelection("Njets2-3, Nb2, HT325-375");
  Manager()->AddRegionSelection("Njets2-3, Nb2, HT375-475");
  Manager()->AddRegionSelection("Njets2-3, Nb2, HT475-575");
  Manager()->AddRegionSelection("Njets2-3, Nb2, HT575-675");
  Manager()->AddRegionSelection("Njets2-3, Nb2, HT675-775");
  Manager()->AddRegionSelection("Njets2-3, Nb2, HT775-875");
  Manager()->AddRegionSelection("Njets2-3, Nb2, HT>875");
  Manager()->AddRegionSelection("Njets>3, Nb0, HT275-325");
  Manager()->AddRegionSelection("Njets>3, Nb0, HT325-375");
  Manager()->AddRegionSelection("Njets>3, Nb0, HT375-475");
  Manager()->AddRegionSelection("Njets>3, Nb0, HT475-575");
  Manager()->AddRegionSelection("Njets>3, Nb0, HT575-675");
  Manager()->AddRegionSelection("Njets>3, Nb0, HT675-775");
  Manager()->AddRegionSelection("Njets>3, Nb0, HT775-875");
  Manager()->AddRegionSelection("Njets>3, Nb0, HT>875");
  Manager()->AddRegionSelection("Njets>3, Nb1, HT275-325");
  Manager()->AddRegionSelection("Njets>3, Nb1, HT325-375");
  Manager()->AddRegionSelection("Njets>3, Nb1, HT375-475");
  Manager()->AddRegionSelection("Njets>3, Nb1, HT475-575");
  Manager()->AddRegionSelection("Njets>3, Nb1, HT575-675");
  Manager()->AddRegionSelection("Njets>3, Nb1, HT675-775");
  Manager()->AddRegionSelection("Njets>3, Nb1, HT775-875");
  Manager()->AddRegionSelection("Njets>3, Nb1, HT>875");
  Manager()->AddRegionSelection("Njets>3, Nb2, HT275-325");
  Manager()->AddRegionSelection("Njets>3, Nb2, HT325-375");
  Manager()->AddRegionSelection("Njets>3, Nb2, HT375-475");
  Manager()->AddRegionSelection("Njets>3, Nb2, HT475-575");
  Manager()->AddRegionSelection("Njets>3, Nb2, HT575-675");
  Manager()->AddRegionSelection("Njets>3, Nb2, HT675-775");
  Manager()->AddRegionSelection("Njets>3, Nb2, HT775-875");
  Manager()->AddRegionSelection("Njets>3, Nb2, HT>875");
  Manager()->AddRegionSelection("Njets>3, Nb3, HT275-325");
  Manager()->AddRegionSelection("Njets>3, Nb3, HT325-375");
  Manager()->AddRegionSelection("Njets>3, Nb3, HT375-475");
  Manager()->AddRegionSelection("Njets>3, Nb3, HT475-575");
  Manager()->AddRegionSelection("Njets>3, Nb3, HT575-675");
  Manager()->AddRegionSelection("Njets>3, Nb3, HT675-775");
  Manager()->AddRegionSelection("Njets>3, Nb3, HT775-875");
  Manager()->AddRegionSelection("Njets>3, Nb3, HT>875");
  Manager()->AddRegionSelection("Njets>3, Nb>3, HT275-325");
  Manager()->AddRegionSelection("Njets>3, Nb>3, HT325-375");
  Manager()->AddRegionSelection("Njets>3, Nb>3, HT>375");

  //Declare the baseline selection cuts
  Manager()->AddCut("No Lepton");
  Manager()->AddCut("No Photon");
  Manager()->AddCut("Spurious Jet");
  Manager()->AddCut("HardJets>=2");
  //Manager()->AddCut("Njets>=2");
  Manager()->AddCut("HardestJetEta<=2.5");
  Manager()->AddCut("MHT-MET ratio<1.25");
  Manager()->AddCut("alphaT>0.55");
  Manager()->AddCut("HT>=275");


  
  //Declare the SR-defining cuts
  string SR_Njets2to3[] = 
    {
      "Njets2-3, Nb0, HT275-325",
      "Njets2-3, Nb0, HT325-375",
      "Njets2-3, Nb0, HT375-475",
      "Njets2-3, Nb0, HT475-575",
      "Njets2-3, Nb0, HT575-675",
      "Njets2-3, Nb0, HT675-775",
      "Njets2-3, Nb0, HT775-875",
      "Njets2-3, Nb0, HT>875",
      "Njets2-3, Nb1, HT275-325",
      "Njets2-3, Nb1, HT325-375",
      "Njets2-3, Nb1, HT375-475",
      "Njets2-3, Nb1, HT475-575",
      "Njets2-3, Nb1, HT575-675",
      "Njets2-3, Nb1, HT675-775",
      "Njets2-3, Nb1, HT775-875",
      "Njets2-3, Nb1, HT>875",
      "Njets2-3, Nb2, HT275-325",
      "Njets2-3, Nb2, HT325-375",
      "Njets2-3, Nb2, HT375-475",
      "Njets2-3, Nb2, HT475-575",
      "Njets2-3, Nb2, HT575-675",
      "Njets2-3, Nb2, HT675-775",
      "Njets2-3, Nb2, HT775-875",
      "Njets2-3, Nb2, HT>875",
    };
  Manager()->AddCut("NJets2-3",SR_Njets2to3);
  string SR_Njets4toInf[] = 
    {
      "Njets>3, Nb0, HT275-325",
      "Njets>3, Nb0, HT325-375",
      "Njets>3, Nb0, HT375-475",
      "Njets>3, Nb0, HT475-575",
      "Njets>3, Nb0, HT575-675",
      "Njets>3, Nb0, HT675-775",
      "Njets>3, Nb0, HT775-875",
      "Njets>3, Nb0, HT>875",
      "Njets>3, Nb1, HT275-325",
      "Njets>3, Nb1, HT325-375",
      "Njets>3, Nb1, HT375-475",
      "Njets>3, Nb1, HT475-575",
      "Njets>3, Nb1, HT575-675",
      "Njets>3, Nb1, HT675-775",
      "Njets>3, Nb1, HT775-875",
      "Njets>3, Nb1, HT>875",
      "Njets>3, Nb2, HT275-325",
      "Njets>3, Nb2, HT325-375",
      "Njets>3, Nb2, HT375-475",
      "Njets>3, Nb2, HT475-575",
      "Njets>3, Nb2, HT575-675",
      "Njets>3, Nb2, HT675-775",
      "Njets>3, Nb2, HT775-875",
      "Njets>3, Nb2, HT>875",
      "Njets>3, Nb3, HT275-325",
      "Njets>3, Nb3, HT325-375",
      "Njets>3, Nb3, HT375-475",
      "Njets>3, Nb3, HT475-575",
      "Njets>3, Nb3, HT575-675",
      "Njets>3, Nb3, HT675-775",
      "Njets>3, Nb3, HT775-875",
      "Njets>3, Nb3, HT>875",
      "Njets>3, Nb>3, HT275-325",
      "Njets>3, Nb>3, HT325-375",
      "Njets>3, Nb>3, HT>375",
    };
  Manager()->AddCut("NJets4toInf",SR_Njets4toInf);

  string SR_Nb0[] = 
    {
      "Njets2-3, Nb0, HT275-325",
      "Njets2-3, Nb0, HT325-375",
      "Njets2-3, Nb0, HT375-475",
      "Njets2-3, Nb0, HT475-575",
      "Njets2-3, Nb0, HT575-675",
      "Njets2-3, Nb0, HT675-775",
      "Njets2-3, Nb0, HT775-875",
      "Njets2-3, Nb0, HT>875",
      "Njets>3, Nb0, HT275-325",
      "Njets>3, Nb0, HT325-375",
      "Njets>3, Nb0, HT375-475",
      "Njets>3, Nb0, HT475-575",
      "Njets>3, Nb0, HT575-675",
      "Njets>3, Nb0, HT675-775",
      "Njets>3, Nb0, HT775-875",
      "Njets>3, Nb0, HT>875",
    };
  Manager()->AddCut("Nb0",SR_Nb0);
  string SR_Nb1[] = 
    {
      "Njets2-3, Nb1, HT275-325",
      "Njets2-3, Nb1, HT325-375",
      "Njets2-3, Nb1, HT375-475",
      "Njets2-3, Nb1, HT475-575",
      "Njets2-3, Nb1, HT575-675",
      "Njets2-3, Nb1, HT675-775",
      "Njets2-3, Nb1, HT775-875",
      "Njets2-3, Nb1, HT>875",
      "Njets>3, Nb1, HT275-325",
      "Njets>3, Nb1, HT325-375",
      "Njets>3, Nb1, HT375-475",
      "Njets>3, Nb1, HT475-575",
      "Njets>3, Nb1, HT575-675",
      "Njets>3, Nb1, HT675-775",
      "Njets>3, Nb1, HT775-875",
      "Njets>3, Nb1, HT>875",
    };
  Manager()->AddCut("Nb1",SR_Nb1);
  string SR_Nb2[] = 
    {
      "Njets2-3, Nb2, HT275-325",
      "Njets2-3, Nb2, HT325-375",
      "Njets2-3, Nb2, HT375-475",
      "Njets2-3, Nb2, HT475-575",
      "Njets2-3, Nb2, HT575-675",
      "Njets2-3, Nb2, HT675-775",
      "Njets2-3, Nb2, HT775-875",
      "Njets2-3, Nb2, HT>875",
      "Njets>3, Nb2, HT275-325",
      "Njets>3, Nb2, HT325-375",
      "Njets>3, Nb2, HT375-475",
      "Njets>3, Nb2, HT475-575",
      "Njets>3, Nb2, HT575-675",
      "Njets>3, Nb2, HT675-775",
      "Njets>3, Nb2, HT775-875",
      "Njets>3, Nb2, HT>875",
    };
  Manager()->AddCut("Nb2",SR_Nb2);
  string SR_Nb3[] = 
    {
      "Njets>3, Nb3, HT275-325",
      "Njets>3, Nb3, HT325-375",
      "Njets>3, Nb3, HT375-475",
      "Njets>3, Nb3, HT475-575",
      "Njets>3, Nb3, HT575-675",
      "Njets>3, Nb3, HT675-775",
      "Njets>3, Nb3, HT775-875",
      "Njets>3, Nb3, HT>875",
    };
  Manager()->AddCut("Nb3",SR_Nb3);
  string SR_Nb4toInf[] = 
    {
      "Njets>3, Nb>3, HT275-325",
      "Njets>3, Nb>3, HT325-375",
      "Njets>3, Nb>3, HT>375",
    };
  Manager()->AddCut("Nb>3",SR_Nb4toInf);

  string SR_HT275to325[] = 
    {
      "Njets2-3, Nb0, HT275-325",
      "Njets2-3, Nb1, HT275-325",
      "Njets2-3, Nb2, HT275-325",
      "Njets>3, Nb0, HT275-325",
      "Njets>3, Nb1, HT275-325",
      "Njets>3, Nb2, HT275-325",
      "Njets>3, Nb3, HT275-325",
      "Njets>3, Nb>3, HT275-325",
    };
  Manager()->AddCut("HT275-325",SR_HT275to325);
  string SR_HT325to375[] = 
    {
      "Njets2-3, Nb0, HT325-375",
      "Njets2-3, Nb1, HT325-375",
      "Njets2-3, Nb2, HT325-375",
      "Njets>3, Nb0, HT325-375",
      "Njets>3, Nb1, HT325-375",
      "Njets>3, Nb2, HT325-375",
      "Njets>3, Nb3, HT325-375",
      "Njets>3, Nb>3, HT325-375",
    };
  Manager()->AddCut("HT325-375",SR_HT325to375);
  string SR_HT375toInf[] = 
    {
      "Njets>3, Nb>3, HT>375",
    };
  Manager()->AddCut("HT>375",SR_HT375toInf);
  string SR_HT375to475[] = 
    {
      "Njets2-3, Nb0, HT375-475",
      "Njets2-3, Nb1, HT375-475",
      "Njets2-3, Nb2, HT375-475",
      "Njets>3, Nb0, HT375-475",
      "Njets>3, Nb1, HT375-475",
      "Njets>3, Nb2, HT375-475",
      "Njets>3, Nb3, HT375-475",
    };
  Manager()->AddCut("HT375-475",SR_HT375to475);
  string SR_HT475to575[] = 
    {
      "Njets2-3, Nb0, HT475-575",
      "Njets2-3, Nb1, HT475-575",
      "Njets2-3, Nb2, HT475-575",
      "Njets>3, Nb0, HT475-575",
      "Njets>3, Nb1, HT475-575",
      "Njets>3, Nb2, HT475-575",
      "Njets>3, Nb3, HT475-575",
    };
  Manager()->AddCut("HT475-575",SR_HT475to575);
  string SR_HT575to675[] = 
    {
      "Njets2-3, Nb0, HT575-675",
      "Njets2-3, Nb1, HT575-675",
      "Njets2-3, Nb2, HT575-675",
      "Njets>3, Nb0, HT575-675",
      "Njets>3, Nb1, HT575-675",
      "Njets>3, Nb2, HT575-675",
      "Njets>3, Nb3, HT575-675",
    };
  Manager()->AddCut("HT575-675",SR_HT575to675);
  string SR_HT675to775[] = 
    {
      "Njets2-3, Nb0, HT675-775",
      "Njets2-3, Nb1, HT675-775",
      "Njets2-3, Nb2, HT675-775",
      "Njets>3, Nb0, HT675-775",
      "Njets>3, Nb1, HT675-775",
      "Njets>3, Nb2, HT675-775",
      "Njets>3, Nb3, HT675-775",
    };
  Manager()->AddCut("HT675-775",SR_HT675to775);
  string SR_HT775to875[] = 
    {
      "Njets2-3, Nb0, HT775-875",
      "Njets2-3, Nb1, HT775-875",
      "Njets2-3, Nb2, HT775-875",
      "Njets>3, Nb0, HT775-875",
      "Njets>3, Nb1, HT775-875",
      "Njets>3, Nb2, HT775-875",
      "Njets>3, Nb3, HT775-875",
    };
  Manager()->AddCut("HT775-875",SR_HT775to875);
  string SR_HT875toInf[] = 
    {
      "Njets2-3, Nb0, HT>875",
      "Njets2-3, Nb1, HT>875",
      "Njets2-3, Nb2, HT>875",
      "Njets>3, Nb0, HT>875",
      "Njets>3, Nb1, HT>875",
      "Njets>3, Nb2, HT>875",
      "Njets>3, Nb3, HT>875",
    };
  Manager()->AddCut("HT>875",SR_HT875toInf);


  //histogram
  Manager()->AddHisto("NJets_METCleaning",16,-0.5,15.5);
  Manager()->AddHisto("NJets_NoLepton",16,-0.5,15.5);
  Manager()->AddHisto("NJets_NoPhoton",16,-0.5,15.5);
  Manager()->AddHisto("NJets_SpuriousJet",16,-0.5,15.5);
  Manager()->AddHisto("NJets_NJets>2",16,-0.5,15.5);
  Manager()->AddHisto("NJets_HardestJetEta<=2.5",16,-0.5,15.5);
  Manager()->AddHisto("NJets_HardJets>=2",16,-0.5,15.5);
  Manager()->AddHisto("NJets_MHTMETratio>1.25",16,-0.5,15.5);
  Manager()->AddHisto("NJets_alphaT>0.55",16,-0.5,15.5);
  Manager()->AddHisto("NJets_HT>275",16,-0.5,15.5);

  Manager()->AddHisto("HT_METCleaning",40,0,4000);
  Manager()->AddHisto("HT_NoLepton",40,0,4000);
  Manager()->AddHisto("HT_NoPhoton",40,0,4000);
  Manager()->AddHisto("HT_NJets>2",40,0,4000);
  Manager()->AddHisto("HT_HT>275",40,0,4000);
  Manager()->AddHisto("HT_HardestJetEta<=2.5",40,0,4000);
  Manager()->AddHisto("HT_HardJets>=2",40,0,4000);
  Manager()->AddHisto("HT_SpuriousJet",40,0,4000);
  Manager()->AddHisto("HT_MHTMETratio>1.25",40,0,4000);
  Manager()->AddHisto("HT_alphaT>0.55",40,0,4000);

  Manager()->AddHisto("MHT_METCleaning",30,0,1500);
  Manager()->AddHisto("MHT_NoLepton",30,0,1500);
  Manager()->AddHisto("MHT_NoPhoton",30,0,1500);
  Manager()->AddHisto("MHT_NJets>2",30,0,1500);
  Manager()->AddHisto("MHT_HT>275",30,0,1500);
  Manager()->AddHisto("MHT_HardestJetEta<=2.5",30,0,1500);
  Manager()->AddHisto("MHT_HardJets>=2",30,0,1500);
  Manager()->AddHisto("MHT_SpuriousJet",30,0,1500);
  Manager()->AddHisto("MHT_MHTMETratio>1.25",30,0,1500);
  Manager()->AddHisto("MHT_alphaT>0.55",30,0,1500);

  Manager()->AddHisto("alphaT_METCleaning",30,0,3);
  Manager()->AddHisto("alphaT_NoLepton",30,0,3);
  Manager()->AddHisto("alphaT_NoPhoton",30,0,3);
  Manager()->AddHisto("alphaT_NJets>2",30,0,3);
  Manager()->AddHisto("alphaT_HT>275",30,0,3);
  Manager()->AddHisto("alphaT_HardestJetEta<=2.5",30,0,3);
  Manager()->AddHisto("alphaT_HardJets>=2",30,0,3);
  Manager()->AddHisto("alphaT_SpuriousJet",30,0,3);
  Manager()->AddHisto("alphaT_MHTMETratio>1.25",30,0,3);
  Manager()->AddHisto("alphaT_alphaT>0.55",30,0,3);



  Manager()->AddHisto("HT_BL+NJets>3",40,0,4000);
  Manager()->AddHisto("NJets_BL+HT>875",16,-0.5,15.5);

  cout << "END   Initialization" << endl;
  
  return true;
}

// ==========================================================
// Finalize ==================================================
// function called one time at the end of the analysis =======
// ==========================================================
void cms_sus_12_028::Finalize(const SampleFormat& summary, const std::vector<SampleFormat>& files)
{
  cout << "BEGIN Finalization" << endl;
  cout << "not really doing anything"<<endl;//could save histos here, where's this done?
  cout << "END   Finalization" << endl;
  return;
}


// ====================================================
// Execute =============================================
// function called each time one event is read =========
// ====================================================
bool cms_sus_12_028::Execute(SampleFormat& sample, const EventFormat& event)
{
  double JEscale = 1;
  double myEventWeight;
  if(Configuration().IsNoEventWeight()) myEventWeight=1.;
  else if(event.mc()->weight()!=0.) myEventWeight=event.mc()->weight();
  else
    {
      INFO << "Found one event with a zero weight. Skipping...\n";
      return false;
    }
  Manager()->InitializeForNewEvent(myEventWeight);

  if (event.rec()!=0)
    {
      // ==========================================
      // Define collections of objects =============
    // ==========================================

    // =====first declare the empty containers:=====
    vector<const RecLeptonFormat*> isoElectron, isoMuon;
    vector<const RecPhotonFormat*> isoPhoton;
    vector<const RecJetFormat*> looseJet, tightJet, spuriousJet, bJet;


     // =====fill the electrons container:=====
    for(unsigned int e=0; e<event.rec()->electrons().size(); e++)
    {
      const RecLeptonFormat *CurrentElectron = &(event.rec()->electrons()[e]);
      double pt = CurrentElectron->momentum().Pt();
      if(!(pt>10)) continue;
      double eta = CurrentElectron->momentum().Eta();
      if(!(fabs(eta)<2.4)) continue;
      double sumpt = calcSumPt(CurrentElectron, 0.3);
      if(!(sumpt/pt < 0.2)) continue;
      isoElectron.push_back(CurrentElectron);
    }

    // =====fill the muons container:=====
    for(unsigned int m=0; m<event.rec()->muons().size(); m++)
    {
      const RecLeptonFormat *CurrentMuon = &(event.rec()->muons()[m]);
      double pt = CurrentMuon->momentum().Pt();
      if(!(pt>10)) continue;
      double eta = CurrentMuon->momentum().Eta();
      if(!(fabs(eta)<2.4)) continue;
      double sumpt = calcSumPt(CurrentMuon, 0.4);
      if(!(sumpt/pt < 0.15)) continue;
      isoMuon.push_back(CurrentMuon);
    }

    // =====fill the photons container:=====
    for(unsigned int p=0; p<event.rec()->photons().size(); p++)
    {
      const RecPhotonFormat *CurrentPhoton = &(event.rec()->photons()[p]);
      double pt = CurrentPhoton->momentum().Pt();
      if(!(pt>25)) continue;
      double eta = CurrentPhoton->momentum().Eta();
      if(!(fabs(eta)<2.4)) continue;
      double sumpt = calcSumPtPhoton(CurrentPhoton, 0.3);
      if(!(sumpt/pt < 0.15)) continue;
      isoPhoton.push_back(CurrentPhoton);
    }


    // =====fill the jet containers and order=====
    // --- Signal-region dependent cut; determine HT bin first ----
    //

    for(unsigned int j=0; j<event.rec()->jets().size(); j++)
    {
      const RecJetFormat *CurrentJet = &(event.rec()->jets()[j]);
      double pt = JEscale*CurrentJet->momentum().Pt();
      double eta = CurrentJet->momentum().Eta();
      double minET = 50;
      double minEThardest = 100;
      if(pt > minET && fabs(eta) > 3){
          spuriousJet.push_back(CurrentJet);
      }
      if(CurrentJet->btag()) bJet.push_back(CurrentJet);

      // Check the HT signal region and adjust minimum jet pT accordingly
      double HT = event.rec()->THT();
      if(325 <= HT && HT < 375){
          minET = 43;
          minEThardest = 87;
      }
      else if(275 <= HT && HT < 325){
          minET = 37;
          minEThardest = 73;
      }

      if(!(pt >= minET && fabs(eta) <= 3)) continue;
      looseJet.push_back(CurrentJet);
      // Only the hardest jet must have eta > 2.5;
      // In contrast, the TWO hardest jets must have 
      // ET larger than minEThardest
      if(!(pt >= minEThardest && fabs(eta) <= 3)) continue;
      tightJet.push_back(CurrentJet);
    }
    SORTER->sort(looseJet);
    SORTER->sort(tightJet);

    // =====Get the missing ET=====
    TLorentzVector pTmiss = event.rec()->MET().momentum();
    double MET = pTmiss.Pt();

    // =====Calculate the HT=====
    double HT = 0;
    for(unsigned int j = 0; j < looseJet.size(); j++)
      {
          // Note that in MHT this is tight jet
          HT += JEscale*looseJet[j]->momentum().Pt();
      }

    // =====Calculate the missing HT=====
    double MHT = 0;
    TLorentzVector MHT_vec = TLorentzVector();
    TLorentzVector SomeJet;
    for(unsigned int j = 0; j < looseJet.size(); j++)
      {
          SomeJet.SetPtEtaPhiE
            (looseJet[j]->momentum().Pt()*JEscale, looseJet[j]->momentum().Eta(), 
             looseJet[j]->momentum().Phi(), looseJet[j]->momentum().E()*JEscale);
          MHT_vec -= SomeJet;
      }
    MHT = MHT_vec.Pt();
    int NJets = looseJet.size();

    // ======Calculate the MHT/MET ratio======
    double MHTMETratio = MHT/MET;

    // ======Calculate the alphaT variable=====
    // Check if for a QCD sample alphaT drops at 0.5
    double alphaT = PHYSICS->Transverse->AlphaT(event.rec());
    /*
    double alphaT = 0;
    if(myJets.size() == 2)
    {
        alphaT = looseJet->alphaTdijet();
    } else {
        alphaT = PHYSICS->Transverse->AlphaT(event.rec());
        //alphaT = alphaT(looseJet, MHT, HT);
    }
    */

    // ==========================================
    // Apply the "baseline selection" cuts =======
    // ==========================================

  
    // =====Before Any Cuts=====
    Manager()->FillHisto("NJets_METCleaning",      NJets);
    Manager()->FillHisto("HT_METCleaning",            HT);
    Manager()->FillHisto("MHT_METCleaning",          MHT);
    Manager()->FillHisto("alphaT_METCleaning",    alphaT);

    // =====Apply no-lepton cut========
    if(!Manager()->ApplyCut((isoElectron.size()+isoMuon.size() == 0),"No Lepton")) return true;
    Manager()->FillHisto("NJets_NoLepton",         NJets);
    Manager()->FillHisto("HT_NoLepton",               HT);
    Manager()->FillHisto("MHT_NoLepton",             MHT);
    Manager()->FillHisto("alphaT_NoLepton",       alphaT);

    // =====Apply no-photon cut========
    if(!Manager()->ApplyCut((isoPhoton.size() == 0),"No Photon")) return true;
    Manager()->FillHisto("NJets_NoPhoton",         NJets);
    Manager()->FillHisto("HT_NoPhoton",               HT);
    Manager()->FillHisto("MHT_NoPhoton",             MHT);
    Manager()->FillHisto("alphaT_NoPhoton",       alphaT);


    // =====Apply Spurious Jet cut============
    if(!Manager()->ApplyCut((spuriousJet.size() == 0),"Spurious Jet")) return true; 
    Manager()->FillHisto("NJets_SpuriousJet",          NJets);
    Manager()->FillHisto("HT_SpuriousJet",                HT);
    Manager()->FillHisto("MHT_SpuriousJet",              MHT);
    Manager()->FillHisto("alphaT_SpuriousJet",        alphaT);


    // =====Apply NJets cut============
    //if(!Manager()->ApplyCut((tightJet.size() > 1),"Njets>=2")) return true; 
    //if(!Manager()->ApplyCut((event.rec()->jets().size() > 1),"Njets>=2")) return true; 
    //Manager()->FillHisto("NJets_NJets>2",          NJets);
    //Manager()->FillHisto("HT_NJets>2",                HT);
    //Manager()->FillHisto("MHT_NJets>2",              MHT);

    // =====Apply Hardest Jet cut============
    if(!Manager()->ApplyCut((tightJet.size() > 1),"HardJets>=2")) return true; 
    Manager()->FillHisto("NJets_HardJets>=2",          NJets);
    Manager()->FillHisto("HT_HardJets>=2",                HT);
    Manager()->FillHisto("MHT_HardJets>=2",              MHT);
    Manager()->FillHisto("alphaT_HardJets>=2",        alphaT);



    // =====Apply Hardest Jet Eta cut============
    if(!Manager()->ApplyCut((tightJet[0]->momentum().Eta() <= 2.5),"HardestJetEta<=2.5")) return true; 
    Manager()->FillHisto("NJets_HardestJetEta<=2.5",          NJets);
    Manager()->FillHisto("HT_HardestJetEta<=2.5",                HT);
    Manager()->FillHisto("MHT_HardestJetEta<=2.5",              MHT);
    Manager()->FillHisto("alphaT_HardestJetEta<=2.5",        alphaT);

    // =====Apply MHT/MET<1.25 cut===========
    if(!Manager()->ApplyCut((MHTMETratio < 1.25),"MHT-MET ratio<1.25")) return true;
    Manager()->FillHisto("NJets_MHTMETratio>1.25",          NJets);
    Manager()->FillHisto("HT_MHTMETratio>1.25",                HT);
    Manager()->FillHisto("MHT_MHTMETratio>1.25",              MHT);
    Manager()->FillHisto("alphaT_MHTMETratio>1.25",        alphaT);

    // =====Apply alphaT > 0.55 cut===========
    if(!Manager()->ApplyCut((alphaT > 0.55),"alphaT>0.55")) return true;
    Manager()->FillHisto("NJets_alphaT>0.55",          NJets);
    Manager()->FillHisto("HT_alphaT>0.55",                HT);
    Manager()->FillHisto("MHT_alphaT>0.55",              MHT);
    Manager()->FillHisto("alphaT_alphaT>0.55",        alphaT);

    // =====Apply HT275 cut============
    if(!Manager()->ApplyCut((HT > 275),"HT>=275")) return true;
    Manager()->FillHisto("NJets_HT>275",           NJets);
    Manager()->FillHisto("HT_HT>275",                 HT);
    Manager()->FillHisto("MHT_HT>275",               MHT);
    Manager()->FillHisto("alphaT_HT>275",         alphaT);




    // ==========================================
    // Fill Histograms for extra checking =======
    // ==========================================
    if(HT>875) Manager()->FillHisto("NJets_BL+HT>875",  NJets);
    if(NJets>3) Manager()->FillHisto("HT_BL+NJets>3",        HT);



    // ==========================================
    // Apply the Signal Region - dependent cuts =======
    // ==========================================
    Manager()->ApplyCut(2 <= NJets && NJets <=3, "NJets2-3");
    Manager()->ApplyCut(4 <= NJets, "NJets4toInf");
    Manager()->ApplyCut(bJet.size()==0, "Nb0");
    Manager()->ApplyCut(bJet.size()==1, "Nb1");
    Manager()->ApplyCut(bJet.size()==2, "Nb2");
    Manager()->ApplyCut(bJet.size()==3, "Nb3");
    Manager()->ApplyCut(bJet.size()>3, "Nb>3");
    Manager()->ApplyCut(275 <= HT && HT < 325, "HT275-325");
    Manager()->ApplyCut(325 <= HT && HT < 375, "HT325-375");
    Manager()->ApplyCut(375 <= HT && HT < 475, "HT375-475");
    Manager()->ApplyCut(475 <= HT && HT < 575, "HT475-575");
    Manager()->ApplyCut(575 <= HT && HT < 675, "HT575-675");
    Manager()->ApplyCut(675 <= HT && HT < 775, "HT675-775");
    Manager()->ApplyCut(775 <= HT && HT < 875, "HT775-875");
    Manager()->ApplyCut(875 <= HT, "HT>875");

    // =====finished=====
    return true;
  }
}

