import math
import itertools
from functions import *
import numpy as np
import logging
import socket
import decimal

#############################################
######## Save hostname for logging ##########
#############################################
if '.' in socket.gethostname():
    HOSTNAME = socket.gethostname()[:socket.gethostname().index('.')]
elif socket.gethostname().startswith('lxte') and socket.gethostname()[-1] in [str(i) for i in range(10)]:
    HOSTNAME = socket.gethostname()
elif socket.gethostname().startswith('top'):
    HOSTNAME = socket.gethostname()
else:
    print 'what kinda hostname  is this?!', socket.gethostname(), 'exiting line 17 from analysis_functions.py'
    sys.exit()

LOGFILE = HOSTNAME + 'default.log'

MESSAGE = ''

def lh(filetyp):
    """ (str) -> str
    Return the marker of start of event
    in the given filetyp.
    >>> lh('lhe')
    ['<event>', '</event>', '#']
    >>> lh('lhco')
    ['0']
    """
    # The character # is inserted in lhe before </event> when using matching.
    # It is a line containing information (2 numbers), don't know what.
    return ["<event>", "</event>", "#"] if filetyp == 'lhe' else ["0"]

def read_to_event(lh_file, filetyp, line):
    r""" (file opened for reading, str, str) -> str
    Read from lh file (filetyp lhe or lhco) until next event or end of
    file is reached.
    At that point, return the line.
    >>> testing = open('testeventreading', 'w')
    >>> testing.write('#Nothing\n  @ \n <event> \n ')
    >>> testing.close()
    >>> testing = open('testeventreading', 'r')
    >>> line = testing.readline()
    >>> lh_file, line = read_to_event(testing, 'lhe', line)
    >>> line
    ' <event> \n'
    >>> testing = open('testeventreading', 'w')
    >>> testing.write('#Nothing\n @ \n 0 \n ')
    >>> testing.close()
    >>> testing = open('testeventreading', 'r')
    >>> line = testing.readline()
    >>> lh_file, line = read_to_event(testing, 'lhco', line)
    >>> line
    ' 0 \n'
    """
    global event_number_current
    if filetyp == 'lhco':
        while line != '' and line.lstrip().split()[0] != lh(filetyp)[0]:
            line = lh_file.readline()
    else:
        while line != '' and (len(line.split()) < 1 or line.split()[0] != lh(filetyp)[0]):
            line = lh_file.readline()
        line = lh_file.readline()
    if filetyp == 'lhco':
        event_number_current = int(line.split()[1])
    return lh_file, line

def lhco_kin(line):
    """ (str) -> dict of float/int
    Read lhco file line and accumulate 
    kinematic information about particle 
    in a dictionary.
    >>> line = '  3   0  -1.349   2.574  39.886   0.000   0.000   0.000   0.211   0.000  0.000'
    >>> d = lhco_kin(line)
    >>> d['pt']
    39.886
    >>> d['had_em']
    0.211
    >>> d['typ']
    0
    """
    pt = float(line.split()[4])
    eta = float(line.split()[2])
    phi = float(line.split()[3])
    had_em = float(line.split()[8])
    particle_id = int(line.split()[1])
    minv = float(line.split()[5])
    px = pt * math.cos(phi)
    py = pt * math.sin(phi)
    b = float(line.split()[7])
    #pz = beam_momentum(pt, eta)
    #E = energy(minv, pt, pz)
    #etl = transverse_energy(E, pt, pz)
    et = transverse_energy_short(minv, eta, pt)
    return {'pt': pt, 'eta': eta, 'phi': phi, 'had_em': had_em, 'typ': particle_id, 'px': px, 'py': py, 'minv': minv, 'et': et, 'b': b}

def lhe_kin(line):
    """(str) -> dict(float/int)
    Read lhco file line and accumulate 
    kinematic information about particle 
    in a dictionary.
    >>> line = '      -2  2  5  5 0 0  0.30629192E+01  0.69490051E+00  0.13999229E+01  0.34544240E+01  0.33000000E+00 0. 0.'
    >>> d = lhe_kin(line)
    >>> d['px']
    3.0629192
    >>> d['hel']
    0
    >>> d['M']
    0.33
    >>> d['typ']
    -2
    """
    global MESSAGE
    px = float(line.split()[6])
    py = float(line.split()[7])
    pt = math.sqrt(px**2 + py**2)
    pz = float(line.split()[8])
    E = float(line.split()[9])
    M = float(line.split()[10])
    hel = int(line.split()[12][:-1])
    typ = int(line.split()[0])
    cat = int(line.split()[1])
    phi = math.atan2(py, px)
    et = transverse_energy(E, pt, pz)
    if pz > E:
        MESSAGE += "pz larger than E: " + str(pz) + ' ' + str(E) + '\n'
    # From definitions of Kinematics Review, PDG and
    # http://www.jthaler.net/olympicswiki/doku.php?id=lhc_olympics:data_file_format#kinematics
    eta = -math.log(abs(math.tan(math.acos(pz/math.sqrt(pz**2 + pt**2))))) if pt != 0 else 10.0
    # eta = math.atanh(pz/E) # for massless particles only
    return {'px': px, 'py': py, 'pz': pz, 'pt': pt, 'eta': eta, 'phi': phi, 'E': E, 'M': M, 'hel': hel, 'typ': typ, 'et': et, 'cat': cat}


def write_example_file(name):
    '''
    Write an example event.
    '''
    f = open(name, 'w')
    f.write('  0        998      0\n')
    f.write('  1   0  -0.391  -1.527 604.214   0.000   0.000   0.000   6.442   0.000  0.000\n')
    f.write('  2   0  -0.566   2.225 414.598   0.000   0.000   0.000   1.158   0.000  0.000\n')
    f.write('  3   4  -0.542   2.800 1358.666 550.208   8.000   0.000   6.253   0.000  0.000\n')
    f.write('  4   4   0.130   0.480 1133.428  -0.391   0.000   0.000 5637.075   0.000  0.000\n')
    f.write('  5   4  -0.388  -1.521 718.615  43.375  12.000   0.000   5.966   0.000  0.000\n')
    f.write('  6   4   1.575   2.246  27.258  21.787  10.000   0.000  11.097   0.000  0.000\n')
    f.write('  7   6   0.000  -0.846 377.258   0.000   0.000   0.000   0.000   0.000  0.000\n')
    f.close()
    return name

def accumulate_event(lh_file, filetyp, line):
    """(file opened for reading, str, str) -> dict of dicts
    Return a dictionary of dictionaries per particle in an event.
    
    >>> examplefile = open(write_example_file('examplefile.name'), 'r')
    >>> line = examplefile.readline()
    >>> filetyp = 'lhco'
    >>> lh_file, line, event = accumulate_event(examplefile, filetyp, line)
    >>> line
    ''
    >>> event[1]['eta']
    -0.391
    >>> event[1]['pt'] 
    604.214
    >>> event[7]['phi']
    -0.846

    """
    line = lh_file.readline()
    event = {}
    particle_number = 1
    # Analyze event until next event or end of file is reached:
    while line != '' and line.split()[0] not in lh(filetyp):
        event[particle_number] = lhco_kin(line) if filetyp == 'lhco' else lhe_kin(line)
        particle_number += 1
        line = lh_file.readline()
    if filetyp == 'lhe':
        line = lh_file.readline()
    return lh_file, line, event

def jet_list(filetyp):
    """(str)
    Return jet numbers for lhco or lhe file.
    """
    return [4] if filetyp == 'lhco' else [21, 1, 2, 3, 4, 5, 6]

def number_of_jets(event, jet):
    """
    Return number of jets in
    an event according to filetype
    definition of event.
    >>> examplefile = open(write_example_file('examplefile.name'), 'r')
    >>> line = examplefile.readline()
    >>> filename, line, event = accumulate_event(examplefile, 'lhco', line)
    >>> jet = jet_list('lhco')
    >>> number_of_jets(event, jet)
    4
    """
    j = [event[k] for k in event if event[k]['typ'] in jet]
    return len(j) # Count total number of jets

def b_in_event(event, minET):
    '''
    Return True if a b quark has been tagged in the event.
    This results in an integer number >0 in the b-tag column
    of an lhco file; however, this number has a different meaning 
    for muons, so do not throw away if it is a muon.
    '''
    muon = 2 
    return len([event[k] for k in event if event[k]['typ'] != muon and event[k]['b'] > 0 and event[k]['et'] >= minET]) > 0

def beam_momentum(pt, eta):
    """
    Calculate the z component of the three-
    momentum, or momentum along the beam axis
    pz, given the transverse momentum pt and 
    pseudorapidity eta.
    """
    return pt * math.sinh(eta)

def energy(minv, pt, pz):
    """
    Calculate the energy component of the four-vector
    given the invariant mass minv, transverse momentum pt, 
    and the momentum along the beam direction pz.
    """
    return math.sqrt(minv**2 + pt**2 + pz**2)

def deltaphis(jets, MHT_phi):
    '''
    Return the deltaphis for the jets in the event
    ranked by jet pt.
    Take care that the correct jets are given.
    '''
    #jets_mht = [j for j in jets_pt_ordered(jets) if j['pt'] > 50 and abs(j['eta']) < 5]
    #jets_mht = [j for j in jets_pt_ordered(jets) if j['pt'] > 50 and abs(j['eta']) < 2.5]
    jets_mht = jets_pt_ordered(jets)
    deltaphi = [abs(j['phi'] - MHT_phi) for j in jets_mht]
    return [abs(dp % (-2 * math.pi)) if dp > math.pi else dp for dp in deltaphi]


def jets_met_azimuth(jets, MHT_phi):
    """
    Return whether jets have azimuthal angle diff with 
    MHT at least 0.5 (first two) and 0.3 (third).
    
    """
    #jets_mht = [j for j in jets_pt_ordered(jets) if j['pt'] > 50 and abs(j['eta']) < 5]
    #print 'jets:', jets
    #print 'mhtphi:', MHT_phi
    jets_mht = jets_pt_ordered(jets)
    #jets_mht = [j for j in jets_pt_ordered(jets) if j['pt'] > 50 and abs(j['eta']) < 2.5]
    #print 'jets_mht:', jets_mht
    #deltaphi_mod = [abs(math.acos(math.cos(j['phi'] - MHT_phi))) for j in jets_mht]
    deltaphi = [abs(j['phi'] - MHT_phi) for j in jets_mht]
    #print 'deltaphi:', deltaphi
    deltaphi_mod = [abs(dp % (-2 * math.pi)) if dp > math.pi else dp for dp in deltaphi]
    #print 'deltaphi_mod:', deltaphi_mod
    #print 'passed:',  deltaphi_mod[0] > 0.5 and deltaphi_mod[1] > 0.5 and deltaphi_mod[2] > 0.3
    return deltaphi_mod[0] >= 0.5 and deltaphi_mod[1] >= 0.5 and deltaphi_mod[2] >=0.3

def transverse_energy(E, pt, pz):
    """
    Calculate the so-called transverse energy
    of the particle given the energy E, transverse
    momentum pt, and the momentum along the beam axis
    pz of the particle.
    See TLorentzVector of ROOT for details.
    """
    return math.sqrt(E**2 * pt**2 /(pt**2 + pz**2))

def transverse_energy_short(minv, eta, pt):
    """
    Calculate the so-called transverse energy
    of the particle given the invariant mass minv,
    the pseudorapidity eta, and the transverse 
    momentum pt.
    """
    return math.sqrt(minv**2/math.cosh(eta)**2 + pt**2)
    #return math.sqrt(minv**2 / (1 + math.sinh(eta)**2) + pt**2)

def alpha_T_minET_hardest_jets(jets, minet=100):
    """
    Return True if and only if the two hardest jets
    have Et > 100, or ET > 73 or ET > 87 GeV for 
    the lowest two bins 0 and 1, respectively.
    """
    return len(jets_et_ordered(jets)) > 1 and jets_et_ordered(jets)[0]['et'] > minet and jets_et_ordered(jets)[1]['et'] > minet

def alpha_T_hardest_jet_eta_lt_2_5(jets):
    """ 
    Return True if and only if the hardest 
    jet has abs(eta) < 2.5.
    """
    hardest = jets_et_ordered(jets)[0]
    return -2.5 < hardest['eta'] < 2.5


def alphaT8_no_additional_eta_3_jet(jets):
    """
    Veto event if additional jet satisfies both ET>50 GeV
    and abs(eta) > 3. Since this holds for the selected jets,
    it should hold for all jets.
    """
    return len([jet for jet in jets if abs(jet['eta']) > 3.0 and jet['et'] > 50]) == 0


def alpha_T_no_additional_eta_3_jet(jets):
    """
    Veto event if additional jet satisfies both ET>50GeV
    and abs(eta) > 3.
    Assume two hardest jets have pt>100 and the very hardest
    jet has abs(eta) < 2.5.
    """
    j = jets_et_ordered(jets)[2:]
    return len([jet for jet in j if not -3 <= jet['eta'] <= 3]) == 0

def jan_missing_HT_phi(jets):
    '''
    check against jan's numbers.
    '''
    jets_mht = jets_pt_ordered(jets)
    MHT_x = sum([-part['pt'] * math.cos(part['phi']) for part in jets_mht]) # Checked against Lennart, OK
    MHT_y = sum([-part['pt'] * math.sin(part['phi']) for part in jets_mht]) # Checked against Lennart, OK
    if MHT_y >= 0:
        if MHT_x > 0:
            return math.atan(MHT_y / MHT_x)
        elif MHT_x == 0:
            return math.pi/2.0
        else:
            return math.atan(MHT_y / MHT_x) + math.pi
    else:
        if MHT_x > 0:
            return math.atan(MHT_y / MHT_x)
        elif MHT_x == 0:
            return 1.5 * math.pi
        else:
            return math.atan(MHT_y / MHT_x) + math.pi

def jan_delta_phi(phi1, phi2):
    '''
    check against jan's numbers.
    '''
    return abs(math.acos(math.cos(phi1 - phi2)))


def missing_HT_phi(jets):
    """
    Return the angle phi of the missing HT of the event.
    """
    jets_mht = jets_pt_ordered(jets)
    #jets_mht = [j for j in jets_pt_ordered(jets) if j['pt'] > 30 and abs(j['eta']) < 5]
    MHT_x = sum([-part['pt'] * math.cos(part['phi']) for part in jets_mht]) # Checked against Lennart, OK
    MHT_y = sum([-part['pt'] * math.sin(part['phi']) for part in jets_mht]) # Checked against Lennart, OK
    #MHT_x = sum([-part['pt'] * math.cos(part['phi']) for part in jets_pt_ordered(jets)[:3]])
    #MHT_y = sum([-part['pt'] * math.sin(part['phi']) for part in jets_pt_ordered(jets)[:3]])
    #print 'mhtx:', MHT_x
    #print 'mhty:', MHT_y
    #print 'mhtphi:', math.atan2(MHT_y, MHT_x)
    #if MHT_y >= 0:
    #    if MHT_x > 0:
    #        return math.atan(MHT_y / MHT_x)
    #    elif MHT_x == 0:
    #        return math.pi/2.0
    #    else:
    #        return math.atan(MHT_y / MHT_x) + math.pi
    #else:
    #    if MHT_x > 0:
    #        return math.atan(MHT_y / MHT_x)
    #    elif MHT_x == 0:
    #        return 1.5 * math.pi
    #    else:
    #        return math.atan(MHT_y / MHT_x) + math.pi
    return math.atan2(MHT_y, MHT_x)

def rough_missing_HT_phi(jets):
    '''
    Check to see if numerical differences show up
    '''
    jets_mht = jets_pt_ordered(jets)
    MHT_x = sum([-round(part['pt'],2) * round(math.cos(part['phi']),4) for part in jets_mht])
    MHT_y = sum([-round(part['pt'],2) * round(math.sin(part['phi']),4) for part in jets_mht])
    return math.atan2(MHT_y, MHT_x)


def precise_missing_HT_phi(jets):
    '''
    Check to see if numerical differences show up
    '''
    jets_mht = jets_pt_ordered(jets)
    MHT_x = sum([-decimal.Decimal(part['pt']) * decimal.Decimal(math.cos(part['phi'])) for part in jets_mht])
    MHT_y = sum([-decimal.Decimal(part['pt']) * decimal.Decimal(math.sin(part['phi'])) for part in jets_mht])
    return decimal.Decimal(math.atan2(MHT_y, MHT_x))


def rough_missing_HT(jets):
    '''
    Check for any numerical differences
    '''
    jets_mht = jets_pt_ordered(jets)
    MHT_x = sum([-round(part['pt'],2) * round(math.cos(part['phi']),4) for part in jets_mht])
    MHT_y = sum([-round(part['pt'],2) * round(math.sin(part['phi']),4) for part in jets_mht])
    return math.sqrt(MHT_x**2 + MHT_y **2)

def precise_missing_HT(jets):
    '''
    Check for any numerical differences
    '''
    jets_mht = jets_pt_ordered(jets)
    MHT_x = sum([-decimal.Decimal(part['pt']) * decimal.Decimal(math.cos(part['phi'])) for part in jets_mht])
    MHT_y = sum([-decimal.Decimal(part['pt']) * decimal.Decimal(math.sin(part['phi'])) for part in jets_mht])
    return decimal.Decimal(math.sqrt(MHT_x**2 + MHT_y **2))

def missing_HT(jets):
    """
    Return the amount of MHT in event.
    """
    #MHT_x = sum([-part['px'] for part in jets])
    #MHT_y = sum([-part['py'] for part in jets])

    MHT_x = sum([-part['pt'] * math.cos(part['phi']) for part in jets])
    MHT_y = sum([-part['pt'] * math.sin(part['phi']) for part in jets])

    # MHT: vector pt sum
    MHT = math.sqrt(MHT_x**2 + MHT_y **2)
    #if MHT != 0:
    #    MHT_phi = math.atan2(MHT_y, MHT_x)
    return MHT

def alphaT8_jets_min_ET(event, jet, minET=50):
    """
    Return the jets according to the
    jet definition of alpha_T in the event:
    transverse energy (et) > 50 GeV or some other
    lower threshold depending on the bin (variable
    can be changed).
    >>> examplefile = open(write_example_file('examplefile.name'), 'r')
    >>> line = examplefile.readline()
    >>> filename, line, event = accumulate_event(examplefile, 'lhco', line)
    >>> jet = jet_list('lhco')
    >>> alpha_T_jets(event, jet, minET=50)[2]
    {'phi': -1.521, 'eta': -0.388, 'pt': 718.615, 'et': 719.7442981452388, 'px': 35.769600276394016, 'py': -717.7242185694078, 'typ': 4, 'had_em': 5.966, 'minv': 43.375}
    """
    return [event[k] for k in event if event[k]['typ'] in jet and event[k]['et'] > minET and abs(event[k]['eta']) < 3.0]

def alpha_T_jets_min_ET(event, jet, minET=50):
    """
    Return the jets according to the
    jet definition of alpha_T in the event:
    transverse energy (et) > 50 GeV or some other
    lower threshold depending on the bin (variable
    can be changed).
    >>> examplefile = open(write_example_file('examplefile.name'), 'r')
    >>> line = examplefile.readline()
    >>> filename, line, event = accumulate_event(examplefile, 'lhco', line)
    >>> jet = jet_list('lhco')
    >>> alpha_T_jets(event, jet, minET=50)[2]
    {'phi': -1.521, 'eta': -0.388, 'pt': 718.615, 'et': 719.7442981452388, 'px': 35.769600276394016, 'py': -717.7242185694078, 'typ': 4, 'had_em': 5.966, 'minv': 43.375}
    """
    return [event[k] for k in event if event[k]['typ'] in jet and event[k]['et'] > minET]

def get_bin_numbers_alphaT(HT):
    '''
    Return whether the HT falls in bin 0, 1, or >=2
    deprecated.
    '''
    binno = get_bin_number_alphaT(HT)
    if binno < 2:
        return binno
    elif binno >= 2:
        return 2

def alphaT_determine_bin_number(event, jet):
    '''
    Determine in what bin the event falls in the peculiar
    way this is done in the alphaT analysis:
    Define HT0 the HT using jet definition of bin 0; if 
    this falls in bin 0, use bin 0; do the same for 1 and 2,
    then when it is 2 return the bin number it falls in if >=2.
    Return the bin number of alphaT.
    '''
    HTs = {}
    bin_nos = []
    for b in range(3):
        HTs[b] = total_HT_alphaT(alphaT8_jets_min_ET(event, jet, minET_alphaT(b)[0]))
        bin_no = get_bin_number_alphaT(HTs[b])
        if bin_no == b and bin_no < 2:
            return bin_no, HTs[b]
            #bin_nos.append(b)
        if b == 2 and bin_no >= 2:
            return get_bin_number_alphaT(HTs[b]), HTs[b]
    return None, HTs[b]

def minET_alphaT(bin_no):
    """
    Return minimal ET for jets depending on bin number.
    For the lowest two bins (0 and 1) the minimum ET is 
    37 and 43 GeV, respectively. For the highest two bins
    the minimum ET is 73 and 87 GeV, respectively.
    Otherwise the minimum ET is just 50 GeV.
    """
    if bin_no == 0:
        return 37, 73
    elif bin_no == 1:
        return 43, 87
    else:
        return 50, 100

def alpha_T_jets(event, jet):
    """
    Return the jets according to the
    jet definition of alpha_T in the event:
    transverse energy (et) > 50 GeV.
    >>> examplefile = open(write_example_file('examplefile.name'), 'r')
    >>> line = examplefile.readline()
    >>> filename, line, event = accumulate_event(examplefile, 'lhco', line)
    >>> jet = jet_list('lhco')
    >>> alpha_T_jets(event, jet)[2]
    {'phi': -1.521, 'eta': -0.388, 'pt': 718.615, 'et': 719.7442981452388, 'px': 35.769600276394016, 'py': -717.7242185694078, 'typ': 4, 'had_em': 5.966, 'minv': 43.375}
    """
    return [event[k] for k in event if event[k]['typ'] in jet and event[k]['et'] > 50]

def jets_min_pt_max_eta(event, jet, pt, eta):
    '''
    Return a list of jets with the minimum pt and maximum eta.
    '''
    return [event[k] for k in event if event[k]['typ'] in jet and event[k]['pt'] >= pt and abs(event[k]['eta']) <= eta]
    
    

def jets_met(event, jet):
    """
    Return jets according to definition of jets + met
    analysis: 
    pt > 50 GeV and abs(eta) < 2.5 or pt > 30 GeV and abs(eta) < 5

    """
    return [event[k] for k in event if event[k]['typ'] in jet and ((event[k]['pt'] > 50 and abs(event[k]['eta']) < 2.5) or (event[k]['pt'] > 30 and abs(event[k]['eta']) < 5))]

def at_most_five_hard_jets_jets_met(event, jet):
    """
    Return whether at most five jets have pt > 50 and eta < 2.5
    """
    return len([event[k] for k in event if event[k]['typ'] in jet and event[k]['pt'] > 50 and abs(event[k]['eta']) < 2.5]) <= 5

def at_least_three_hard_jets_jets_met(event, jet):
    """
    Return whether at least three jets have pt > 50 and eta < 2.5
    """
    return len([event[k] for k in event if event[k]['typ'] in jet and event[k]['pt'] > 50 and abs(event[k]['eta']) < 2.5]) >= 3

def total_HT_alphaT(jets):
    """
    Return scalar et sum as defined in the alphaT analysis.
    """
    return sum([jets[i]['et'] for i in range(len(jets))])

def total_HT_jets_met(jets):
    """
    Return scalar et sum as defined in the jets + met analysis.
    """
    return sum([jets[i]['pt'] for i in range(len(jets)) if jets[i]['pt'] > 50 and abs(jets[i]['eta']) < 2.5])

def muon_veto_alphaT8(event):
    """
    Return True for a veto if any muons with a pt >10 GeV
    are present in the event.
    """
    muon = 2
    return len([m for m in event if event[m]['typ'] == muon and event[m]['pt'] > 10]) > 0

def electron_veto_alphaT8(event):
    """
    Return True for a veto if any electrons with a pt >10 GeV
    are present in the event.
    """
    electron = 1
    return len([e for e in event if event[e]['typ'] == electron and event[e]['pt'] > 10]) > 0

def photon_veto_alphaT8(event):
    """
    Return True for a veto if any photons with a pt > 25 GeV
    are present in the event.
    """
    photon = 0
    return len([p for p in event if event[p]['typ'] == photon and event[p]['pt'] > 25]) > 0

def small_mht_met_ratio(mht, event, f='lhco'):
    """
    Return True if the ratio of MHT/MET is small enough as
    required : 1.25. Depends on filetype!
    """
    if f != 'lhco':
        return False
    met = 6
    mets = [event[k]['pt'] for k in event if event[k]['typ'] == met]
    return (mht/mets[0]) < 1.25

def lepton_pt_jets_met(event):
    """
    Return whether all leptons have at least pt > 10
    """
    leptons = [2, 3]
    return len([l for l in event if event[l]['typ'] in leptons and event[l]['pt'] < 10]) == 0

def muon_veto_jets_met(event):
    """
    Return True for a veto if any 'muons' are present in the event.
    Muons are required to have abs(eta) < 2.4 (rapidity)
    and a pt > 10 GeV (transverse momentum).
    """
    muon = 2
    return len([m for m in event if event[m]['typ'] == muon and abs(event[m]['eta']) < 2.4 and event[m]['pt'] > 10]) > 0

def electron_veto_jets_met(event):
    """
    Return True for a veto if any 'electrons' are present in the event.
    Electrons are required to have a pt > 10 GeV, abs(eta) < 2.5 and 
    are not allowed to have 1.44 < abs(eta) < 1.57. In other words,
    Electrons are required to have pt > 10, abs(eta) < 2.5 
    and (abs(eta) < 1.44 or abs(eta)>1.57).
    >>> event = {1: {'phi': -2.555, 'eta': 2.658, 'pt': 49.77, 'et': 49.782167390123796, 'px': -41.4500270675025, 'py': -27.549013704728342, 'typ': 1, 'had_em': 5.36, 'minv': 7.89}, 2: {'phi': 3.121, 'eta': 2.793, 'pt': 190.43, 'et': 190.50988265137633, 'px': -190.38962480319896, 'py': 3.921181875039652, 'typ': 4, 'had_em': 2.98, 'minv': 45.21}, 3: {'phi': -0.594, 'eta': 0.088, 'pt': 189.57, 'et': 191.47685800765746, 'px': 157.0982880558532, 'py': -106.09859937774938, 'typ': 4, 'had_em': 0.0, 'minv': 27.06}, 4: {'phi': -2.415, 'eta': 1.44, 'pt': 92.87, 'et': 93.32834245928031, 'px': -69.41496900769168, 'py': -61.69602076034729, 'typ': 4, 'had_em': 0.78, 'minv': 20.59}, 5: {'phi': 1.576, 'eta': -2.505, 'pt': 37.26, 'et': 37.27950117369041, 'px': -0.19388798859595563, 'py': 37.25949553399614, 'typ': 4, 'had_em': 2.69, 'minv': 7.43}, 6: {'phi': 0.866, 'eta': 0.0, 'pt': 198.65, 'et': 198.65, 'px': 128.7011030060434, 'py': 151.3203508620959, 'typ': 6, 'had_em': 0.0, 'minv': 0.0}}
    >>> electron_veto_jets_met(event)
    False
    >>> event = {1: {'phi': -0.621, 'eta': -0.021, 'pt': 300.36, 'et': 300.9046280243216, 'px': 244.2818913231699, 'py': -174.76409005163222, 'typ': 1, 'had_em': 0.0, 'minv': 18.1}, 2: {'phi': 0.675, 'eta': 1.31, 'pt': 122.12, 'et': 122.21824151947456, 'px': 95.33993287229441, 'py': 76.3124603187867, 'typ': 4, 'had_em': 0.0, 'minv': 9.74}, 3: {'phi': 2.831, 'eta': 0.0, 'pt': 328.33, 'et': 328.33, 'px': -312.62026596598156, 'py': 100.34519523803294, 'typ': 6, 'had_em': 0.0, 'minv': 0.0}}
    >>> electron_veto_jets_met(event)
    True
    """
    electron = 1
    return len([e for e in event if event[e]['typ'] == electron and event[e]['pt'] > 10 and ((-1.44 < event[e]['eta'] < 1.44) or (-2.5 < event[e]['eta'] < -1.57) or (1.57 < event[e]['eta'] < 2.5))]) > 0

def jets_met_analysis(event, data, eventno, jet):
    """dict of dicts, dict of dicts, int, list of int -> bool, dict of dicts, int
    Apply jets_met cuts on event and update data dictionary with HT, MHT, 
    and bin information of the eventno given.
    The list of jets called jet is giving away what particles to treat as a jet.
    """
    jet_no = number_of_jets(event, jet)

    # Make list of jets according to jets+met definition:
    jets = jets_met(event, jet)
    
    MHT = missing_HT(jets)
    HT = total_HT_jets_met(jets)
    bin_no = get_bin_number_jets_met(HT, MHT)
    # Save bin/MHT/HT info:
    if bin_no != None:
        data[eventno] = {'MHT=': MHT, 'HT=': HT, 'bin=': bin_no }
    if muon_veto_jets_met(event):
        return False, '1. "muon" veto', data, bin_no
    elif electron_veto_jets_met(event):
        return False, '2. "electron" veto', data, bin_no
    elif not at_least_three_hard_jets_jets_met(event, jet):
        return False, '3. less than 3 jets have abs(eta) < 2.5 or pt > 50', data, bin_no
    elif not jets_MET_baseline_selection(missing_HT(jets), total_HT_jets_met(jets)): 
        return False, '4. MHT/HT baseline cut', data, bin_no
    elif not jets_met_azimuth(jets, missing_HT_phi(jets)):
        return False, '5. highest pt jets have azimuthal angle w/ MHT <= 0.5 resp <= 0.3 rad', data, bin_no
    # Now the whether the event passed, the cut, a dictionary of
    # bin/MHT/HT per event that passes baseline cut, and the bin
    # number are returned.
    return True, 'none', data, bin_no

def mht_8TeV(event, data, eventno, jet):
    """dict of dicts, dict of dicts, int, list of int -> bool, dict of dicts, int
    Apply jets_met cuts on event and update data dictionary with HT, MHT, 
    and bin information of the eventno given.
    The list of jets called jet is giving away what particles to treat as a jet.
    """
    jet_no = number_of_jets(event, jet)

    # Make list of jets according to jets+met definition:
    #jets = jets_met(event, jet)
    jets_mht = jets_min_pt_max_eta(event, jet, 30, 5)
    jets_ht = jets_min_pt_max_eta(event, jet, 50, 2.5)
    #MHT = missing_HT(jets)
    MHT = missing_HT(jets_mht)
    HT = total_HT_jets_met(jets_ht)
    #HT = total_HT_jets_met(jets)
    bin_no = get_bin_number_mht8(HT, MHT)
    # Save bin/MHT/HT info:
    if bin_no != None:
        data[eventno] = {'MHT=': MHT, 'HT=': HT, 'bin=': bin_no }
    if muon_veto_jets_met(event):
        return False, '1. "muon" veto', data, bin_no
    elif electron_veto_jets_met(event):
        return False, '2. "electron" veto', data, bin_no
    #elif not at_least_three_hard_jets_jets_met(event, jet):
    elif len(jets_ht) < 3:
        return False, '3. less than 3 jets have abs(eta) < 2.5 or pt > 50', data, bin_no
    elif len(jets_ht) > 5:
    #elif not at_most_five_hard_jets_jets_met(event, jet):
        return False, '4. more than 5 jets have abs(eta) < 2.5 or pt > 50', data, bin_no
    elif not jets_MET_baseline_selection(MHT, HT): 
    #elif not jets_MET_baseline_selection(missing_HT(jets), total_HT_jets_met(jets)): 
        return False, '5. MHT/HT baseline cut', data, bin_no
    elif not jets_met_azimuth(jets_ht, missing_HT_phi(jets_mht)):
    #elif not jets_met_azimuth(jets, missing_HT_phi(jets)):
        #borders = [i for i in  deltaphis(jets_ht, missing_HT_phi(jets_mht)) if 0.49<=i<=0.51 or 0.29<=i<=0.31]
        #if not borders == [] and int(eventno) in [1270, 1349, 10536]:
        #    print 'Failed deltaphi cut -- event', eventno, 'in bin', bin_no + 1, 'deltaphi was on border:', deltaphis(jets_ht, missing_HT_phi(jets_mht)) 
        #    print 'jet pts:', [j['pt'] for j in jets_pt_ordered(jets_mht)]
        #    print 'jet etas:', [j['eta'] for j in jets_pt_ordered(jets_mht)]
        #    print 'jet phis:', [j['phi'] for j in jets_pt_ordered(jets_mht)]
        #    print 'missing HT phi, more precise, less precise:', missing_HT_phi(jets_mht), precise_missing_HT_phi(jets_mht), rough_missing_HT_phi(jets_mht)
        #    print 'missing HT phi from Jan:', jan_missing_HT_phi(jets_mht)
        #    print 'deltaphi, more precise, less precise:',  [abs(missing_HT_phi(jets_mht) - j['phi']) for j in jets_pt_ordered(jets_mht)],  [abs(decimal.Decimal(precise_missing_HT_phi(jets_mht)) - decimal.Decimal(j['phi'])) for j in jets_pt_ordered(jets_mht)],  [round(abs(round(rough_missing_HT_phi(jets_mht),3) - j['phi']), 3) for j in jets_pt_ordered(jets_mht)]
        return False, '6. highest pt jets have azimuthal angle w/ MHT <= 0.5 resp <= 0.3 rad', data, bin_no
    # Now the whether the event passed, the cut, a dictionary of
    # bin/MHT/HT per event that passes baseline cut, and the bin
    # number are returned.
    #if 299.9 < MHT < 300.1 and int(eventno) in [1270, 1349, 10536]:
    #    print 'Passed all cuts -- event', eventno, 'in bin', bin_no + 1, 'your MHT was on border:', MHT
    #    print 'jet pts:', [j['pt'] for j in jets_pt_ordered(jets_mht)]
    #    print 'jet etas:', [j['eta'] for j in jets_pt_ordered(jets_mht)]
    #    print 'jet phis:', [j['phi'] for j in jets_pt_ordered(jets_mht)]
    #    print 'missing HT, more precise, less precise:', missing_HT(jets_mht), precise_missing_HT(jets_mht), round(rough_missing_HT(jets_mht), 3)
    #if round(missing_HT_phi(jets_mht) - jan_missing_HT_phi(jets_mht), 4) != 0:
    #    print 'Event', eventno, 'not matching mhtphi Jan:',  missing_HT_phi(jets_mht), jan_missing_HT_phi(jets_mht)
    #    print 'deltaphis:', [abs(missing_HT_phi(jets_mht) - j['phi']) for j in jets_pt_ordered(jets_mht)]
    #    print 'deltaphisjan:', [jan_delta_phi(jets_pt_ordered(jets_ht)[i]['phi'], jan_missing_HT_phi(jets_mht)) for i in range(3)]
    return True, 'none', data, bin_no

def alphaT_12028_8TeV(event, data, eventno, jet, f='lhco'):
    """dict of dicts, dict of dicts, int, list of int -> bool, dict of dicts, int
    Apply alpha_T cuts on event and update data dictionary with HT, MHT, 
    and bin information of the eventno given.
    The list of jets called jet is giving away what particles to treat as a jet.
    """
    # Determine HT independent of cuts so as to determine bin:
    #all_jets = alpha_T_jets_min_ET(event, jet, 0)
    all_jets = alpha_T_jets_min_ET(event, jet, 50)
    all_all_jets = alpha_T_jets_min_ET(event, jet, 0)
    HT_nocuts = total_HT_alphaT(all_jets)
    bin_no = get_bin_number_alphaT(HT_nocuts)
    
    #HT = HT_nocuts - cuts not bin dependent
    if muon_veto_alphaT8(event):
        return False, '1. muon veto:', data, bin_no
    if electron_veto_alphaT8(event):
        return False, '2. electron veto:', data, bin_no
    if photon_veto_alphaT8(event):
        return False, '3. photon veto:', data, bin_no
    if len(all_all_jets) < 2:
        return False, '4. number of jets < 2:', data, bin_no
    if not alpha_T_hardest_jet_eta_lt_2_5(all_all_jets):
        return False, '5. hardest jet has abs(eta) >= 2.5:', data, bin_no
    if not alphaT8_no_additional_eta_3_jet(all_jets):
        return False, '6. additional jet to two hardest has abs(eta) > 3:', data, bin_no
    
    if bin_no >= 3:
        HT = total_HT_alphaT(all_jets)
        bin_no = get_bin_number_alphaT(HT)
    else:
        bin_no, HT = alphaT_determine_bin_number(event, jet)
    if bin_no == None:
        #if not bin_no == get_bin_number_alphaT(HT):
        return False, '7. HT bin selection for bins', data, bin_no
    
    # Count number of jets according to alphaT definition:
    minET, minEThardest = minET_alphaT(bin_no)
    jets = alphaT8_jets_min_ET(event, jet, minET)
    MHT = missing_HT(jets)
    #HT = total_HT_alphaT(jets)
    if not alpha_T_minET_hardest_jets(jets, minEThardest):
        return False, '8. highest ET jets cut:', data, bin_no
    if not small_mht_met_ratio(MHT, event, f=f):
        return False, '9. mht/met ratio too large', data, bin_no

    # Cuts with alpha_T variable:
    if (len(jets) == 2 and not alphaT_dijet(jets_et_ordered(jets)) > 0.55) \
            or (len(jets) > 2 and not alphaT_pseudojets(jets_et_ordered(jets), MHT, HT) > 0.55):
               return False, '10. alphaT cut:', data, bin_no
    if len(jets) > 3:
        return False, '11. has > 3 jets:', data, bin_no
    if b_in_event(event, minET):
        return False, '12. has b in event:', data, bin_no

    # Now the whether the event passed, the cut, a dictionary of
    # bin/MHT/HT per event that passes baseline cut, and the bin
    # number are returned.
    return True, 'none', data, bin_no


def alphaT_analysis(event, data, eventno, jet):
    """ alias for the analysis used now """
    return alphaT_12028_8TeV(event, data, eventno, jet)

def jets_MET_baseline_selection(MHT, HT):
    """
    Return whether the event falls in the baseline selection.
    This means HT > 500 GeV, MHT > 200 GeV.
    >>> jets_MET_baseline_selection(559.87, 113.774809738)
    False
    >>> jets_MET_baseline_selection(559.87, 613.774809738)
    True
    """
    return MHT >= 200.0 and HT >= 500.0

def get_bin_number_mht8(ht, mht):
        """
        Specify in which bin of the jets+MET analysis the HT, MHT fall.
        >>> get_bin_number_mht8(900, 455)
        6
        >>> get_bin_number_mht8(1600, 450)
        16
        >>> get_bin_number_mht8(1600, 300)
        16
        >>> get_bin_number_mht8(1250, 300)
        13
        >>> get_bin_number_mht8(500, 200)
        0
        >>> get_bin_number_mht8(600, 300)
        1
        >>> get_bin_number_mht8(1000, 300)
        9
        """
        if ht >= 1500: # Changed!
            if mht >= 300: # Changed!
                return 16
            elif 200 <= mht < 300: # Changed!
                return 15 
            else:
                if not jets_MET_baseline_selection(mht, ht):
                    return None
                    print "Check your bin numbers; bin seems not to exist (1): ", ht, mht
                    sys.exit()
        elif 1250 <= ht < 1500: # Changed!
            if mht >= 450: # Changed!
                return 14
            elif 450 > mht >= 300: # Changed!
                return 13
            elif 300 > mht >= 200: # Changed!
                return 12
            else:
                if not jets_MET_baseline_selection(mht, ht):
                    return None
                    logging.warning("Check your bin numbers; bin seems not to exist (1): " + str(ht) + ', ' + str(mht))
                    sys.exit()
        elif 1250 > ht >= 1000: # Changed!
            if mht >= 600: # Same (but not with above changes)
                return 11
            elif 600 > mht >= 450: # Changed!
                return 10
            elif 450 > mht >= 300: # Changed!
                return 9
            elif 300 > mht >= 200: # Changed!
                return 8
            else:
                    if not jets_MET_baseline_selection(mht, ht):
                        return None
                    logging.warning("Check your bin numbers; bin seems not to exist (2): " + str(ht) + ', ' + str(mht))
                    sys.exit()
        elif 1000 > ht >= 800: # Same
                if mht >= 600: # Same
                        return 7
                elif 600 > mht >= 450: # Changed!
                        return 6
                elif 450 > mht >= 300: # Changed!
                        return 5
                elif 300 > mht >= 200: # Changed!
                        return 4
                else:
                    if not jets_MET_baseline_selection(mht, ht):
                        return None
                    logging.warning("Check your bin numbers; bin seems not to exist (3): " + str(ht) + ', ' + str(mht))
                    sys.exit()
        elif 800 > ht >= 500: # Same
                if mht >= 600: # Same
                        return 3
                elif 600 > mht >= 450: # Changed
                        return 2
                elif 450 > mht >= 300: # Changed
                        return 1
                elif 300 > mht >= 200: # Changed
                        return 0
                else:
                    if not jets_MET_baseline_selection(mht, ht):
                        return None
                    logging.warning("Check your bin numbers; bin seems not to exist (4): " + str(ht) + ', ' + str(mht))
                    sys.exit()
        return None

def get_bin_number_jets_met(ht, mht):
        """
        Specify in which bin of the jets+MET analysis the HT, MHT fall.
        """
        if ht >= 1400 and mht >= 200:
                return 13
        elif 1400 > ht >= 1200:
                if mht >= 350:
                        return 12
                elif 350 > mht >= 200:
                        return 11
                else:
                    if not jets_MET_baseline_selection(mht, ht):
                        return None
                    print "Check your bin numbers; bin seems not to exist (1): ", ht, mht
                    sys.exit()
        elif 1200 > ht >= 1000:
                if mht >= 500:
                        return 10
                elif 500 > mht >= 350:
                        return 9
                elif 350 > mht >= 200:
                        return 8
                else:
                    if not jets_MET_baseline_selection(mht, ht):
                        return None
                    print "Check your bin numbers; bin seems not to exist (2): ", ht, mht
                    sys.exit()
        elif 1000 > ht >= 800:
                if mht >= 600:
                        return 7
                elif 600 > mht >= 500:
                        return 6
                elif 500 > mht >= 350:
                        return 5
                elif 350 > mht >= 200:
                        return 4
                else:
                    if not jets_MET_baseline_selection(mht, ht):
                        return None
                    print "Check your bin numbers; bin seems not to exist: (3)", ht, mht
                    sys.exit()
        elif 800 > ht >= 500:
                if mht >= 600:
                        return 3
                elif 600 > mht >= 500:
                        return 2
                elif 500 > mht >= 350:
                        return 1
                elif 350 > mht >= 200:
                        return 0
                else:
                    if not jets_MET_baseline_selection(mht, ht):
                        return None
                    print "Check your bin numbers; bin seems not to exist: (4)", ht, mht
                    sys.exit()
        return None

def alpha_T_baseline_selection(HT):
    """
    Return whether this event falls in the baseline selection of alphaT.
    """
    return HT >= 275

def get_bin_number_alphaT(ht):
    """
    Return bin number in which this event falls for alphaT.
    """
    if ht < 275:
        return None
    elif 275 <= ht < 325:
        return 0
    elif 325 <= ht < 375:
        return 1
    elif 375 <= ht < 475:
        return 2
    elif 475 <= ht < 575:
        return 3
    elif 575 <= ht < 675:
        return 4
    elif 675 <= ht < 775:
        return 5
    elif 775 <= ht < 875:
        return 6
    elif 875 <= ht:
        return 7
    else:
        print "Check your bin numbers! Alpha T cannot place ht of ", ht
        sys.exit()


def jets_MHT_HT_cut(MHT, HT, bin_no=14):
    """
    Return whether event passed jets+met _HT_cut
    """
    mhtmin, mhtmax, htmin, htmax = get_bin_bounds(bin_no)
    return htmin <= HT < htmax and mhtmin <= MHT < mhtmax

def alpha_T_HT_cut(HT, bin_no=8):
    """
    Return whether event passed alpha_T_HT_cut
    """
    htmin = 275
    htmax = 1000000000
    htmin, htmax = get_alphaT_bin_bounds(bin_no)
    return htmin <= HT < htmax
    
def jets_pt_ordered(list_of_jets):
    """
    Return a list of lists that contain jet
    kinematic information ordered by
    pt and phi (from largest to smallest):
    >>> j = [{'id': 1, 'et': 2, 'pt': 0.5, 'phi': math.pi, 'eta': 2.0}, {'id': 2, 'et': 1, 'pt': 1, 'phi': math.pi / 2.0, 'eta': 1.5}]
    >>> o = jets_pt_ordered(j)
    >>> o[0]['pt']
    1
    >>> o[0]['et']
    1
    >>> o[1]['id']
    1
    """
    # Obtain a list of lists (jets)  in order of pt-ranking of jets
    # Note that jets seem to be actually already ranked according 
    # to pt in the LHCO-file, so this may be redundant.
    return sorted(list_of_jets, key=lambda jet: jet['pt'], reverse=True)


def jets_et_ordered(list_of_jets):
    """
    Return a list of lists that contain jet
    kinematic information ordered by
    et (from largest to smallest):
    """
    # Obtain a list of lists (jets)  in order of et-ranking of jets
    return sorted(list_of_jets, key=lambda jet: jet['et'], reverse=True)


def alphaT_dijet(j):
    """([{ints/floats}] -> float
    Take a list of jets represented by lists of 
    jet number (0), pt (1) phi (2), eta (3)
    and calculate alphaT according to 
    dijet formula.
    Return alphaT.
    >>> math.sqrt((3) ** 2 - (-1) ** 2 - (0.5) ** 2)
    2.7838821814150108
    >>> res = alphaT_dijet([{'id': 1, 'et': 2, 'pt': 1, 'phi': math.pi, 'eta': 2.0}, {'id': 2, 'et': 1, 'pt': 0.5, 'phi': math.pi / 2.0, 'eta': 1.5}])
    >>> res == 1/2.7838821814150108
    True
    """
    global MESSAGE
    et = 'et'
    pt = 'pt'
    phi = 'phi'
    eta = 'eta'
    sum_et = j[0][et] + j[1][et]
    sum_pt_x = j[0][pt]*math.cos(j[0][phi]) + j[1][pt]*math.cos(j[1][phi])
    sum_pt_y = j[0][pt]*math.sin(j[0][phi]) + j[1][pt]*math.sin(j[1][phi])
    
    #vector_sum_pt = math.sqrt(sum_pt_x ** 2 + sum_pt_y ** 2)

    # The transverse mass is the square root of the difference between 
    # the sum of the transverse energies and
    # the vector sum of the transverse energies:
    # et_pt_difference = sum_et**2 - vector_sum_pt**2
    #print "et_pt_difference less than zero?:", sum_et**2, '-', sum_pt_x**2, '-', sum_pt_y**2, '=', et_pt_difference
    #print "m_transverse = math.sqrt(", sum_et**2 - sum_pt_x**2 - sum_pt_y**2, ")"
    if not (sum_et**2 - sum_pt_x**2 - sum_pt_y**2) < 0:
        m_transverse = math.sqrt(sum_et**2 - sum_pt_x**2 - sum_pt_y**2)
    else:
        import shutil as sh
        import os
        if '/' in event_file:
            pathname = 'issue_at_event_' + str(event_number_current) + '_00_' + event_file[event_file.rindex('/')+1:]
        else:
            pathname = 'issue_at_event_' + str(event_number_current) + '_00_' + event_file
        count = 1
        while os.path.exists(pathname):
            pathname = 'issue_at_event_' + str(event_number_current) + '_' + str(count).zfill(2) + '_' + event_file
            count += 1
        sh.copy(event_file, pathname)
        MESSAGE += '\n'
        MESSAGE += "*******************************************************" + '\n'
        MESSAGE += "******************************************************* \n"
        MESSAGE += "******************* WARNING *************************** \n"
        MESSAGE +="et_pt_difference less than zero in event #:" + str(event_number_current) +'\n'
        MESSAGE +=" sum_et**2 - sum_pt_x**2 - sum_pt_y ** 2 \n" 
        MESSAGE +=str(sum_et**2) + ' - ' +  str(sum_pt_x**2) +  ' - ' + str(sum_pt_y ** 2) +'\n' 
        MESSAGE +='=' + str(sum_et**2 - sum_pt_x**2 - sum_pt_y**2) + '\n'
        MESSAGE +='jets in this event (dijet):' + str(j)
        MESSAGE +='\n Event file saved as ' + pathname + '\n' 
        MESSAGE +="******************************************************* \n"
        MESSAGE +="****************************************************** \n"
        MESSAGE +="******************************************************* \n"
        return 0

    # alphaT is the transverse energy of the second highest pt jet divided
    # by the transverse mass
    alphaT = j[1][et]/m_transverse
    return alphaT

def delta_ht(j):
    """([{ints/floats}] -> float
    Take a list of jets represented by lists of
    jet number (0), pt (1) phi (2), eta (3)
    and calculate delta_ht:
    Two pseudojets are constructed from half the jets each so that the 
    difference between the sum of their transverse energies, delha_ht 
    is minimal:
        >>> jets = [{'id': 1, 'et': 5, 'pt': 4, 'phi': math.pi, 'eta': 2.0}, {'id': 2, 'et': 4, 'pt': 3, 'phi': math.pi / 2.0, 'eta': 1.5}, {'id': 1, 'et': 2, 'pt': 1, 'phi': math.pi, 'eta': 2.0}, {'id': 2, 'et': 1, 'pt': 0.5, 'phi': math.pi / 2.0, 'eta': 1.5}]
    >>> delta_ht(jets)
    0
    """
    n_jets = len(j)
    #print n_jets
    # Make a list of all transverse energies of the jets:
    et_jets = [j[i]['et'] for i in range(n_jets)]
    #print et_jets
    # Make a list of all possible combinations of this list:
    #ets = list(itertools.permutations(et_jets))
    #print list(itertools.combinations(range(len(et_jets)), len(et_jets)//2))
    #print [b[j] for b in ([list(itertools.combinations(range(len(et_jets)), i)) for i in range(1, len(et_jets)//2 + 1)]) for j in range(len(b))]

    # You assumed the pseudojets should be made up of half the jets (len(et_jets)//2) but this is not so; one pseudojet could have 3 the other 1 jet(s).
    #delta_ht =  min([abs(sum([et_jets[i] for i in a[1]])- sum(a[1])) for a in [tuple([a, [et_jets[i] for i in range(len(et_jets)) if i not in a]]) for a in list(itertools.combinations(range(len(et_jets)), len(et_jets)//2))]])
    #print [abs(sum([et_jets[i] for i in a[0]])- sum(a[1])) for a in [tuple([a, [et_jets[i] for i in range(len(et_jets)) if i not in a]]) for a in [b[j] for b in ([list(itertools.combinations(range(len(et_jets)), i)) for i in     range(1, len(et_jets)//2 +1)]) for j in range(len(b))]]]
    delta_ht = min([abs(sum([et_jets[i] for i in a[0]])- sum(a[1])) for a in [tuple([a, [et_jets[i] for i in range(len(et_jets)) if i not in a]]) for a in [b[j] for b in ([list(itertools.combinations(range(len(et_jets)), i)) for i in range(1, len(et_jets)//2 +1)]) for j in range(len(b))]]])
    
    # Two pseudojets are constructed from half the jets each so that the 
    # difference between the sum of their transverse energies, delha_ht 
    # is minimal:
    #delta_ht = min([abs(sum(ets[i][:n_jets//2]) - sum(ets[i][n_jets//2:])) for i in range(len(ets))])
    return delta_ht


def alphaT_pseudojets(j, MHT, HT):
    """([{ints/floats}] -> float
    Take a list of jets represented by lists of 
    dictionaries containing jet number (id),
    transverse momentum (pt), transverse energy
    (et), the angle phi, and the pseudorapidity
    (eta), and calculate alphaT according to 
    pseudojet formula.
    Return alphaT.
    >>> alphaT_pseudojets([{'et': 220}, {'et':150}, {'et': 75}], 200, 445)
    0.5534262839761064
    >>> jets = [{'id': 1, 'et': 5, 'pt': 2, 'phi': math.pi, 'eta': 2.0}, {'id': 2, 'et': 4, 'pt': 3, 'phi': math.pi / 2.0, 'eta': 1.5}, {'id': 1, 'et': 2, 'pt': 1, 'phi': math.pi, 'eta': 2.0}, {'id': 2, 'et': 1, 'pt': 4, 'phi': math.pi / 2.0, 'eta': 1.5}]
    >>> 0.5 / 0.75
    0.6666666666666666
    >>> 0.5 * (1 - 0) / math.sqrt(1 - (20/40.0)**2)
    0.5773502691896258
    >>> alphaT_pseudojets(jets, 20, 40)
    0.5773502691896258
    >>> examplefile = open(write_example_file('examplefile.name'), 'r')
    >>> line = examplefile.readline()
    >>> filetyp = 'lhco'
    >>> lh_file, line, event = accumulate_event(examplefile, filetyp, line)
    >>> line
    ''
    >>> jetlist = jet_list(filetyp)
    >>> jets = alpha_T_jets(event, jetlist)
    >>> pt1 = 1358.666
    >>> pt2 = 1133.428
    >>> pt3 = 718.616
    >>> eta1 = -0.542
    >>> minv1 = 550.208
    >>> eta2 = 0.130
    >>> minv2 = -0.391
    >>> eta3 = -0.388
    >>> minv3 = 43.375
    >>> et1 = transverse_energy_short(minv1, eta1, pt1) 
    >>> et2 = transverse_energy_short(minv2, eta2, pt2) 
    >>> et3 = transverse_energy_short(minv3, eta3, pt3) 
    >>> round(sum([et1, et2, et3])) == round(total_HT_alphaT(jets))
    True
    >>> alphat = 0.5 * (1.0 - abs((et1 - (et2 + et3))) / sum([et1, et2, et3])) / math.sqrt(1.0 - missing_HT(jets)**2/sum([et1, et2, et3])**2)
    >>> alphat2 = alphaT_pseudojets(jets, missing_HT(jets), total_HT_alphaT(jets))
    >>> round(delta_ht(jets), 2) == round(abs(et1 - (et2 + et3)), 2)
    True
    >>> round(alphat, 4) == round(alphat2, 4)
    True
    """
    # alphaT is then the following expression depending on delta_ht, HT, and MHT:
    #print HT, MHT
    alphaT = 0.5 * (1.0 - float(delta_ht(j)) / float(HT)) / float(math.sqrt(1.0 - (float(MHT) / float(HT))**2))
    return alphaT

def save_mht_ht(event_file, infofile):
    """file -> [list of numbers]
    Return the mht and ht of all events
    of a file according to definitions of
    the jets+met analysis.
    """
    lh_file = gzip.open(event_file, 'rb') if event_file.endswith('gz') else open(event_file, 'r')
    line = lh_file.readline()
    count = 0
    jetless = 0
    mht_ht = {}
    writefile = open(infofile, 'a')
    writefile.write(write_line(['mht', 'ht'], sep='|') + '\n')
    jet = jet_list(filetype(event_file))
    while line != '' and count < 10000000000:
        lh_file, line = read_to_event(lh_file, filetype(event_file), line)
        lh_file, line, event = accumulate_event(lh_file, filetype(event_file), line) # Accumulate particles in dict
        # If no jet found, empty event (added by Delphes)
        jet_no = number_of_jets(event, jet)
        if jet_no == 0:
            jetless += 1
            continue
        
        jets = jets_met(event, jet)
        MHT = missing_HT(jets)
        HT = total_HT_jets_met(jets)
        writefile.write(write_line([MHT, HT], sep='|') + '\n')
        count += 1
        mht_ht[count] = {'ht': HT, 'mht': MHT}
    print "There were", jetless, "events without jets."
    writefile.close()
    return mht_ht

def mht_et_pt_alphaT(event_file, infofile=None, cuts=False):
    """file -> [list of numbers]
    Return the mht and ht of all events
    of a file according to definitions of
    the jets+met analysis.
    """
    lh_file = gzip.open(event_file, 'rb') if event_file.endswith('gz') else open(event_file, 'r')
    line = lh_file.readline()
    count = 0
    jetless = 0
    mht_et = {}
    jet = jet_list(filetype(event_file))

    # Include taus as jets; include all right- and left-handed u/d squarks as jets (these
    # are the final state particles in lhe files)
    jet.extend([15, 1000002, 1000001, 2000002, 2000001, -1000002, -1000001, -2000001, -2000002]) if filetype(event_file) == 'lhe' else jet.append(3)
    # include ued particles:
    jet.extend([5100002, 5100001, 6100002, 6100001, -5100002, -5100001, -6100001, -6100002]) if filetype(event_file) == 'lhe' else jet.append(3)
    

    while line != '' and count < 10000000000:
        lh_file, line = read_to_event(lh_file, filetype(event_file), line)
        lh_file, line, event = accumulate_event(lh_file, filetype(event_file), line) # Accumulate particles in dict
        if filetype(event_file) == 'lhe':
            # Take only outgoing particles! 1: outgoing final state particles
            #-1: incoming particle; (-)2: intermediate resonance (spacelike propagator)
            [event.pop(k) for k in [k for k in event if event[k]['cat'] != 1]]
        # If no jet found, empty event (added by Delphes)
        jet_no = number_of_jets(event, jet)
        if jet_no == 0:
            jetless += 1
            continue
        
        # Determine binnumber for alphaT:
        if cuts and not alphaT_12028_8TeV(event, {}, count, jet, f=filetype(event_file))[0]:
            alphaT = np.nan
            pt_leading_alphaT, pt_second_alphaT, pt_third_alphaT = np.nan, np.nan
            ET = np.nan
            nojetsalphaT = np.nan
            MHT_alphaT = np.nan
        else:
            alphaT, pt_leading_alphaT, pt_second_alphaT, pt_third_alphaT, ET, MHT_alphaT, nojetsalphaT = alphaT_mht_ht_pt_alphaT(event, jet)

        # Jets and quantities according to MHT analysis:
        if cuts and not jets_met_analysis(event, {}, count, jet)[0]:
            MHT = np.nan
            HT = np.nan
            pt_leading_MHT, pt_second_MHT, pt_third_MHT = np.nan, np.nan
        else:
            MHT, HT, pt_leading_MHT, pt_second_MHT, pt_third_MHT = jets_met_mht_pt_alphaT(event, jet)
        # I want here to make a function for just pt
        all_all_jets = [event[i] for i in event if event[i]['typ'] in jet]
        pts = [jets_pt_ordered(all_all_jets)[i]['pt'] for i in range(0, min(len(all_all_jets), 3))]
        pt_leading = pts[0]
        pt_second = pts[1] if len(pts) > 1 else np.nan
        pt_third = pts[2] if len(pts) > 2 else np.nan
        
        mht_et[count] = {'ET': ET, 'MHT_alphaT': MHT_alphaT, 'pt_leading_alphaT': pt_leading_alphaT, 'pt_second_alphaT': pt_second_alphaT, 'pt_third_alphaT': pt_third_alphaT, 'MHT': MHT, 'HT': HT, 'pt_leading_MHT': pt_leading_MHT, 'pt_second_MHT': pt_second_MHT, 'pt_third_MHT': pt_third_MHT, 'alphaT': alphaT, 'pt_leading': pt_leading, 'pt_second': pt_second, 'pt_third': pt_third, 'alphaTnojets': nojetsalphaT}
        count += 1
    infofile = None
    if not infofile == None:
        dicts = [mht_et[k].update({'eventno':i}) for k in mht_et]
        import csv
        csv.register_dialect('sql', delimiter='|', quoting=csv.QUOTE_NONE)
        with open(infofile, 'w') as f:
            writer = csv.DictWriter(f, ['eventno', 'ET', 'MHT_alphaT', 'pt_leading_alphaT', 'pt_second_alphaT', 'MHT', 'HT', 'pt_leading_MHT', 'pt_second_MHT', 'alphaT', 'pt_leading', 'pt_second'], dialect = 'sql')
            writer.writeheader()
            writer.writerows(dicts)

    #print "There were", jetless, "events without jets."
    return mht_et

def jets_met_mht_pt_alphaT(event, jet):
    '''
    Get mht, ht, pt for this event using
    MHT analysis definitions.
    '''
    jets = jets_met(event, jet)
    MHT = missing_HT(jets)
    HT = total_HT_jets_met(jets)
    if len(jets) > 2:
        pt_leading_MHT, pt_second_MHT, pt_third_MHT = [jets_pt_ordered(jets)[i]['pt'] for i in range(0, min(len(jets), 3))]
    elif len(jets) == 2:
        pt_leading_MHT, pt_second_MHT = [jets_pt_ordered(jets)[i]['pt'] for i in range(0, min(len(jets), 2))]
        pt_third_MHT = np.nan
    elif len(jets) == 1:
        pt_leading_MHT, pt_second_MHT, pt_third_MHT = jets_pt_ordered(jets)[0]['pt'], np.nan, np.nan
    else:
        pt_leading_MHT, pt_second_MHT, pt_third_MHT = np.nan, np.nan, np.nan
    return MHT, HT, pt_leading_MHT, pt_second_MHT, pt_third_MHT


def alphaT_mht_ht_pt_alphaT(event, jet):
    '''
    Get alphaT, mht, ht (here et!), pt for
    this event using alphaT analysis definitions.
    '''

    all_jets = alpha_T_jets_min_ET(event, jet, 50)
    all_all_jets = alpha_T_jets_min_ET(event, jet, 0)
    HT_nocuts = total_HT_alphaT(all_jets)
    bin_no = get_bin_number_alphaT(HT_nocuts)
    if bin_no >= 3:
        ET = total_HT_alphaT(all_jets)
        bin_no = get_bin_number_alphaT(ET)
    else:
        bin_no, ET = alphaT_determine_bin_number(event, jet)
    
    # Jets and quantities according to alphaT analysis:
    minET, minEThardest = minET_alphaT(bin_no)
    jets_alphaT = alphaT8_jets_min_ET(event, jet, minET)
    MHT_alphaT = missing_HT(jets_alphaT)
    ET = total_HT_alphaT(jets_alphaT)
    pt_third_alphaT = np.nan
    if len(jets_alphaT) == 2:
        alphaT = alphaT_dijet(jets_et_ordered(jets_alphaT))
        pt_leading_alphaT, pt_second_alphaT = [jets_pt_ordered(jets_alphaT)[i]['pt'] for i in range(0, min(len(jets_alphaT), 2))]
    elif len(jets_alphaT) > 2:
        alphaT = alphaT_pseudojets(jets_et_ordered(jets_alphaT), MHT_alphaT, ET)
        pt_leading_alphaT, pt_second_alphaT, pt_third_alphaT = [jets_pt_ordered(jets_alphaT)[i]['pt'] for i in range(0, min(len(jets_alphaT), 3))]
    else:
        #continue
        alphaT = np.nan
        #if len(jets_alphaT) == 1:
        #    pt_leading_alphaT, pt_second_alphaT = jets_pt_ordered(jets_alphaT)[0]['pt'], None
        #else:
        pt_leading_alphaT, pt_second_alphaT, pt_third_alphaT = np.nan, np.nan, np.nan
    return alphaT, pt_leading_alphaT, pt_second_alphaT, pt_third_alphaT, ET, MHT_alphaT, len(jets_alphaT)

def analyze_eventfile(event_file, tau, analysis):
    """(str, bool, str) -> int, int, {dict}, {dict}
    Return the amount of events that passed, 
    the amount of events in the file, the cuts
    applied and the number of times they were applied
    (this is in a certain order), and the 
    data in a form of {eventno, bin, mht, ht} of 
    a file after analyzing it with the given analysis.
    Tau is given - taus should be counted as jets for 
    Delphes 3 but not for Delphes 2.
    """
    # Open the file.
    lh_file = gzip.open(event_file, 'rb') if event_file.endswith('gz') else open(event_file, 'r')
    line = lh_file.readline()
    # Initiate variables
    event_number = 0 # total events analyzed
    events_passed = {} # total events that passed cuts
    count = 0 # just in case we end up in an endless loop
    data = {} # to save MHT,  HT, bin information
    cuts = {} # keep track of how many events are lost per cut (order matters)

    # Define what jets are (certain numbers in lhe or lhco files):
    jet = jet_list(filetype(event_file))

    # Add tau to this list if tau is True, ie to be counted as a jet:
    if tau:
        jet.append(15) if filetype(event_file) == 'lhe' else jet.append(3)

    # Start analyzing event by event:
    while line != '' and count < 10000000000:
        # Go to event, read it, and store in a dictionary:
        lh_file, line = read_to_event(lh_file, filetype(event_file), line)
        eventno = line.split()[1]
        lh_file, line, event = accumulate_event(lh_file, filetype(event_file), line) # Accumulate particles in dict
        # If no jet found, emtpy event (added by Delphes) - do not count:
        jet_no = number_of_jets(event, jet)
        if jet_no == 0:
            continue
        # Start given analysis on event:
        if analysis in ['alphaT', 'alpha', 'alpha8', 'alpha_T', 'alphaT8', 'alphaT8TeV']:
            passed, cut, data, bin_no = alphaT_12028_8TeV(event, data, eventno, jet)
        elif analysis in ['alphaT7', 'alpha7', 'alpha_T7', 'alphaT7', 'alpha_T7']:
            passed, cut, data, bin_no = alphaT_analysis(event, data, eventno, jet)
        elif analysis in ['jets_met', 'JETS_MET', 'jets_MET', 'jetsMET', 'jetsmet']:
            passed, cut, data, bin_no = jets_met_analysis(event, data, eventno, jet)
        elif analysis in ['jets_met8', 'JETS_MET8', 'mht8', 'mht', 'MHT']:
            passed, cut, data, bin_no = mht_8TeV(event, data, eventno, jet)
        else:
            continue
        # Act according to whether event passed; save any cuts in dictionary:
        if passed: #cut == 'none': The event passed all cuts.
            if bin_no in events_passed:
                events_passed[bin_no] += 1
            else:
                events_passed[bin_no] = 1
            event_number += 1 # Count total # of events analyzed
        elif cut in cuts:
            cuts[cut] += 1
            event_number += 1 # Count total # of events analyzed
        else:
            cuts[cut] = 1
            event_number += 1 # Count total # of events analyzed
        count += 1
    # Now the number of events that passed, number of events analyzed,
    # and a dictionary of cuts and their numbers, and a dictionary of
    # bin/MHT/HT per event that passes baseline cut are returned.
    return events_passed, event_number, cuts, data

def count_jets(event_file, tau):
    """ file, bool -> dict
    Return the number of true jets
    and number of alphaT defined jets in file.
    """
    lh_file = gzip.open(event_file, 'rb') if event_file.endswith('gz') else open(event_file, 'r')
    line = lh_file.readline()
    event_number = 0 # total events analyzed
    count = 0
    jets = {'total': {}, 'alpha_T': {}} # keep track of how many events are lost per cut (order matters)
    while line != '' and count < 10000000000:
        lh_file, line = read_to_event(lh_file, filetype(event_file), line)
        lh_file, line, event = accumulate_event(lh_file, filetype(event_file), line) # Accumulate particles in dict
        jet = jet_list(filetype(event_file))
        alpha_T = len(alpha_T_jets(event, jet))
        total = number_of_jets(event, jet)
        if alpha_T in jets['alpha_T']:
            jets['alpha_T'][alpha_T] += 1
        else:
            jets['alpha_T'][alpha_T] = 1
        if total in jets['total']:
            jets['total'][total] += 1
        else:
            jets['total'][total] = 1
        event_number += 1
         
    return jets, event_number

def filetype(event_file):
    a = -3 if event_file.endswith('gz') else None
    return event_file[event_file.rindex('.')+1:a]
 
if __name__ == '__main__':
    import doctest
    doctest.testmod()
