import os, sys, glob
from ROOT import *
from time import sleep

def HistoSamStyle(histo):
    LabelSize = 0.049
    lwidth = 3
    histo.SetMinimum(0)
    histo.SetLineWidth(lwidth)
    histo.SetMarkerStyle(21)
    histo.GetXaxis().SetTitleSize(1.25*LabelSize)
    histo.GetXaxis().SetTitleOffset(0.9)
    histo.GetYaxis().SetTitleSize(1.25*LabelSize)
    histo.GetXaxis().SetLabelSize(LabelSize)
    histo.GetYaxis().SetLabelSize(LabelSize)
    histo.GetYaxis().SetTitle("Unit Normalized")
    histo.GetYaxis().SetTitleOffset(1.1)
    histo.SetTitle('')
    histo.GetXaxis().SetRange(1,histo.GetNbinsX()/2)
    histo.GetXaxis().CenterTitle()
    histo.GetYaxis().CenterTitle()


def mkNameNiceRoot(badname):
    niceName = badname.replace('@',' ')
    niceName = niceName.replace('__','  ')
    niceName = niceName.replace('T_','T>')
    niceName = niceName.replace('s_','s>')
    niceName = niceName.replace('_','-')
    niceName = niceName.replace('>','$>$')
    niceName = niceName.replace('<','$<$')
    niceName = niceName.replace('  ',',  ')
    niceName = niceName.replace('MHT','$#slash{H}_{T}$ (GeV)')
    niceName = niceName.replace('HT','$H_{T}$ (GeV)') 
    niceName = niceName.replace('NJets','n_{j}')
    niceName = niceName.replace('DeltaPhi',' $#Delta(#phi)$')
    niceName = niceName.replace('$','')
    niceName = niceName.strip()
    return niceName

keyword = sys.argv[1]
histoKey = sys.argv[2]
myfile = TFile('result_'+keyword+'.root')
print 'Opening file: ' + 'result_'+keyword+'.root'
myfile.ls()



ma5histo = myfile.Get('"'+histoKey+'"_ma5')
HistoSamStyle(ma5histo)
varname =histoKey[:histoKey.find('after')-1]
ma5histo.GetXaxis().SetTitle( mkNameNiceRoot(varname))
ma5histo.SetLineColor(9)

offhisto = myfile.Get('"'+histoKey+'"_off')
HistoSamStyle(offhisto)
offhisto.GetXaxis().SetTitle(mkNameNiceRoot(histoKey[:histoKey.find('after')]))
offhisto.SetLineColor(9)
offhisto.SetLineStyle(2)

ma5histo.SetMaximum(1.1*max(ma5histo.GetMaximum(),offhisto.GetMaximum()))
offhisto.SetMaximum(1.1*max(ma5histo.GetMaximum(),offhisto.GetMaximum()))

leg = TLegend(.56,.62,.86,.8)
if "HT" in histoKey:
    tickoption = ma5histo.GetXaxis().GetTicks()
    ma5histo.GetXaxis().SetNdivisions(8, kFALSE)
    ma5histo.GetXaxis().SetTicks(tickoption)
    offhisto.LabelsOption('>',"X")
if "MHT" in histoKey:
    leg = TLegend(.6, .72, .9, .9)
    ma5histo.SetAxisRange(0,950,"X")
    offhisto.SetAxisRange(0,950,"X")
if "NJets" in histoKey:
    leg = TLegend(.55,.6,.83,.78)
    ma5histo.SetAxisRange(-0.5, 7.5, "X")
    offhisto.SetAxisRange(-0.5, 7.5, "X")

leg.AddEntry(offhisto,"CMS result",'l')
leg.AddEntry(ma5histo,"MA5 result",'l')
leg.SetHeader("T2qq (700,100)")
leg.GetListOfPrimitives().First().SetTextAlign(22)
leg.SetFillColor(kWhite)
leg.SetTextFont(42)
leg.SetBorderSize(1)
leg.SetShadowColor(kWhite)


c1 = TCanvas('c1','c1',850,700)
ma5histo.Draw()
offhisto.Draw('same')
leg.Draw()

#print c1.GetWindowWidth()
c1.SetBottomMargin(0.15)
c1.SetLeftMargin(0.15)
c1.Update()
c1.Print("cms-012-"+keyword+varname+"Compare.pdf")
sleep(3)
exit(0)


