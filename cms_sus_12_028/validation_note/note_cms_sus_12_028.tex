\documentclass[12pt,A4paper]{article}
%\pdfoutput=1

\usepackage{cite}
\usepackage{hyperref}
\usepackage{graphicx}
    \usepackage{amsmath}
    \usepackage{caption}
    \usepackage{subcaption}
    \usepackage{cancel}
    \usepackage{etoolbox} % provides \patchcmd macro
    \makeatletter % modify the "headings" page style
    \patchcmd{\ps@headings}{{\slshape
ightmark}\hfil	hepage}{	hepage\hfil}{}{}
    \makeatother
    \pagestyle{headings}
\usepackage{listings}
\usepackage{color}

\textwidth 170mm
\textheight 220mm
\oddsidemargin -5mm
\evensidemargin 5mm
\topmargin -16pt

\newcommand{\mulobs}{$\sigma^{\mathrm{UL}}_{\mathrm{obs}}$}
\newcommand{\ulobs}{\sigma^{\mathrm{UL}}_{\mathrm{obs}}}
\newcommand{\mulexp}{$\sigma^{\mathrm{UL}}_{\mathrm{exp}}$}
\newcommand{\ulexp}{\sigma^{\mathrm{UL}}_{\mathrm{exp}}}
\newcommand{\mtheo}{$\sigma^{\mathrm{pred}}_{\mathrm{theo}}$}
\newcommand{\theo}{\sigma_{\mathrm{pred}}^{\mathrm{theo}}}

\newcommand{\lumi}{\mathcal{L}}
\newcommand{\twomm}{\hspace{2mm}}
\newcommand{\neu}{{\widetilde{\chi}_1^0}}

\newcommand{\go}{{\widetilde{g}}}
\newcommand{\sq}{{\widetilde{q}}}
\newcommand{\mmsq}{$m_{\widetilde{q}}$}
\newcommand{\msq}{m_{\widetilde{q}}}
\newcommand{\mlsp}{m_{\widetilde{\chi}}}
\newcommand{\mmlsp}{$m_{\widetilde{\chi}}$}
\newcommand{\mgo}{m_{\widetilde{g}}}
\newcommand{\mmgo}{$m_{\widetilde{g}}$}

\lstset{ %
  language=C++,                % the language of the code
  basicstyle=\footnotesize,           % the size of the fonts that are used for the code
  %  numbers=left,                   % where to put the line-numbers
  %  numberstyle=\tiny\color{gray},  % the style that is used for the line-numbers
  %  stepnumber=2,                   % the step between two line-numbers. If it's 1, each line 
  % will be numbered
  %  numbersep=5pt,                  % how far the line-numbers are from the code
  backgroundcolor=\color{white},      % choose the background color. You must add \usepackage{color}
  showspaces=false,               % show spaces adding particular underscores
  showstringspaces=false,         % underline spaces within strings
  showtabs=false,                 % show tabs within strings adding particular underscores
  frame=single,                   % adds a frame around the code
  rulecolor=\color{black},        % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. commens (green here))
  tabsize=2,                      % sets default tabsize to 2 spaces
  captionpos=b,                   % sets the caption-position to bottom
  breaklines=true,                % sets automatic line breaking
  breakatwhitespace=false,        % sets if automatic breaks should only happen at whitespace
  title=\lstname,                   % show the filename of files included with \lstinputlisting;
  % also try caption instead of title
  keywordstyle=\color{blue},          % keyword style
  commentstyle=\color{dkgreen},       % comment style
  stringstyle=\color{mauve},         % string literal style
  escapeinside={\%*}{*)},            % if you want to add a comment within your code
  morekeywords={*,\dots}               % if you want to add more keywords to the set
}


\title{Validation of the \texttt{MadAnalysis 5} implementation of CMS-SUS-12-028}
\author{Jory Sonneveld (RWTH Aachen)\\
\normalsize {\it email: sonneveld@physik.rwth-aachen.de}
\date{\today}
}

\begin{document}
        \maketitle


\section{Setup}
In this document, the \texttt{MadAnalysis 5} implementation of the all-hadronic \href{https://twiki.cern.ch/twiki/bin/view/CMSPublic/PhysicsResultsSUS12028}{CMS-SUS-12-028} (see also \href{http://arxiv.org/abs/1303.2985}{arXiv:1303.2985}) is validated.

For this purpose, \texttt{LHE} files provided by CMS were showered with \texttt{Pythia 6.4} and subsequently detector simulated with the \texttt{MadAnalysis 5 v1.1.11 patch 1b} version of \texttt{Delphes} (\texttt{delphesMA5tune}).

The \texttt{LHE} files provided by CMS were for 6 combinations of simplified models and colored sparticle and LSP masses:
\begin{itemize}
  \item 
    \href{https://twiki.cern.ch/twiki/pub/CMSPublic/PhysicsResultsSUS12028/T1_700_300.lhe}{the \texttt{T1} model with $\mgo=700$ GeV and $\mlsp=300$ GeV};
  \item 
    \href{https://twiki.cern.ch/twiki/pub/CMSPublic/PhysicsResultsSUS12028/T1bbbb_900_500.lhe}{the \texttt{T1bbbb} model with $\mgo=900$ GeV and $\mlsp=500$ GeV};
  \item 
    \href{https://twiki.cern.ch/twiki/pub/CMSPublic/PhysicsResultsSUS12028/T1tttt_850_250.lhe}{the \texttt{T1tttt} model with $\mgo=850$ GeV and $\mlsp=250$ GeV};
  \item 
    \href{https://twiki.cern.ch/twiki/pub/CMSPublic/PhysicsResultsSUS12028/T2_600_250.lhe}{the \texttt{T2} model with $\msq=600$ GeV and $\mlsp=250$ GeV};
  \item 
    \href{https://twiki.cern.ch/twiki/pub/CMSPublic/PhysicsResultsSUS12028/T2bb_500_150.lhe}{the \texttt{T2bb} model with $\msq=500$ GeV and $\mlsp=150$ GeV};
  \item 
    \href{https://twiki.cern.ch/twiki/pub/CMSPublic/PhysicsResultsSUS12028/T2tt_400_0.lhe}{the \texttt{T2tt} model with $\msq=400$ GeV and $\mlsp=0$ GeV}.
\end{itemize}


In order to run \texttt{Pythia 6.4} successfully, the following lines were added to the \texttt{LHE} file:
\lstset{language=xml}
\begin{lstlisting}
<MGRunCard>
   1   = ickkw   ! turning matching on/off for multi-jet sample
  30   = xqcut   ! minimum kt jet measure between partons
</MGRunCard>
\end{lstlisting}
and
\begin{lstlisting}
<MGParamCMS>
           5 = nqmatch    ! Max Jet Flavor
           2 = maxjets    ! Largest number (inclusive ktMLM matching multipl.)
           0 = minjets    ! Smallest number of additional light flavour jets
         5.0 = etaclmax   ! Maximum pseudorapidity for particles to cluster
</MGParamCMS>
\end{lstlisting}


In \texttt{MadAnalysis 5}, to run \texttt{Delphes} the following settings were used:
\lstset{language=sh}
\begin{lstlisting}
ma5>set main.fastsim.package = delphesMA5tune
\end{lstlisting}
and
\begin{lstlisting}
ma5>set main.fastsim.detector = cms
\end{lstlisting}
The \href{http://madanalysis.irmp.ucl.ac.be/attachment/wiki/PhysicsAnalysisDatabase/delphesMA5tune_card_CMS_SUSY.tcl}{\texttt{DelphesMA5tune} card} used for these analysis can be found on the \texttt{MadAnalysis} \href{http://madanalysis.irmp.ucl.ac.be/attachment/wiki/PhysicsAnalysisDatabase/}{Physics Analysis Database} page.
Finally, they were analyzed with an implementation of the CMS-SUS-12-028 analysis in the same version of \texttt{MadAnalysis 5}.

Cutflows and histograms of several observables are compared with those of CMS in the next sections.
In the last section, exclusions in the squark or gluino and LSP mass planes are compared.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\texttt{T1bbbb} simplified model}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure}[h!]
\centering
\includegraphics[width=0.25\textwidth]{img/T1bbbb.png}
\caption{Diagram of the dominant SUSY production mechanism
for the \texttt{T1bbbb} working point.}
\end{figure}

    \begin{table}[h!]
    \begin{centering}
    \begin{tabular}{ | l | l | r | }
%\hline
%    \texttt{T1bbbb} ($\mgo=900$, $\mlsp=500$; 0.060276 pb) & &\\
\hline
\input{cutflows/T1bbbb_cutflow.tex}
        \hline
    \end{tabular}
    \caption{The cut flow for the baseline selection in CMS SUS-12-028 for
    the  \texttt{T1bbbb} working point $(m_{\tilde g},\,m_{\tilde\chi^0_1})=(900,\,500)$~GeV. 
%    The second column is the official account as
%    reported by 
%    https://twiki.cern.ch/twiki/pub/CMSPublic/PhysicsResultsSUS12028/T1qqqq.pdf,
 %   and our own results are given in column 3. The official counts are
    The counts are
    normalized to the luminosity ${\cal L}=11.5$/fb times cross section $\sigma= 0.060276$~pb
    corresponding to 705.23 events which are taken to be the count
    after the first cut, MET Cleaning.}
    \label{table:T1bbbb}
    \end{centering}
    \end{table}
    
    \begin{table}
    \begin{centering}
    \begin{tabular}{ | l | c | c || l |c |c |}
    \hline
    \input{srs/T1bbbb_sr.tex}
    \end{tabular}
    \caption{The signal region counts in CMS SUS-12-028 for the \texttt{T1bbbb} scenario 
    after all selection has been applied. 
    %Column 2 is the official account obtained through generous correspondence with Christian Sanders,
    Our own results displayed in column 3. These counts were determined by applying the signal region selection to the end of the cut flow featured in \autoref{table:T1bbbb}.}
    \end{centering}
    \end{table}

    \begin{figure}[]
      \begin{center}
        \includegraphics[width=0.48\textwidth]{img/T1bbbb_alphaT.pdf}
        \includegraphics[width=0.48\textwidth]{img/AlphaT_ge4j.pdf}
      \end{center}
      \caption{Distribution of values of $\alpha_T$ for the \texttt{T1bbbb} model for 
          working point $(m_{\tilde g},\,m_{\tilde\chi^0_1})=(900,\,500)$~GeV. 
          The number of events is
          normalized to the luminosity ${\cal L}=11.5$/fb times cross section $\sigma= 0.060276$~pb
          corresponding to 705.23 events, divided by 0.05 as was done by CMS.
      The requirement $H_T > 375$ GeV is imposed, and events are categorized according to the number of reconstructed jets in the event.
      The final bin contains all events with $\alpha_T > 3$.
      Left: results from \texttt{MadAnalysis 5}. Right: CMS results from the \href{https://twiki.cern.ch/twiki/bin/view/CMSPublic/PhysicsResultsSUS12028}{CMS-SUS-12-028 TWiki}
      }
      \label{fig:T1bbbb_alphaT}
    \end{figure}

    \begin{figure}[]
      \begin{center}
        \includegraphics[width=0.48\textwidth]{img/T1bbbb_900_500_NJets_NoPhoton.pdf}
        \includegraphics[width=0.48\textwidth]{img/T1bbbb_900_500_NJets_alphaTgt055.pdf}
      \end{center}
      \caption{Distribution of the number of jets for the \texttt{T1bbbb} model working point after the lepton and photon cuts (left) and after the $\alpha_T>0.55$ cut (right).}
      \label{fig:T1bbbb_njets}
    \end{figure}

    \begin{figure}[]
      \begin{center}
        \includegraphics[width=0.48\textwidth]{img/T1bbbb_900_500_HT_HardJetsgteq2.pdf}
        \includegraphics[width=0.48\textwidth]{img/T1bbbb_900_500_HT_alphaTgt055.pdf}
      \end{center}
      \caption{Distribution of $H_T$ for the \texttt{T1bbbb} model working point after requiring at least 2 hard jets (left) and after the $\alpha_T>0.55$ cut (right).}
      \label{fig:T1bbbb_ht}
    \end{figure}

    \begin{figure}[]
      \begin{center}
        \includegraphics[width=0.48\textwidth]{img/T1bbbb_900_500_alphaT_NoPhoton.pdf}
        \includegraphics[width=0.48\textwidth]{img/T1bbbb_900_500_alphaT_HardJetsgteq2.pdf}
      \end{center}
      \caption{Distribution of $\alpha_T$ for the \texttt{T1bbbb} model working point after the lepton and photon cuts (left) and requiring at least 2 hard jets (right).
      The last bin contains all events with $\alpha_T\geq 3$.
   }
      \label{fig:T1bbbb_alphat}
    \end{figure}




\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\texttt{T2bb} simplified model}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure}[h!]
\centering
\includegraphics[width=0.25\textwidth]{img/T2bb.jpeg}
\caption{Diagram of the dominant SUSY production mechanism
for the \texttt{T2bb} working point.}
\end{figure}

    \begin{table}[h!]
    \begin{centering}
    \begin{tabular}{ | l | l | r | }
\hline
\input{cutflows/T2bb_cutflow.tex}
        \hline
    \end{tabular}
    \caption{The cut flow for the baseline selection in CMS SUS-12-028 for
    the  \texttt{T2bb} working point $(m_{\tilde q},\,m_{\tilde\chi^0_1})=(900,\,500)$~GeV. 
%    The second column is the official account as
%    reported by 
%    https://twiki.cern.ch/twiki/pub/CMSPublic/PhysicsResultsSUS12028/T1qqqq.pdf,
 %   and our own results are given in column 3. The official counts are
    The counts are
    normalized to the luminosity ${\cal L}=11.5$/fb times cross section $\sigma=0.847051$~pb
    corresponding to 9910 events which are taken to be the count
    after the first cut, MET Cleaning.}
    \label{table:T2bb}
    \end{centering}
    \end{table}
    
    \begin{table}
    \begin{centering}
    \begin{tabular}{ | l | c | c || l |c |c |}
    \hline
    \input{srs/T2bb_sr.tex}
    \end{tabular}
    \caption{The signal region counts in CMS SUS-12-028 for the \texttt{T2bb} scenario 
    after all selection has been applied. 
    Our own results displayed in column 3. These counts were determined by applying the signal region selection to the end of the cut flow featured in \autoref{table:T2bb}.}
    \end{centering}
    \end{table}

    \begin{figure}[]
      \begin{center}
        \includegraphics[width=0.48\textwidth]{img/T2bb_alphaT.pdf}
        \includegraphics[width=0.48\textwidth]{img/AlphaT_le3j.pdf}
      \end{center}
      \caption{Distribution of values of $\alpha_T$ for the \texttt{T2bb} model for 
          working point $(m_{\tilde q},\,m_{\tilde\chi^0_1})=(500,\,150)$~GeV. 
          The number of events is
    normalized to the luminosity ${\cal L}=11.5$/fb times cross section $\sigma=0.847051$~pb
    corresponding to 9910 events, divided by 0.05 as was done by CMS.
      The requirement $H_T > 375$ GeV is imposed, and events are categorized according to the number of reconstructed jets in the event.
      The final bin contains all events with $\alpha_T > 3$.
      Left: results from \texttt{MadAnalysis 5}. Right: CMS results from the \href{https://twiki.cern.ch/twiki/bin/view/CMSPublic/PhysicsResultsSUS12028}{CMS-SUS-12-028 TWiki}
      }
      \label{fig:T2bb_alphaT}
    \end{figure}

    \begin{figure}[]
      \begin{center}
        \includegraphics[width=0.48\textwidth]{img/T2bb_500_150_NJets_NoPhoton.pdf}
        \includegraphics[width=0.48\textwidth]{img/T2bb_500_150_NJets_alphaTgt055.pdf}
      \end{center}
      \caption{Distribution of the number of jets for the \texttt{T2bb} model working point after the lepton and photon cuts (left) and after the $\alpha_T>0.55$ cut (right).}
      \label{fig:T2bb_njets}
    \end{figure}

    \begin{figure}[]
      \begin{center}
        \includegraphics[width=0.48\textwidth]{img/T2bb_500_150_HT_HardJetsgteq2.pdf}
        \includegraphics[width=0.48\textwidth]{img/T2bb_500_150_HT_alphaTgt055.pdf}
      \end{center}
      \caption{Distribution of $H_T$ for the \texttt{T2bb} model working point after requiring at least 2 hard jets (left) and after the $\alpha_T>0.55$ cut (right).}
      \label{fig:T2bb_ht}
    \end{figure}

    \begin{figure}[]
      \begin{center}
        \includegraphics[width=0.48\textwidth]{img/T2bb_500_150_alphaT_NoPhoton.pdf}
        \includegraphics[width=0.48\textwidth]{img/T2bb_500_150_alphaT_HardJetsgteq2.pdf}
      \end{center}
      \caption{Distribution of $\alpha_T$ for the \texttt{T2bb} model working point after the lepton and photon cuts (left) and requiring at least 2 hard jets (right).
      The last bin contains all events with $\alpha_T\geq 3$.
   }
      \label{fig:T2bb_alphat}
    \end{figure}




\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\texttt{T1} simplified model}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure}[h!]
\centering
\includegraphics[width=0.25\textwidth]{img/T1.pdf}
\caption{Diagram of the dominant SUSY production mechanism
for the \texttt{T1} working point.}
\end{figure}

    \begin{table}[h!]
    \begin{centering}
    \begin{tabular}{ | l | l | r | }
\hline
\input{cutflows/T1_cutflow.tex}
        \hline
    \end{tabular}
    \caption{The cut flow for the baseline selection in CMS SUS-12-028 for
    the  \texttt{T1} working point $(m_{\tilde g},\,m_{\tilde\chi^0_1})=(700,\,300)$~GeV. 
%    The second column is the official account as
%    reported by 
%    https://twiki.cern.ch/twiki/pub/CMSPublic/PhysicsResultsSUS12028/T1qqqq.pdf,
 %   and our own results are given in column 3. The official counts are
    The counts are
    normalized to the luminosity ${\cal L}=11.5$/fb times cross section $\sigma=0.433971$~pb
    corresponding to 5077 events which are taken to be the count
    after the first cut, MET Cleaning.}
    \label{table:T1}
    \end{centering}
    \end{table}
    
    \begin{table}
    \begin{centering}
    \begin{tabular}{ | l | c | c || l |c |c |}
    \hline
    \input{srs/T1_sr.tex}
    \end{tabular}
    \caption{The signal region counts in CMS SUS-12-028 for the \texttt{T1} scenario 
    after all selection has been applied. 
    %Column 2 is the official account obtained through generous correspondence with Christian Sanders,
    Our own results displayed in column 3. These counts were determined by applying the signal region selection to the end of the cut flow featured in \autoref{table:T1}.}
    \end{centering}
    \end{table}


    \begin{figure}[]
      \begin{center}
        \includegraphics[width=0.48\textwidth]{img/T1_700_300_NJets_NoPhoton.pdf}
        \includegraphics[width=0.48\textwidth]{img/T1_700_300_NJets_alphaTgt055.pdf}
      \end{center}
      \caption{Distribution of the number of jets for the \texttt{T1} model working point after the lepton and photon cuts (left) and after the $\alpha_T>0.55$ cut (right).}
      \label{fig:T1_njets}
    \end{figure}

    \begin{figure}[]
      \begin{center}
        \includegraphics[width=0.48\textwidth]{img/T1_700_300_HT_HardJetsgteq2.pdf}
        \includegraphics[width=0.48\textwidth]{img/T1_700_300_HT_alphaTgt055.pdf}
      \end{center}
      \caption{Distribution of $H_T$ for the \texttt{T1} model working point after requiring at least 2 hard jets (left) and after the $\alpha_T>0.55$ cut (right).}
      \label{fig:T1_ht}
    \end{figure}

    \begin{figure}[]
      \begin{center}
        \includegraphics[width=0.48\textwidth]{img/T1_700_300_alphaT_NoPhoton.pdf}
        \includegraphics[width=0.48\textwidth]{img/T1_700_300_alphaT_HardJetsgteq2.pdf}
      \end{center}
      \caption{Distribution of $\alpha_T$ for the \texttt{T1} model working point after the lepton and photon cuts (left) and requiring at least 2 hard jets (right).
      The last bin contains all events with $\alpha_T\geq 3$.
   }
      \label{fig:T1_alphat}
    \end{figure}




\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\texttt{T1tttt} simplified model}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure}[h!]
\centering
\includegraphics[width=0.25\textwidth]{img/T1tttt.pdf}
\caption{Diagram of the dominant SUSY production mechanism
for the \texttt{T1tttt} working point.}
\end{figure}

    \begin{table}[h!]
    \begin{centering}
    \begin{tabular}{ | l | l | r | }
\hline
\input{cutflows/T1tttt_cutflow.tex}
        \hline
    \end{tabular}
    \caption{The cut flow for the baseline selection in CMS SUS-12-028 for
    the  \texttt{T1tttt} working point $(m_{\tilde g},\,m_{\tilde\chi^0_1})=(850,\,250)$~GeV. 
%    The second column is the official account as
%    reported by 
%    https://twiki.cern.ch/twiki/pub/CMSPublic/PhysicsResultsSUS12028/T1ttttqqqq.pdf,
 %   and our own results are given in column 3. The official counts are
    The counts are
    normalized to the luminosity ${\cal L}=11.5$/fb times cross section $\sigma=0.0966803$~pb
    corresponding to 1131 events which are taken to be the count
    after the first cut, MET Cleaning.}
    \label{table:T1tttt}
    \end{centering}
    \end{table}
    
    \begin{table}
    \begin{centering}
    \begin{tabular}{ | l | c | c || l |c |c |}
    \hline
    \input{srs/T1tttt_sr.tex}
    \end{tabular}
    \caption{The signal region counts in CMS SUS-12-028 for the \texttt{T1tttt} scenario 
    after all selection has been applied. 
    %Column 2 is the official account obtained through generous correspondence with Christian Sanders,
    Our own results displayed in column 3. These counts were determined by applying the signal region selection to the end of the cut flow featured in \autoref{table:T1tttt}.}
    \end{centering}
    \end{table}


    \begin{figure}[]
      \begin{center}
        \includegraphics[width=0.48\textwidth]{img/T1tttt_850_250_NJets_NoPhoton.pdf}
        \includegraphics[width=0.48\textwidth]{img/T1tttt_850_250_NJets_alphaTgt055.pdf}
      \end{center}
      \caption{Distribution of the number of jets for the \texttt{T1tttt} model working point after the lepton and photon cuts (left) and after the $\alpha_T>0.55$ cut (right).}
      \label{fig:T1tttt_njets}
    \end{figure}

    \begin{figure}[]
      \begin{center}
        \includegraphics[width=0.48\textwidth]{img/T1tttt_850_250_HT_HardJetsgteq2.pdf}
        \includegraphics[width=0.48\textwidth]{img/T1tttt_850_250_HT_alphaTgt055.pdf}
      \end{center}
      \caption{Distribution of $H_T$ for the \texttt{T1tttt} model working point after requiring at least 2 hard jets (left) and after the $\alpha_T>0.55$ cut (right).}
      \label{fig:T1tttt_ht}
    \end{figure}

    \begin{figure}[]
      \begin{center}
        \includegraphics[width=0.48\textwidth]{img/T1tttt_850_250_alphaT_NoPhoton.pdf}
        \includegraphics[width=0.48\textwidth]{img/T1tttt_850_250_alphaT_HardJetsgteq2.pdf}
      \end{center}
      \caption{Distribution of $\alpha_T$ for the \texttt{T1tttt} model working point after the lepton and photon cuts (left) and requiring at least 2 hard jets (right).
      The last bin contains all events with $\alpha_T\geq 3$.
   }
      \label{fig:T1tttt_alphat}
    \end{figure}




\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\texttt{T2} simplified model}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure}[h!]
\centering
\includegraphics[width=0.25\textwidth]{img/T2.pdf}
\caption{Diagram of the dominant SUSY production mechanism
for the \texttt{T2} working point.}
\end{figure}

    \begin{table}[h!]
    \begin{centering}
    \begin{tabular}{ | l | l | r | }
\hline
\input{cutflows/T2_cutflow.tex}
        \hline
    \end{tabular}
    \caption{The cut flow for the baseline selection in CMS SUS-12-028 for
    the  \texttt{T2} working point $(m_{\tilde q},\,m_{\tilde\chi^0_1})=(600,\,250)$~GeV. 
%    The second column is the official account as
%    reported by 
%    https://twiki.cern.ch/twiki/pub/CMSPublic/PhysicsResultsSUS12028/T2qqqq.pdf,
 %   and our own results are given in column 3. The official counts are
    The counts are
    normalized to the luminosity ${\cal L}=11.5$/fb times cross section $\sigma=0.244862$~pb
    corresponding to 2865 events which are taken to be the count
    after the first cut, MET Cleaning.}
    \label{table:T2}
    \end{centering}
    \end{table}
    
    \begin{table}
    \begin{centering}
    \begin{tabular}{ | l | c | c || l |c |c |}
    \hline
    \input{srs/T2_sr.tex}
    \end{tabular}
    \caption{The signal region counts in CMS SUS-12-028 for the \texttt{T2} scenario 
    after all selection has been applied. 
    %Column 2 is the official account obtained through generous correspondence with Christian Sanders,
    Our own results displayed in column 3. These counts were determined by applying the signal region selection to the end of the cut flow featured in \autoref{table:T2}.}
    \end{centering}
    \end{table}


    \begin{figure}[]
      \begin{center}
        \includegraphics[width=0.48\textwidth]{img/T2_600_250_NJets_NoPhoton.pdf}
        \includegraphics[width=0.48\textwidth]{img/T2_600_250_NJets_alphaTgt055.pdf}
      \end{center}
      \caption{Distribution of the number of jets for the \texttt{T2} model working point after the lepton and photon cuts (left) and after the $\alpha_T>0.55$ cut (right).}
      \label{fig:T2_njets}
    \end{figure}

    \begin{figure}[]
      \begin{center}
        \includegraphics[width=0.48\textwidth]{img/T2_600_250_HT_HardJetsgteq2.pdf}
        \includegraphics[width=0.48\textwidth]{img/T2_600_250_HT_alphaTgt055.pdf}
      \end{center}
      \caption{Distribution of $H_T$ for the \texttt{T2} model working point after requiring at least 2 hard jets (left) and after the $\alpha_T>0.55$ cut (right).}
      \label{fig:T2_ht}
    \end{figure}

    \begin{figure}[]
      \begin{center}
        \includegraphics[width=0.48\textwidth]{img/T2_600_250_alphaT_NoPhoton.pdf}
        \includegraphics[width=0.48\textwidth]{img/T2_600_250_alphaT_HardJetsgteq2.pdf}
      \end{center}
      \caption{Distribution of $\alpha_T$ for the \texttt{T2} model working point after the lepton and photon cuts (left) and requiring at least 2 hard jets (right).
      The last bin contains all events with $\alpha_T\geq 3$.
   }
      \label{fig:T2_alphat}
    \end{figure}




\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\texttt{T2tt} simplified model}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure}[h!]
\centering
\includegraphics[width=0.25\textwidth]{img/T2tt.jpeg}
\caption{Diagram of the dominant SUSY production mechanism
for the \texttt{T2tt} working point.}
\end{figure}

    \begin{table}[h!]
    \begin{centering}
    \begin{tabular}{ | l | l | r | }
\hline
\input{cutflows/T2tt_cutflow.tex}
        \hline
    \end{tabular}
    \caption{The cut flow for the baseline selection in CMS SUS-12-028 for
    the  \texttt{T2tt} working point $(m_{\tilde q},\,m_{\tilde\chi^0_1})=(400,\,0)$~GeV. 
%    The second column is the official account as
%    reported by 
%    https://twiki.cern.ch/twiki/pub/CMSPublic/PhysicsResultsSUS12028/T2ttqqqq.pdf,
 %   and MA5 results are given in column 3. The official counts are
    The counts are
    normalized to the luminosity ${\cal L}=11.5$/fb times cross section $\sigma=3.54338$~pb
    corresponding to 41458 events which are taken to be the count
    after the first cut, MET Cleaning.}
    \label{table:T2tt}
    \end{centering}
    \end{table}
    
    \begin{table}
    \begin{centering}
    \begin{tabular}{ | l | c | c || l |c |c |}
    \hline
    \input{srs/T2tt_sr.tex}
    \end{tabular}
    \caption{The signal region counts in CMS SUS-12-028 for the \texttt{T2tt} scenario 
    after all selection has been applied. 
    %Column 2 is the official account obtained through generous correspondence with Christian Sanders,
    Our own results displayed in column 3. These counts were determined by applying the signal region selection to the end of the cut flow featured in \autoref{table:T2tt}.}
    \end{centering}
    \end{table}


    \begin{figure}[]
      \begin{center}
        \includegraphics[width=0.48\textwidth]{img/T2tt_400_0_NJets_NoPhoton.pdf}
        \includegraphics[width=0.48\textwidth]{img/T2tt_400_0_NJets_alphaTgt055.pdf}
      \end{center}
      \caption{Distribution of the number of jets for the \texttt{T2tt} model working point after the lepton and photon cuts (left) and after the $\alpha_T>0.55$ cut (right).}
      \label{fig:T2tt_njets}
    \end{figure}

    \begin{figure}[]
      \begin{center}
        \includegraphics[width=0.48\textwidth]{img/T2tt_400_0_HT_HardJetsgteq2.pdf}
        \includegraphics[width=0.48\textwidth]{img/T2tt_400_0_HT_alphaTgt055.pdf}
      \end{center}
      \caption{Distribution of $H_T$ for the \texttt{T2tt} model working point after requiring at least 2 hard jets (left) and after the $\alpha_T>0.55$ cut (right).}
      \label{fig:T2tt_ht}
    \end{figure}

    \begin{figure}[]
      \begin{center}
        \includegraphics[width=0.48\textwidth]{img/T2tt_400_0_alphaT_NoPhoton.pdf}
        \includegraphics[width=0.48\textwidth]{img/T2tt_400_0_alphaT_HardJetsgteq2.pdf}
      \end{center}
      \caption{Distribution of $\alpha_T$ for the \texttt{T2tt} model working point after the lepton and photon cuts (left) and requiring at least 2 hard jets (right).
      The last bin contains all events with $\alpha_T\geq 3$.
   }
      \label{fig:T2tt_alphat}
    \end{figure}




\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



\section{Exclusion Limits}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

It is also instructive to reproduce the 95\%~CL exclusion lines in the $(m_{\tilde g},\,m_{\tilde\chi^0_1})$ 
and $(m_{\tilde q},\,m_{\tilde\chi^0_1})$ mass planes.  
\autoref{fig:limitplots}  shows the limit curves (in red) obtained with a
\texttt{MadAnalysis 5} implementation and using the \texttt{exclusion\_CLs.py} code described in 
\href{http://arxiv.org/abs/arXiv:1407.3278}{arXiv:1407.3278}.
SUSY cross sections were taken from
\href{https://twiki.cern.ch/twiki/bin/view/LHCPhysics/SUSYCrossSections}{the LHC SUSY cross section working group},
uncertainties from which lead to the red, dashed lines.
Also shown are the official CMS exclusions with its $\pm 1\sigma$ theoretical uncertainty (solid and dashed blue lines).

It should be noted that the limit setting procedure used in \texttt{MadAnalysis 5} differs from that used by CMS, and so should be 
considered a rough estimate. For any given point
on the mass plane, the production cross section is taken from the LHC SUSY cross section working group
and the signal acceptance times efficiency is computed with the \texttt{MadAnalysis 5} recasting code for the analysis.
Subsequently, the most sensitive signal region, out of the 59 total signal regions, is determined based on the 
number of expected signal and background counts.
Finally, an upper limit on the cross section is determined from the expected signal, expected
background, expected uncertainty on the background, and observed counts in the most sensitive
signal region.
The ratios of LHC SUSY cross section ($\pm$ uncertainty) to upper limit were interpolated, 
and a contour through a ratio of one yields the red solid (dashed) lines.

The primary difference between the method used here and the method used by CMS is that
here only the most sensitive signal region is considered in the exclusion calculation, 
whereas CMS uses all signal regions simultaneously and considers correlations
in the uncertainty between signal regions.
This difference can lead to stronger exclusions by CMS.

 \begin{figure}
        \centering
%        \includegraphics[width=0.48\textwidth]{limit_plots/cms-012-limit-T1qqqq.pdf}
%        \includegraphics[width=0.48\textwidth]{limit_plots/cms-012-limit-T1tttt.pdf}\\
%        \includegraphics[width=0.48\textwidth]{limit_plots/cms-012-limit-T5VV.pdf}
%        \includegraphics[width=0.48\textwidth]{limit_plots/cms-012-limit-T2.pdf}
        \includegraphics[width=\textwidth]{img/ma5_t2qq_limits_alphat.pdf}
\caption{The 95\% CL exclusion limits (in red) reproduced with our {\sc{MadAnalysis}}~5 implementation compared to the official limits (in blue) from \href{https://twiki.cern.ch/twiki/bin/view/CMSPublic/PhysicsResultsSUS12028}{CMS-SUS-12-028}.
These limits are for the simplified model \texttt{T2}.
%Top left: T1qqqq, top right: T1tttt, bottom left: T5VV, bottom right: T2 simplified models.
\label{fig:limitplots}}
        \end{figure} 




\end{document}
