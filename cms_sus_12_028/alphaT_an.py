import argparse
import math
import itertools
from functions import *
from analysis_functions import *

parser = argparse.ArgumentParser()
parser.add_argument('--file', dest='file', type=str, required=True, help='Specify the filename for which the efficiency should be read.')
parser.add_argument('--no-tau', dest='tau',action='store_false', help='Do not count taus as jets: If this specified, taus are not counted as jets. Default is to include taus as jets.')
parser.add_argument('--countjets', '-j', dest='countjets', action='store_true', default=False, help='Whether the number of jets in each event should be counted in addition. By default this is not done.')
parser.add_argument('--more-info', '-mi', dest='print_mht_ht', action='store_true', help='Print MHT/HT/bin information for events falling into specific bins.')
parser.set_defaults(print_mht_ht=False)
parser.set_defaults(tau=True)
arguments = vars(parser.parse_args())

# The event file should be an lhe file or an lhco file.
event_file = arguments['file']

#Whether to include taus as jets:
tau = arguments['tau']
cutflow = True
# In order to announce errors
MESSAGE = ''

# Whether to print MHT/HT information
print_mht_ht = arguments['print_mht_ht']


if __name__ == '__main__':
    #import doctest
    #doctest.testmod()
    print 'taus as jets:', tau
    passed, n, cuts, data = analyze_eventfile(event_file, tau, 'alpha_T')
    print 'ALPHA_T:', n, ' and events passed:', sum([passed[k] for k in passed])
    for binno in passed:
        print "passed bin " + str(binno) + ":", passed[binno], '+-', int(round(relative_error(passed[binno], n)))
    print "ALPHA_T CUTS"
    for cut in cuts:
        print cut, cuts[cut]
    if print_mht_ht:
        print "ALPHA_T HT/MHT and bins:"
        for i in range(int(n*1.2)):
            if str(i) in data:
                print i, data[str(i)] #, for k in data[str(i)])
    l = [str(passed[binno]) + ' +- ' + str(int(round(relative_error(passed[binno], n)))) for binno in range(8) if binno in passed]
    print '\n'
    s0 = ''
    s1 = ''
    s2 = ''
    for binno in passed:
        s0 += str(binno) + '|'
        s1 += str(round(passed[binno]/(n*1.0),4)) + '+-' + str(round(relative_error_scaled(passed[binno], n), 5)) + '|'
        s2 += str(passed[binno]) + '+-' + str(round(relative_error_scaled(passed[binno], n), 5)) + '|'
    cumul = str(sum([passed[binno] for binno in passed])/(1.0*n)) + ' +- ' + str(int(relative_error_scaled(sum([passed[binno] for binno in passed]), n))/(1.0*n))
    print 'alpha_T cumulative:', cumul
    print write_line(l, sep='|')  
    print '\n'
    if cutflow:
        print 'Cutflow: ' + str(n) + ' events'
        l = n
        for i in range(13):
            ks = [c for c in cuts if c.startswith(str(i)+'.')]
            if len(ks) > 0:
                k = ks[0]
            else:
                continue
            l = l - cuts[k]
            print k, l

    if arguments['countjets']:
        jets, event_number = count_jets(event_file, tau)
        print "Total amount of jets in ", event_number, "events:"
        for k in jets['total']:
            print k, ":", jets['total'][k]
        print "Amount of alphaT jets:"
        for k in jets['alpha_T']:
            print k, ":", jets['alpha_T'][k]
    print MESSAGE
